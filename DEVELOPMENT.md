# How To Develop

## BitBake Files
- es sollen Linux Zeilenumbrüche (LF) verwendet werden.
- es soll soweit möglich mit Tabulatoren und nicht mit Spaces eingerückt werden.
- .bb Files sollen nur dann eine Versionsnummer im Namen haben (z.B. hallowelt_1.0.bb), wenn diese spezielle Regeln enthalten die nur für diese spezielle Version der verwendeten Sourcen benötigt werden. Gleiches gilt für 'git' als Versionsnummer im .bb Namen, das nur dann verwendet wird, wenn das .bb File spezielle Anpassungen für die git Quelle der Sourcen enthält.
- Bei externen git Sourcen kann PV = "git" angegeben werden, wenn nicht eine bestimmte getaggte Version genommen wird. Wird von einer getaggten git Version auf neuere git Sourcen ohne tag aktualisiert, wird die Version um '+git' erweitert, z.B. PV = "1.3+git"
- Um immer den HEAD der externen git Sourcen zu nehmen kann AUTOREV verwendet werden, was dafür sorgt das mit jede änderung der Sourcen eine höhere Paket Version gebaut wird:
  ```
  BPV = "1.0.0"
  PR = "r1"
  SRCREV = "${AUTOREV}"
  PV = "${BPV}+git${SRCPV}"
  ```
- Die Package Revision des .bb Files wird bei jeder Änderung automatisch erhöt (z.B. r1.1, r1.2, ...). Trotzdem soll die PR Revisions Angabe erhöt werden, wenn die Entwicklung eines Features abgeschlossen ist, da die automatische Erhöhung der Revision bei jeden Entwickler getrennt erfolgt.
- Es ist möglich eine zentrale Datei anzulegen, in der die zu verwendende Version der einzelnen Pakete steht, oder bei welchem Paket AUTOREV verwendet werden soll: https://www.yoctoproject.org/docs/1.8/ref-manual/ref-manual.html#maintaining-build-output-quality
- Jedes .bb File soll eine SUMMARY und DESCRIPTION enthalten, die gut verständlich beschreibt, worum es sich bei diesem Paket handelt.

## VDR Plugins
- Über die vdr.bbclass und vdr-plugin.bbclass werden einige Variablen festgelegt und regeln ausgeführt, die für alle Plugins benötigt werden.
- Plugin Start Optionen werden unter /etc/vdr/conf.d/... gespeichert. Wenn nichts spezielles konfiguriert wird, wird dort eine Datei angelegt, die lediglich den Pluginnamen enthält, damit das Plugin geladen wird.
- Start Optionen die über das Webif konfigurierbar sein sollen, werden beim Start des VDRs über ein run Script aus den systemweiten Settings in die /etc/vdr/conf.d/... Datei übertragen. (siehe satip Plugin)

## Kernel Config
2.3.2. Generating Configuration Files

You can manipulate the .config file used to build a linux-yocto recipe with the menuconfig command as follows:

     $ bitbake linux-yocto -c menuconfig


This command starts the Linux kernel configuration tool, which allows you to prepare a new .config file for the build. When you exit the tool, be sure to save your changes at the prompt.

The resulting .config file is located in ${WORKDIR} under the linux-${MACHINE}-${KTYPE}-build directory. You can use the entire .config file as the defconfig file as described in the "Changing the Configuration" section.

A better method is to create a configuration fragment using the differences between two configuration files: one previously created and saved, and one freshly created using the menuconfig tool.

To create a configuration fragment using this method, follow these steps:

    Complete a build at least through the kernel configuration task as follows:

         $ bitbake linux-yocto -c kernel_configme -f


    Run the menuconfig command:

         $ bitbake linux-yocto -c menuconfig


    Run the diffconfig command to prepare a configuration fragment. The resulting file fragment.cfg will be placed in the ${WORKDIR} directory:

         $ bitbake linux-yocto -c diffconfig


    The diffconfig command creates a file that is a list of Linux kernel CONFIG_ assignments. See the "Changing the Configuration" section for information on how to use the output as a configuration fragment.

## devtool
     https://wiki.koansoftware.com/index.php/Using_devtool_to_modify_recipes_in_Yocto