FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

# the logo must have a dimension multiple of 8
# convert logo-1920x1080.png -depth 8 -compress none -verbose BMP3:logo-1920x1080.bmp

SRC_URI += "file://logo-1920x1080.bmp"

do_deploy:append() {
  install -m 0644 ${WORKDIR}/logo-1920x1080.bmp ${DEPLOYDIR}/
  install -m 0644 ${WORKDIR}/logo-1920x1080.bmp ${DEPLOYDIR}/logo.bmp
}
