DESCRIPTION = "RPI eeprom updater and config tool and files"
SUMMARY = "rpi-eeprom"
COMPATIBLE_MACHINE = "(raspberrypi4|raspberrypi4-64)"
PR = "r2"

LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://LICENSE"

SRC_URI = "git://github.com/raspberrypi/rpi-eeprom.git;protocol=https;branch=master"
SRCREV = "ef2fc67d235d037b2b468813e646e20890fcea07"

S = "${WORKDIR}/git"

RDEPENDS:${PN} += "python3 pciutils util-linux-findmnt"

do_install () {
	install -d ${D}${sysconfdir}/
	install -d ${D}${sysconfdir}/default/
	install -m 755 ${WORKDIR}/git/rpi-eeprom-update-default ${D}${sysconfdir}/default/rpi-eeprom-update

	install -d ${D}${bindir}
	install -m 755 ${WORKDIR}/git/rpi-eeprom-config ${D}${bindir}
	install -m 755 ${WORKDIR}/git/rpi-eeprom-digest ${D}${bindir}
	install -m 755 ${WORKDIR}/git/rpi-eeprom-update ${D}${bindir}

	install -d ${D}${base_libdir}/firmware/raspberrypi/bootloader/default
	# RPI-4
	install -m 644 -D ${WORKDIR}/git/firmware-2711/default/* ${D}${base_libdir}/firmware/raspberrypi/bootloader/default
	# RPI-5
	#install -m 644 -D ${WORKDIR}/git/firmware-2712/default/* ${D}${base_libdir}/firmware/raspberrypi/bootloader/default
}

FILES:${PN} = " \
	${sysconfdir}/default/* \
	${bindir}/* \
	${base_libdir}/firmware/* \
	"
