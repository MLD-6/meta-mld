do_deploy:append() {
	if [ ! -z "${INITRAMFS_IMAGE}" -a "${INITRAMFS_IMAGE_BUNDLE}" != "1" ]; then
		sed -i '/#initramfs / c\initramfs initrd' $CONFIG
	fi
	sed -i 's/#disable_splash/disable_splash/' $CONFIG
}
