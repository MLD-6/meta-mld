FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
PR = "r4"

SRC_URI += "file://btrfs.cfg"
SRC_URI += "file://compress-kernel.cfg"
SRC_URI += "file://fw-loader.cfg"
SRC_URI += "file://ir.cfg"
SRC_URI += "file://squashfs.cfg"
SRC_URI += "file://xfs.cfg"
SRC_URI += "file://nfsd.cfg"

KERNEL_FEATURES += "features/media/media-all.scc"
KERNEL_FEATURES += "features/overlayfs/overlayfs.scc"
KERNEL_FEATURES += "features/wifi/wifi-all.scc"

INSANE_SKIP:${PN} = "installed-vs-shipped"

# pkg_postrm:${KERNEL_PACKAGE_NAME}-image-zimage:prepend() {
# #!/bin/sh
# [ ! -e ${KERNEL_IMAGEDEST}/zImage-* ] || exit 0
# }

# pkg_postrm:${KERNEL_PACKAGE_NAME}-image-uimage:prepend() {
# #!/bin/sh
# [ ! -e ${KERNEL_IMAGEDEST}/uImage-* ] || exit 0
# }
