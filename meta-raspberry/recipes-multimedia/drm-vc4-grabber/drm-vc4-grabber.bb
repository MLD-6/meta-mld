SUMMARY = "Hyperion DRM VC4 screen grabber"
DESCRIPTION = "Hyperion DRM VC4 screen grabber."
PV = "0.1.1"
PR = "r3"

LICENSE = "Apache-2.0"
COMPATIBLE_MACHINE = "(raspberrypi4-64|raspberrypi5)"

SRC_URI = "https://github.com/rudihorn/drm-vc4-grabber/releases/download/v0.1.1/drm-vc4-grabber-v0.1.1-aarch64-linux.tar.xz"
SRC_URI[sha256sum] = "d17186a07ea0271398ccd9cf52b2872d08214311c1c9fa43785e2006296ce906"

SRC_URI += "\
	file://drm-capture.service \
"

DEPENDS += " \
	hyperion \
	"
RDEPENDS:${PN} += " \
	hyperion \
	"


inherit systemd

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/drm-vc4-grabber-v0.1.1-aarch64-linux/drm-vc4-grabber ${D}${bindir}
	install -d ${D}${systemd_unitdir}/system/
	install -m 0644 ${WORKDIR}/drm-capture.service ${D}${systemd_unitdir}/system/
}

INSANE_SKIP:${PN} += "already-stripped"

SYSTEMD_SERVICE:${PN} = "drm-capture.service"
