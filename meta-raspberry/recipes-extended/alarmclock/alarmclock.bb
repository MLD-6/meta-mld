DESCRIPTION = "A radio alarm clock."
SUMMARY = "alarmclock"
AUTHOR = "Claus Muus"
SECTION = "base/utils"
BPV = "1.0.0"
PR = "r1"
SRCREV = "${AUTOREV}"
PV = "${BPV}+git${SRCPV}"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://LICENSE;md5=4ee23c52855c222cba72583d301d2338"
# get list of all Licenses used by node_modules: "devtool add --npm-dev git://gitlab.com/clausmuus/alarmclock.git"

SRC_URI = "git://gitlab.com/clausmuus/alarmclock.git;protocol=https;branch=main"
#SRC_URI[sha256sum] = "4b9ea1df26ca6d60c751d4b87352e649088a42a4"

S = "${WORKDIR}/git"

inherit npm systemd

DEPENDS = "nodejs alsa-lib"
RDEPENDS:${PN} = "nodejs python3"
PREFERRED_VERSION_nodejs = "20.%"
ASSUME_PROVIDED:${PN} = "make perl"

SYSTEMD_SERVICE:${PN} = "alarmclock.service"

sysroot = "${WORKDIR}/recipe-sysroot-native"

do_compile () {
	npm --target_arch=arm install
	find node_modules -name "*.a" -type f -delete
	patch -p 0 -i patches/oled-display.patch
	patch -p 0 -i patches/raspi-1wire-temp.patch
	patch -p 0 -i patches/stupid-player.patch
}

do_install () {
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${S}/alarmclock.service ${D}${systemd_unitdir}/system
	install -d ${D}${datadir}/alarmclock
	cp    ${S}/app.js ${D}${datadir}/alarmclock
	cp -r ${S}/fonts ${D}${datadir}/alarmclock
	cp -r ${S}/modules ${D}${datadir}/alarmclock
	cp -r ${S}/node_modules ${D}${datadir}/alarmclock
	cp -r ${S}/sounds ${D}${datadir}/alarmclock
	cp -r ${S}/views ${D}${datadir}/alarmclock
	find ${D}${datadir}/alarmclock/node_modules -name "*.pl" -type f -delete
	rm ${D}${datadir}/alarmclock/node_modules/@schneefux/lame/deps/lame/debian/rules
}

FILES:${PN} += " \
	${datadir}/alarmclock/* \
	${systemd_unitdir}/system/* \
	"

pkg_postinst:${PN}() {
#!/bin/sh
if [ -z "$D" ]; then
	pip3 install luma.oled

	echo "You must add this to your /boot/config.txt to allow hardware access:" >&2
	echo "dtparam=audio=on,spi=on,i2c_arm=on\ndtoverlay=w1-gpio,gpiopin=18" >&2
fi
}
