SUMMARY = "VDR tvscaper plugin"
AUTHOR = "Stefan Braun <louis.braun@gmx.de>"
PV = "git"
PR = "r3"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://github.com/MarkusEh/vdr-plugin-tvscraper.git;protocol=https;branch=master"
SRCREV = "f0b383f39ca656c9e3e3c04d5d0d70b9fc4e2b87"


S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	curl \
	sqlite3 \
	jansson \
	libxml2 \
	"

RDEPENDS:${PN}+= " \
	curl \
	sqlite3 \
	jansson \
	libxml2 \
	"


EXTRA_OEMAKE = " \
	STRIP=/bin/true \
	SDKSTAGE="${STAGING_DIR_HOST}" \
	'INCLUDES+=-I$(SDKSTAGE)/usr/include/libxml2' \
	'LIBS = "$(shell pkg-config --libs libcurl)"' \
	'LIBS += "$(shell pkg-config --libs sqlite3)"' \
	'LIBS += "$(shell pkg-config --cflags --libs jansson)"' \
	'LIBS += "-lxml2"' \
	"

do_install() {
	oe_runmake DESTDIR=${D} install
	oe_runmake DESTDIR=${D} install-plugins
}

FILES:${PN} += " \
	${sharedir}/vdr/plugins/tvscraper/* \
	"

