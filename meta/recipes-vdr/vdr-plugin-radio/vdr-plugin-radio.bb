SUMMARY = "VDR radio plugin"
PV = "git"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/vdr-projects/vdr-plugin-radio.git;protocol=https;branch=master"
SRCREV = "468280ff7252f9504e5b3d63fcf5d277b5627541"

SRC_URI += " \
	file://00_radio_VDR271.patch \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

#DEPENDS += "vdr-font-symbols"

EXTRA_OEMAKE = ' \
	SDKSTAGE="${STAGING_DIR_HOST}" \
	'

do_install() {
	oe_runmake DESTDIR=${D} install
}
