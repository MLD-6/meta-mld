SUMMARY = "This plugin uses a Hauppauge PVR card as an input device."
PV = "git"
PR = "r5"

LICENSE = "AGPL-3.0-only"

SRC_URI = "git://github.com/yavdr/vdr-plugin-pvrinput.git;protocol=https;branch=master"
SRCREV = "aef2954e20f55f585c892ba99d4d8b2f826e76bf"


S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
    udev \
	"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[${PLUGIN_NAME}]" > ${D}${sysconfdir}/vdr/conf.d/30_${PLUGIN_NAME}.conf
}

