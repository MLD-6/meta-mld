SUMMARY = "This plugin offers SVDRP connections as a service to other plugins."
AUTHOR = "Frank Schmirler"
PV = "1.0.0"
PR = "r3"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=393a5ca445f6965873eca0259a17f833"

SRC_URI = "http://vdr.schmirler.de/svdrpservice/vdr-svdrpservice-${PV}.tgz"
SRC_URI[sha256sum] = "c25147163c7abf864ca99d9d4c1a821242f3c4eddb658d8b691edb3f543818d8"  

S = "${WORKDIR}/svdrpservice-${PV}"

inherit vdr-plugin

do_install() {
	oe_runmake DESTDIR=${D} install
}
