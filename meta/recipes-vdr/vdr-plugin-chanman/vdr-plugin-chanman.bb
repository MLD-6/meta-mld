SUMMARY = "Channel manager"
AUTHOR = " RINALDO Giancarlo <rinaldo@dipmat.unime.it>"
PV = "git"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/ua0lnj/vdr-plugin-chanman.git;protocol=https;branch=master"
SRCREV  = "193e4220e2607b403b0e04589b608289f9ae73e7"

S = "${WORKDIR}/git"

inherit vdr-plugin

EXTRA_OEMAKE = " STRIP=/bin/true "

do_install() {
	oe_runmake DESTDIR=${D} install
}
