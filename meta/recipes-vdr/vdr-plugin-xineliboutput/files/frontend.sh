#!/bin/sh

case "$1" in
  start)
    ${0%/*/*}/frontend.sh resume
    trap 'trap "" TERM; kill -s TERM -- -$$' TERM # kill childs on SIGTERM
    test -e /etc/default/vdr-sxfe && . /etc/default/vdr-sxfe
    vdr-sxfe $ARGS & wait
    ${0%/*/*}/frontend.sh suspend
    ;;
  resume)
		# refreshrate=$(setting get vdr.refreshrate)
		# screen=$(setting get display.active[0].screen)
    # test "$refreshrate" && setmode -o "$screen" -r $refreshrate
    ;;
  suspend)
		# refreshrate=$(setting get display.active[0].refreshrate)
		# screen=$(setting get display.active[0].screen)
    # test "$refreshrate" && setmode -o "$screen" -r $refreshrate
    ;;
  stop)
    ;;
esac
