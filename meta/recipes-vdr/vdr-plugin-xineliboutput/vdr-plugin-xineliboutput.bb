SUMMARY = "VDR HD output device for all devices by xinelib"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://gitlab.com/vdr-projects/vdr-plugin-xineliboutput.git;protocol=https;branch=master"
SRCREV = "021d0fcc0ba766fb80c6810c8e2e8787babcc19b"

SRC_URI += " \
	file://01_mpg2c-build.patch \
	"

SRC_URI += " \
	file://frontend.sh \
	file://run.sh \
	file://vdr-plugin-xineliboutput.menu \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

RDEPENDS:${PN} += "xserver-xorg"
RDEPENDS:${PN} += "util-linux-setterm"
RREPLACES:${PN} += "vdr-plugin-frontend"
RCONFLICTS:${PN} += "vdr-plugin-frontend"
RPROVIDES:${PN} += "vdr-plugin-frontend"

DEPENDS = "libxine vdr"

CXXFLAGS += "-fPIC -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE" 

EXTRA_OECONF = " \
	--disable-opengl \
	--disable-libcec \
	--enable-x11 \
	"

#do_configure:append() {
#  cat > Make.config <<-EOF
#    CONFIGURE_OPTS = --disable-opengl --disable-libcec --enable-x11
#	 EOF
#}

do_install() {
	oe_runmake DESTDIR=${D} frontends install

	install -d ${D}${sysconfdir}/default
	echo "ARGS=\"xvdr://localhost --waitvdr --reconnect -P tvtime:method=use_vo_driver\"" > ${D}${sysconfdir}/default/vdr-sxfe
	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[${PLUGIN_NAME}]\n-l none\n-t" > ${D}${sysconfdir}/vdr/conf.d/30_${PLUGIN_NAME}.conf
	install -d ${D}${sysconfdir}/vdr/plugins/xineliboutput
	ln -s ../../svdrphosts.conf ${D}${sysconfdir}/vdr/plugins/xineliboutput/allowed_hosts.conf
	install -d ${D}${sharedir}/vdr/frontend.d
	install -m 0755 ${WORKDIR}/frontend.sh  ${D}${sharedir}/vdr/frontend.d/50_xineliboutput.sh
	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh  ${D}${sharedir}/vdr/run.d/30_xineliboutput.sh
	install -d ${D}${sharedir}/menu
	install -m 0644 ${WORKDIR}/vdr-plugin-xineliboutput.menu ${D}${sharedir}/menu
}

pkg_postinst:${PN}() {
#!/bin/sh
cat >> /etc/vdr/setup.conf.add <<- EOF
	xineliboutput.Audio.SoftwareVolumeControl = 1
	xineliboutput.Media.BrowseFilesDir = /data/video
	xineliboutput.Media.BrowseImagesDir = /data/photo
	xineliboutput.Media.BrowseMusicDir = /data/music
	xineliboutput.Remote.UseTcp = 1
	xineliboutput.Remote.UseUdp = 1
	xineliboutput.RemoteMode = 1
	xineliboutput.Video.Deinterlace = tvtime
EOF
}

FILES:${PN} += " \
	${sharedir}/vdr/frontend.d/* \
	${sharedir}/vdr/run.d/* \
	${libdir}/xine/plugins/2.11/*.so \
	${libdir}/xine/plugins/2.11/*.types \
	${libdir}/xine/plugins/2.11/post/*.so \
	${datadir}/xine-lib/* \
	${datadir}/menu/* \
	"
