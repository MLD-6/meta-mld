SUMMARY = "VDR favorites plugin"
HOMEPAGE = "https://github.com/vdr-projects/vdr-plugin-favorites"
AUTHOR = "gnapheus@vdr-portal.de"

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://github.com/vdr-projects/vdr-plugin-favorites.git;protocol=https;branch=master"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "a1b640ef65e4e0c9305cc2b0acc458cfdfe1d045"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += ""

sharedir = "${prefix}/share"

do_configure () {
	# Specify any needed configure commands here
	:
}

do_compile () {
	# You will almost certainly need to add additional arguments here
	oe_runmake
}

do_install () {
	# This is a guess; additional arguments may be required
	oe_runmake install 'DESTDIR=${D}'
}
 