SUMMARY = "VDR HD output device with Nvidia vdpau GPU support"
COMPATIBLE_MACHINE = "(genericx86-64)"
PV = "git"
PR = "r10"

LICENSE = "AGPL-3.0-only"
LIC_FILES_CHKSUM = "file://AGPL-3.0.txt;md5=c959e6f173ca1387f63fbaec5dc12f99"

SRC_URI = "git://github.com/ua0lnj/vdr-plugin-softhddevice.git;protocol=https;branch=vdpau+vaapi+cuvid"
SRCREV = "68590fee9982421358c9f33270399314e9fa3f13"

SRC_URI += " \
	file://frontend.sh \
	file://run.sh \
	file://vdr-plugin-softhddevice.menu \
	file://vdr-plugin-softhddevice.service \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin
inherit systemd

SYSTEMD_SERVICE:${PN} = "vdr-plugin-softhddevice.service"

RDEPENDS:${PN} += "xserver-xorg"
RREPLACES:${PN} += "vdr-plugin-frontend"
RCONFLICTS:${PN} += "vdr-plugin-frontend"
RPROVIDES:${PN} += "vdr-plugin-frontend"

DEPENDS += " \
	nvidia \
	ffmpeg \
	xcb-util-wm \
	libvdpau \
	libglu \
	glew \
	freeglut \
	glm \
	freetype \
	"

EXTRA_OEMAKE = " \
	CUVID=0 \
	OPENGLOSD=0 \
	VDPAU=1 \
	VAAPI=0 \  
	OPENGL=1 \
	AVRESAMPLE=0 \
	SWSCALE=1 \
	SWRESAMPLE=1 \
	EGL=1 \
	"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[softhddevice]" > ${D}${sysconfdir}/vdr/conf.d/30_softhddevice.conf
	install -d ${D}${sharedir}/vdr/frontend.d
	install -m 0755 ${WORKDIR}/frontend.sh  ${D}${sharedir}/vdr/frontend.d/50_softhddevice.sh
	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_softhddevice.sh
	install -d ${D}${sharedir}/menu
	install -m 0644 ${WORKDIR}/vdr-plugin-softhddevice.menu ${D}${sharedir}/menu
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/vdr-plugin-softhddevice.service ${D}${systemd_unitdir}/system
}

pkg_postinst:${PN}() {
#!/bin/sh
cat >> /etc/vdr/setup.conf.add <<- EOF
	softhddevice.1080i.Deinterlace = 2
	softhddevice.1080i_fake.Deinterlace = 2
	softhddevice.576i.Deinterlace = 2
	softhddevice.720p.Deinterlace = 1
	softhddevice.AudioSoftvol = 1
	softhddevice.BlackPicture = 1
	softhddevice.MakePrimary = 1
	softhddevice.Suspend.Close = 1
EOF
}

FILES:${PN} += " \
	${sharedir}/vdr/frontend.d/* \
	${sharedir}/vdr/run.d/* \
	${sharedir}/menu/* \
	${systemd_unitdir}/system/* \
	"
