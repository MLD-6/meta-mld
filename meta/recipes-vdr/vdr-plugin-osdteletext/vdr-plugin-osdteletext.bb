SUMMARY = "VDR osdteletext plugin"
HOMEPAGE = "https://github.com/vdr-projects/vdr-plugin-osdteletext"
AUTHOR = "Marcel Wiesweg <marcel.wiesweg@gmx.de>"
PV = "git"
PR = "r0"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=751419260aa954499f7abaabaa882bbe"

SRC_URI = "git://github.com/vdr-projects/vdr-plugin-osdteletext.git;protocol=https;branch=master"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "cae4629f84886015b0619af6fdb1084853b80f93"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += ""

sharedir = "${prefix}/share"


do_configure () {
	# Specify any needed configure commands here
	:
}

do_compile () {
	# You will almost certainly need to add additional arguments here
	oe_runmake
}

do_install() {
	oe_runmake DESTDIR=${D} install-lib install-i18n
	install -d ${D}/usr/share/fonts/ttf
	install -m 0644 ${S}/teletext2.ttf      ${D}/usr/share/fonts/ttf
}

FILES:${PN} += "${datadir}/fonts/ttf/"

#Additional default configuration for this plugin
pkg_postinst:${PN}() {
#!/bin/sh
echo -e "--directory=/tmp/vtx \n" >> ${sysconfdir}/vdr/conf.d/50_${PLUGIN_NAME}.conf
}
