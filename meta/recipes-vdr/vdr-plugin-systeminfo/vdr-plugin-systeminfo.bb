DESCRIPTION = "VDR plugin to show system information"
HOMEPAGE = "https://example.com/vdr-plugin-systeminfo"
LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

SRC_URI = "git://github.com/FireFlyVDR/vdr-plugin-systeminfo.git;protocol=https;branch=main"
SRCREV = "33edb3d8e2f1c7dfc8fed912f56f57d15b50dc53"

PV = "1.0"
PR = "r0"

SRC_URI += " \
    file://systeminfo.conf \
    "

S = "${WORKDIR}/git"

inherit vdr-plugin

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/plugins
    install -d ${D}${sysconfdir}/vdr/plugins/systeminfo
	install -m 0755 ${S}/scripts/systeminfo.sh      ${D}${sysconfdir}/vdr/plugins/systeminfo/systeminfo.sh

   	install -d ${D}${sysconfdir}/vdr/conf.d
	install -m 0644 ${WORKDIR}/systeminfo.conf      ${D}${sysconfdir}/vdr/conf.d/50_systeminfo.conf

}

FILES:${PN} += " \
    ${sysconfdir}/vdr/plugins \
    ${sysconfdir}/vdr/plugins/systeminfo \
	${sysconfdir}/vdr/plugins/systeminfo/systeminfo.sh \
    ${sysconfdir}/vdr/conf.d \
    ${sysconfdir}/vdr/conf.d/50_systeminfo.conf \
	"


