#!/bin/sh

case "$1" in
	start)
		args=$(setting get -d "-h 127.0.0.1 -p 13666" vdr.plugins.lcdproc.args)

		echo -e "[lcdproc]\n$args" > /etc/vdr/conf.d/50_lcdproc.conf
		;;
esac
