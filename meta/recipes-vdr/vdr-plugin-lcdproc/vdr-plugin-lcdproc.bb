SUMMARY = "Output to LCD modules that are supported by LCDproc"
AUTHOR = "Martin Hammerschmid <martin@hammerschmid.com>"
PV = "1.7.34"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/vdr-projects/vdr-plugin-lcdproc.git;protocol=https;branch=master"
SRCREV = "0c548975b2d66d860180c79d58235a8923641a0c"
SRC_URI[sha256sum] = "62d069d87f75e32d10eec0d4e37a1a2294d361d244e37e062dc212500bfb49a3"

SRC_URI += " \
	file://01_lcdproc_lock.patch \
	file://02_lcdproc_setup.patch \
	file://10_lcdproc_mld.patch \
	file://run.sh \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += "lcdproc"
RDEPENDS:${PN} += "lcdproc"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[lcdproc]" > ${D}${sysconfdir}/vdr/conf.d/50_lcdproc.conf

	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/50_lcdproc.sh
}


FILES:${PN} += " \
	${sharedir}/vdr/run.d/* \
	"
