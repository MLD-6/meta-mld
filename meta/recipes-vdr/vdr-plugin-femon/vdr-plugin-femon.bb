SUMMARY = "DVB Frontend Status Monitor plugin for VDR"
AUTHOR = "Rolf Ahrenberg"
PV = "git"
PR = "r5"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://github.com/rofafor/vdr-plugin-femon.git;protocol=https;branch=master"
SRCREV  = "28e4fb8de8578a1b8a36eb38d2e710dc60be7e3f"

SRC_URI += " \
	file://00_femon_VDR271.patch \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

EXTRA_OEMAKE = " STRIP=/bin/true "

do_install() {
	oe_runmake DESTDIR=${D} install
}
