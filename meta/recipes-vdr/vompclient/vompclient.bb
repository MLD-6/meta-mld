DESCRIPTION = "VOMP client for the Raspberry Pi"
HOMEPAGE = "http://www.loggytronic.com/vomp.php"
SECTION = "multimedia"
COMPATIBLE_MACHINE = "raspberrypi"
PV = "git"
PR = "r2"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=751419260aa954499f7abaabaa882bbe"

SRCREV = "master"
SRC_URI = "git://git.vomp.tv/vompclient.git;protocol=https;branch=master"

SRC_URI += " \
	file://init \
	file://make-oe.patch \
	file://0001-Fixed-sig.patch \
	"

S = "${WORKDIR}/git"

inherit update-rc.d

DEPENDS = "virtual/libgl virtual/libgles2 virtual/egl libav freetype libcec-rpi imagemagick"

INITSCRIPT_NAME = "vompclient"
INITSCRIPT_PARAMS = "defaults 20" 

export LIBPATHS = " \
	-L${STAGING_DIR_HOST}/lib \
	-L${STAGING_DIR_HOST}/usr/lib \
	-L${STAGING_DIR_HOST}/usr/lib/vc \
	"

export INCLUDES = " \
	-I${STAGING_DIR_HOST}/usr/include \
	-I${STAGING_DIR_HOST}/usr/include/interface/vcos \
	-I${STAGING_DIR_HOST}/usr/include/interface/vcos/generic \
	-I${STAGING_DIR_HOST}/usr/include/interface/vcos/pthreads \
	-I${STAGING_DIR_HOST}/usr/include/vc \
	-I${STAGING_DIR_HOST}/usr/include/freetype2 \
	-I${STAGING_DIR_HOST}/usr/include/ImageMagick \
	"
## Install in ${D}
export DEST = "${D}/usr/bin"

do_compile() {
	oe_runmake -f Makefile.oe
}

do_install() {
	install -d ${D}${bindir}/
	install -m 0755 ${S}/vompclient ${D}${bindir}/
	
	install -d ${D}${sysconfdir}/init.d
	install -m 0755 ${WORKDIR}/init ${D}${sysconfdir}/init.d/vompclient
	
#	install -d ${D}${sysconfdir}/default
#	install -m 0755 ${WORKDIR}/default ${D}${sysconfdir}/default/vompclient
}

FILES:${PN} = " \
	${bindir}/vompclient \
	${sysconfdir}/init.d/vompclient \
	"
