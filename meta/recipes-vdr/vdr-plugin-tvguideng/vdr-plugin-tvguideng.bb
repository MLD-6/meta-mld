SUMMARY = "VDR tvguideng plugin"
AUTHOR = "Louis Braun <louis.braun@gmx.de>"
PV = "git"
PR = "r5"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://gitlab.com/kamel5/tvguideng.git;protocol=https;branch=master"
SRCREV = "fe2bd897b860ca08da8a6d5f9f887b755e47013e"

#SRC_URI += " \
#	file://00_tvguideng_VDR271.patch \
#	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	graphicsmagick \
	libskindesignerapi \
	"
RDEPENDS:${PN}+= "bash libskindesignerapi"
RRECOMMENDS:${PN}+= "vdr-plugin-skindesigner"

EXTRA_OEMAKE = " \
	STRIP=/bin/true \
	SDKSTAGE="${STAGING_DIR_HOST}" \
	"

do_install() {
	oe_runmake DESTDIR=${D} install
}

FILES:${PN} += " \
	${datadir}/vdr/plugins/tvguideng/* \
	${sysconfdir}/vdr/icons/default/* \
	${sysconfdir}/vdr/icons/default/osdElements/* \
	${sysconfdir}/vdr/icons/default/recmenuicons/* \
	"
