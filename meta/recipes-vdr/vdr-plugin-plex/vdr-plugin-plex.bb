SUMMARY = "A Plex plugin for Linux VDR Plays files via the Plexmediaserver transcoder directly in VDR At the moment ONLY video files are played."
PV = "git"
PR = "r4"

LICENSE = "AGPL-3.0-only"

SRC_URI = "git://github.com/chriszero/vdr-plugin-plex.git;protocol=https;branch=master"
SRCREV = "6affa0ca83115dc92be2e020fe4af376b45217dc"

SRC_URI += " \
	file://00_plex_VDR271.patch \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
    poco \
    libskindesignerapi \
    libpcre \
	"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[${PLUGIN_NAME}]" > ${D}${sysconfdir}/vdr/conf.d/30_${PLUGIN_NAME}.conf
}

