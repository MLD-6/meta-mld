SUMMARY = "Channellists - A VDR Plugin to manage your Channellists"
PV = "git"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/vdr-projects/vdr-plugin-channellists.git;protocol=https;branch=master"
SRCREV = "4ecadb61fc469d662d1273ce26b1ab20ec291e31"

S = "${WORKDIR}/git"

inherit vdr-plugin

#DEPENDS += "vdr-font-symbols"

EXTRA_OEMAKE = ' \
	SDKSTAGE="${STAGING_DIR_HOST}" \
	'

do_install() {
	oe_runmake DESTDIR=${D} install
}
