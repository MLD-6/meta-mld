SUMMARY = "Imports the EPG of an other VDR."
AUTHOR = "Frank Schmirler"
PV = "1.0.1"
PR = "r5"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "http://vdr.schmirler.de/epgsync/vdr-epgsync-${PV}.tgz"
SRC_URI[sha256sum] = "b9e57e9c2dbebf20d5d193c84e991ae6e3a941db4b5b45780e239210f31a51e2"  

SRC_URI += " \
	file://vdr-2.3.2-epgsync-1.0.1_v2.patch \
	"

S = "${WORKDIR}/epgsync-${PV}"

inherit vdr-plugin

do_install() {
	oe_runmake DESTDIR=${D} install
}
