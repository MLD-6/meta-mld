SUMMARY = "VDR skin lcarsng (advanced version of skin LCARS"
HOMEPAGE = "https://gitlab.com/kamel5/skinlcarsng"
AUTHOR = "kamel5"

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://gitlab.com/kamel5/skinlcarsng.git;protocol=https;branch=master"

PV = "1.0+git"
SRCREV = "f41741f236c68a469f4c88ddd56e6e292a7797aa"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += "graphicsmagick"

do_install () {
	oe_runmake install 'DESTDIR=${D}'
}

