SUMMARY = "VDR tvguide plugin"
AUTHOR = "Stefan Braun <louis.braun@gmx.de>"
PV = "git"
PR = "r3"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://gitlab.com/kamel5/tvguide.git;protocol=https;branch=master"
SRCREV = "9cc17d87bd3d2f18825122d3ffb79425cc17817f"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
		graphicsmagick \
	"

EXTRA_OEMAKE = " \
	STRIP=/bin/true \
	SDKSTAGE="${STAGING_DIR_HOST}" \
	"

do_install() {
	oe_runmake DESTDIR=${D} install
	ln -sf /usr/share/vdr/channellogos/ ${D}/usr/share/vdr/plugins/tvguide/logos

}

FILES:${PN} += " \
	${datadir}/vdr/plugins/tvguide/* \
	${sysconfdir}/vdr/icons/default/* \
	${sysconfdir}/vdr/icons/default/osdElements/* \
	${sysconfdir}/vdr/icons/default/recmenuicons/* \
	"
