SUMMARY = "VDR plugin to provide a web interface for VDR"
HOMEPAGE = "https://github.com/Zabrimus/vdr-plugin-webbridge"
AUTHOR = "Zabrimus "

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a \
                    file://test/client/COPYING;md5=7ab2261f952d35fba9be4e1fe5b81a69 \
                    file://thirdparty/libsocket-2.4/LICENSE;md5=e39552db04434007136f1b37dd9c3711 \
                    file://thirdparty/uSockets-0.8.8/LICENSE;md5=86d3f3a95c324c9479bd8986968f4327 \
                    file://thirdparty/uWebSockets-20.65.0/LICENSE;md5=86d3f3a95c324c9479bd8986968f4327"

SRC_URI = "git://github.com/Zabrimus/vdr-plugin-webbridge.git;protocol=https;branch=tv"
SRCREV = "ead240923d41e70066c268c71b6714e5a098ec88"

SRC_URI += " \
	file://WebBridge.conf \
	"

# Modify these as desired
BPV = "0.1.0"
PR = "r1"
SRCREV = "${AUTOREV}"
PV = "${BPV}+git${SRCPV}"

S = "${WORKDIR}/git"

inherit vdr-plugin

do_configure () {
	# Specify any needed configure commands here
	:
}

do_compile () {
	# You will almost certainly need to add additional arguments here
	oe_runmake
	
	# Version ermitteln
	grep "\*VERSION" webbridge.cpp | sed -e 's/^[^\"]*\"\([^\"]*\)\".*$/\1/'
}

do_install () {
	# This is a guess; additional arguments may be required
	oe_runmake DESTDIR=${D} PREFIX=/usr install

	install -d ${D}${sysconfdir}/vdr/remote.conf.d
	install -m 0644 -D ${WORKDIR}/WebBridge.conf ${D}${sysconfdir}/vdr/remote.conf.d
}

FILES:${PN} += " \
	${sysconfdir}/vdr/remote.conf.d/* \
	"
