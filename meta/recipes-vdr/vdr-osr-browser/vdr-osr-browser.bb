DESCRIPTION = "A offscreen browser to render webpages for the vdr-plugin-hbbtv"
SUMMARY = "Offscreen browser for VDR"
AUTHOR = "Zabrimus"
SECTION = "x11/multimedia"

PV = "0.1.0+git${SRCPV}"
SRCREV = "68e7485612f82e48fe4e0f017e6a5b700ac84caf"
CEF_VERSION = "81.3.10+gb223419+chromium-81.0.4044.138"
#CEF_VERSION = "133.4.5+gdb28106+chromium-133.0.6943.142"

LICENSE = "GPL-2.0-only"

SRC_URI = " \
	git://github.com/Zabrimus/vdr-osr-browser.git;protocol=https;branch=master \
	https://cef-builds.spotifycdn.com/cef_binary_${@d.getVar('CEF_VERSION').replace('+', '%2B')}_linux64_minimal.tar.bz2;downloadfilename=cef_${CEF_VERSION}.tar.bz2 \
	"
SRC_URI[sha256sum] = "285f451be8e39a12f607d5b71e782fa0c1c91ce5b61a53daebbf0f9f16ebb05f"

SRC_URI += " \
	file://makefile_cmake.patch \
	"

S = "${WORKDIR}/git"

inherit pkgconfig

DEPENDS = "curl cmake-native libx11"
RDEPENDS:${PN} = "ffmpeg"

do_configure:append () {
	if [ ! -e ${S}/thirdparty/cef ]; then
		cp -r ${WORKDIR}/cef_binary_${CEF_VERSION}_linux64_minimal ${S}/thirdparty/cef
		ln -s ../include ${S}/thirdparty/cef/include/cef
	fi
}

do_compile () {
	oe_runmake prepare_release
	oe_runmake release
}

do_install () {
	install -d ${D}${datadir}/cef
	install -m 0755 ${S}/Release ${D}${datadir}/cef
}

FILES:${PN} += " \
	${bindir}/ \
	${datadir}/cef/ \
	"
