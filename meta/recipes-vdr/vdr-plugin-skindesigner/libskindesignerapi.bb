SUMMARY = "VDR SkinDesigner libskindesignerapi"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"
PV="1.2.8"

require skindesigner-common.inc

S = "${WORKDIR}/git/libskindesignerapi"

inherit pkgconfig gettext

DEPENDS = " \
	vdr \
"
RDEPENDS_${PN} += "bash"

TARGET_CC_ARCH += "${LDFLAGS}"

do_install() {
	oe_runmake DESTDIR=${D} PREFIX=/usr install-lib install-includes install-pc

}

