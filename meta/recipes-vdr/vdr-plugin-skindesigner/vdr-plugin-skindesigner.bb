SUMMARY = "VDR skindesigner plugin"
PV = "git"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

require skindesigner-common.inc

SRC_URI += " \
	file://vdr-skindesigner-makefile.patch \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	cairo \
	librsvg \
	libxml2 \
	jpeg \
	libpng  \
	curl \
	libskindesignerapi \
	lmsensors \
	"
RDEPENDS:${PN}+= "bash libskindesignerapi lmsensors"
RRECOMMENDS:${PN}+= "channellogos"

EXTRA_OEMAKE = ' SDKSTAGE="${STAGING_DIR_HOST}" '
TARGET_CC_ARCH += "${LDFLAGS}"

do_install() {
	oe_runmake DESTDIR=${D} PREFIX=/usr install-lib install-i18n install-themes install-skins install-scripts
	 	
	install -d ${D}/usr/share/fonts
	install -m 0755 ${S}/fonts/VDROpenSans/* ${D}/usr/share/fonts
    ln -sf /usr/share/vdr/channellogos/ ${D}/usr/share/vdr/plugins/skindesigner/logos
} 


FILES:${PN} += " \
	${sysconfdir}/vdr/themes/* \
	${datadir}/vdr/plugins/skindesigner/* \
	${datadir}/fonts \
	"
