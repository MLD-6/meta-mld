SUMMARY = "Skin soppalusikka for VDR"
AUTHOR = "Rolf Ahrenberg"
PV = "git"
PR = "r0"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://github.com/rofafor/vdr-plugin-skinsoppalusikka.git;protocol=https;branch=master"
SRCREV  = "0514a3f596b1d773fdf2635d65c9e109d05111ff"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += "vdr-font-symbols"

EXTRA_OEMAKE = " STRIP=/bin/true "

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/themes
	install -m 0755 ${S}/themes/*.theme ${D}${sysconfdir}/vdr/themes
	
	install -d ${D}/etc/vdr/plugins/skinsoppalusikka/logos
	install -m 0755 ${S}/symbols/*.xpm ${D}/etc/vdr/plugins/skinsoppalusikka/logos	
}

FILES:${PN} += " \
	${sysconfdir}/vdr/themes/* \
	${sysconfdir}/vdr/skinsoppalusikka/logos/* \
	"
