SUMMARY = "VDR Plugin undelete"
HOMEPAGE = "http://phivdr.dyndns.org/vdr/"
AUTHOR = "Petri Hintukainen <phintuka@users.sourceforge.net>"
DESCRIPTION = "Plugin to recover deleted recordings (works only within 5 minutes of deleting them)"

PV = "2.3.1-pre1"

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "http://phivdr.dyndns.org/vdr/vdr-undelete/vdr-undelete-${PV}.tgz"
SRC_URI[sha256sum] = "d2683ddeec6225e7e9ad56bcb18b23a165d1b01696262efb2844b32d457db8b2"

S = "${WORKDIR}/undelete-${PV}"

inherit vdr-plugin

sharedir = "${prefix}/share"

do_compile () {
	# You will almost certainly need to add additional arguments here
	oe_runmake
}

do_install () {
	# This is a guess; additional arguments may be required
	oe_runmake install 'DESTDIR=${D}'
}

