#!/bin/sh

case "$1" in
	start)
		args=$(setting get -d "-m /etc/vdr/plugins/iptv/ -y /usr/bin/yt-dlp" vdr.plugins.iptv.args)

		devices=$(setting get -d 1 vdr.plugins.iptv.count)
		args="$args -d $devices"

		echo -e "[iptv]\n$args" > /etc/vdr/conf.d/50_iptv.conf
		;;
esac
