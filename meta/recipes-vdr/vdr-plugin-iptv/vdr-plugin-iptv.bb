SUMMARY = "This is an IPTV plugin for the Video Disk Recorder (VDR)."
AUTHOR = " Rolf Ahrenberg  Rolf.Ahrenberg@sci.fi \
           Antti Seppälä  a.seppala@gmail.com"
PV = "2.6.6"
PR = "r4"

LICENSE = "GPL-2.0-only"

SRC_URI = " \
	git://github.com/Zabrimus/vdr-plugin-iptv.git;protocol=https;branch=master \
	git://github.com/Zabrimus/vdr-plugin-iptv-samples.git;protocol=https;branch=master;name=channellists;destsuffix=channellists \
	"
SRCREV_default = "e5ef0d4d35a129cf08145d4509bc83fdc3bc0ca7"
SRCREV_channellists = "8ae2607feae77745bf2f990f5f197dc500c9a111"
SRCREV_FORMAT = "default channellists"

SRC_URI += " \
	file://IPTV_MagentaTV.conf \
	file://run.sh \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	curl \
	vlc \
	"
RDEPENDS:${PN} += " \
	vlc \
	"
RRECOMMENDS:${PN} += " \
	ffmpeg \
	"
#RRECOMMENDS:${PN} += "yt-dlp"


EXTRA_OEMAKE = " \
	STRIP=/bin/true \
	"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/channels
	install -m 0644 ${WORKDIR}/IPTV_MagentaTV.conf ${D}${sysconfdir}/vdr/channels/
	install -m 0644 ${WORKDIR}/channellists/samples/kodinerds/kodi_tv_main.channels.conf ${D}${sysconfdir}/vdr/channels/IPTV_FreeTV-DE.conf
	(IFS=:; while read a u; do sed "s#F=M3U|U=.*|A=$a|#F=M3US|U=$(echo "$u" | sed 's/:/%3A/g')|A=$a|#" -i ${D}${sysconfdir}/vdr/channels/IPTV_FreeTV-DE.conf; done < ${WORKDIR}/channellists/samples/kodinerds/kodi_tv_main.cfg)
	install -d ${D}${sysconfdir}/vdr/plugins/iptv

	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/50_iptv.sh
}

FILES:${PN} += " \
	${sharedir}/vdr/plugins/iptv/ \
	${sysconfdir}/vdr/ \
	${sharedir}/vdr/run.d/ \
	"
