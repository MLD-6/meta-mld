SUMMARY = "VDR skincurses plugin"
HOMEPAGE = "http://www.tvdr.de"
AUTHOR = "Klaus Schmidinger"
PV = "2.5.6"
PR = "r0"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "file://src"

SRC_URI += " \
	file://run.sh \
	file://frontend.sh \
	file://vdr-plugin-skincurses.menu \
	"

S = "${WORKDIR}/src"

inherit vdr-plugin

RREPLACES:${PN} += "vdr-plugin-frontend"
RCONFLICTS:${PN} += "vdr-plugin-frontend"
RPROVIDES:${PN} += "vdr-plugin-frontend"

do_install() {
	oe_runmake DESTDIR=${D} install-lib install-i18n

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[${PLUGIN_NAME}]" > ${D}${sysconfdir}/vdr/conf.d/30_${PLUGIN_NAME}.conf
	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_skincurses.sh
	install -d ${D}${sharedir}/vdr/frontend.d
	install -m 0755 ${WORKDIR}/frontend.sh  ${D}${sharedir}/vdr/frontend.d/50_skincurses.sh
	install -d ${D}${sharedir}/menu
	install -m 0644 ${WORKDIR}/vdr-plugin-skincurses.menu ${D}${sharedir}/menu
}

FILES:${PN} += " \
	${sharedir}/vdr/run.d/* \
	${sharedir}/vdr/frontend.d/* \
	${sharedir}/menu/* \
	"

pkg_postinst:${PN} () {
	if [ -e $D/boot/syslinux/syslinux.cfg ]; then
  	sed "s/vga=current/vga=0x314/g" -i $D/boot/syslinux/syslinux.cfg
	fi
}
