#!/bin/sh

case "$1" in
  start)
    ${0%/*/*}/frontend.sh resume
    trap 'trap "" TERM; kill -s TERM -- -$$' TERM # kill childs on SIGTERM
    tail -f /dev/null & wait # infiny wait
    ${0%/*/*}/frontend.sh suspend
    ;;
  resume)
     ;;
  suspend)
    ;;
  stop)
    ;;
esac
