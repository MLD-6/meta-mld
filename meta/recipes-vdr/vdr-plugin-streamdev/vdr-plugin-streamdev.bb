SUMMARY = "VDR streamdev plugin"
SECTION = "mld/console/multimedia"
PV = "git"
PR = "r6"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

SRCREV = "e2a9b979d3fb92967c7a6a8221e674eb7e55c813"
SRC_URI = "git://github.com/vdr-projects/vdr-plugin-streamdev;protocol=https;branch=master"

SRC_URI += " \
	file://15_streamdev_VDR250.patch \
	"

SRC_URI += " \
	file://run.sh \
	file://externremux.sh \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

RDEPENDS:${PN}-client = "${RDEPENDS:${PN}}"
RDEPENDS:${PN}-server = "${RDEPENDS:${PN}}"
RRECOMMENDS:${PN}-client +="vdr-plugin-svdrpservice vdr-plugin-epgsync"
RRECOMMENDS:${PN}-client +="nfs-client"
RRECOMMENDS:${PN}-server +="nfs-server"

do_install() {
	oe_runmake DESTDIR=${D} LIBDIR=/usr/lib/vdr install

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[streamdev-server]" > ${D}${sysconfdir}/vdr/conf.d/50_streamdev-server.conf
	echo "[streamdev-client]" > ${D}${sysconfdir}/vdr/conf.d/50_streamdev-client.conf
	install -d ${D}${sysconfdir}/vdr/plugins/streamdev-server
	ln -s ../../svdrphosts.conf ${D}${sysconfdir}/vdr/plugins/streamdev-server/streamdevhosts.conf
	install -m 0755 ${WORKDIR}/externremux.sh ${D}${sysconfdir}/vdr/plugins/streamdev-server/externremux.sh
	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_streamdev-client.sh
}

pkg_postinst:${PN}() {
#!/bin/sh
mkdir -p /etc/vdr/channels
touch /etc/vdr/channels/Server-channellist.conf
}

PACKAGES = " ${PN}-client ${PN}-server ${PN}-client-dbg ${PN}-server-dbg"

FILES:${PN}-server = " \
	${libdir}/vdr/libvdr-streamdev-server* \
	${sysconfdir}/vdr/conf.d/50_streamdev-server.conf \
	${sysconfdir}/vdr/plugins/streamdev-server/streamdevhosts.conf \
	${sysconfdir}/vdr/plugins/streamdev-server/externremux.sh \
	"
FILES:${PN}-client = " \
	${libdir}/vdr/libvdr-streamdev-client* \
	${sysconfdir}/vdr/conf.d/50_streamdev-client.conf \
	${sharedir}/vdr/run.d/30_streamdev-client.sh \
	"

FILES:${PN}-client-dbg = "${libdir}/vdr/.debug/libvdr-streamdev-client*"
FILES:${PN}-server-dbg = "${libdir}/vdr/.debug/libvdr-streamdev-server*"

#FILES:${PN}-client-locale = "${datadir}/locale/*/*/*-client.mo"
#FILES:${PN}-server-locale = "${datadir}/locale/*/*/*-server.mo"
