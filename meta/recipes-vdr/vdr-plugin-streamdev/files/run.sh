#!/bin/sh

case "$1" in
	start)
		server_ip=$(setting get vdr.plugins.streamdev_client.server_ip)

		if [ ! "$server_ip" ]; then
			echo "Serach server:"
			/lib/systemd/systemd-networkd-wait-online --timeout 30 -i eth0 -i wlan0 --any
			server_ip=$(setting get 'vdr.plugins.streamdev_client.servers()[0]')
			echo "Found server IP: $server_ip"
			setting set vdr.plugins.streamdev_client.server_ip \"$server_ip\"
		fi
		if [ "$server_ip" ]; then
			name="$(nslookup $server_ip | sed -n "s/.*$server_ip \([a-zA-Z0-9\-]\+\).*/\1/p")"
			if ! grep -q "SVDRPDefaultHost = $name" /etc/vdr/setup.conf; then
				echo "Update server name: $name"
				cat >> /etc/vdr/setup.conf.add <<- EOF
					!SVDRPPeering = 2
					!SVDRPDefaultHost = $name
					!streamdev-client.StartClient = 1
					streamdev-client.StreamFilters = 1
					streamdev-client.HideMenuEntry = 1
				EOF
			fi
			if ! grep -q "streamdev-client.RemoteIp = $server_ip" /etc/vdr/setup.conf; then
				echo "Update server IP: $server_ip"
				echo "!streamdev-client.RemoteIp = $server_ip" >> /etc/vdr/setup.conf.add
			fi		
		fi

		if [ "$server_ip" ]; then
			echo "Wait for network connection:"
			/lib/systemd/systemd-networkd-wait-online --timeout 30 -i eth0 -i wlan0 --any

			if $(setting -d false get vdr.plugins.streamdev_client.start_server); then
				server_mac=$(setting get vdr.plugins.streamdev_client.server_mac)
				if [ "$server_mac" ]; then
					echo "Start streamdev server:"
					setting call network.wake $server_mac
				fi
			fi

			if $(setting -d false get vdr.plugins.streamdev_client.wait_for_server); then
				echo "Wait for streamdev server:"
				timeout 90 sh -c "while [ -z '$(svdrpsend.sh -H $server_ip QUIT)' ]; do sleep 1; done"
			fi

			if [ "$(setting get vdr.channels.active)" = "Server-channellist" ] && echo "QUIT" | nc $server_ip 6419 >/dev/null 2>&1; then
				echo "Get channellist from server:"
				echo -e "LSTC :groups\nQUIT" | nc $server_ip 6419 2>/dev/null | sed 's/\x0D$//' | sed -n "s/250.[^ ]\+ \(.*\)/\1/p" > /etc/vdr/channels/Server-channellist.conf
			fi
		fi
		;;
esac
