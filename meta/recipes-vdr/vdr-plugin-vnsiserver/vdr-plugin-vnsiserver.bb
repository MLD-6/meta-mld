SUMMARY = "VDR plugin to handle KODI clients."
AUTHOR = "Rainer Hochecker  fernetmenta@online.de"
PV = "1.8.0"
PR = "r2"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/FernetMenta/vdr-plugin-vnsiserver.git;protocol=https;branch=master"
SRCREV = "a06ad5fbd4f5d2d547a17db291178d113dfbc699"

S = "${WORKDIR}/git"

inherit vdr-plugin

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/plugins/vnsiserver
	ln -s ../../svdrphosts.conf ${D}${sysconfdir}/vdr/plugins/vnsiserver/allowed_hosts.conf
}
