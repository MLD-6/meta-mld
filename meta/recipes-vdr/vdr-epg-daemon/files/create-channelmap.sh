#!/bin/sh

# create channelmap.conf for epg-daemon from VDR channels.conf

CHANNELS_CONF="/etc/vdr/channels.conf"
EPGDID_LIST="/etc/epgd/channelmap-ids.csv"
CHANNEL_MAP="/etc/epgd/channelmap.conf"

if [ ! -e $CHANNELS_CONF ]; then
  echo "$CHANNELS_CONF not found!"
  exit
fi

lines=$(setting get -d 50 epgDaemon.lines)
sources="$(setting get -d "tvm tvsp vdr" epgDaemon.sources)"
merge=$(setting get -d true epgDaemon.merge)

cut -d":" -f1,4,10,11,12 $CHANNELS_CONF | head -n $lines | sed '/^$/d' | sed '/^\./d' | while IFS=: read -r a b c d e; do
  vdr_name="$(echo "$a" | cut -d ";" -f1 | cut -d "," -f1)"
  vdr_id="$b-$d-$e-$c"

  IFS=, read tvm tvsp <<< $(grep -i "'$vdr_name'" $EPGDID_LIST | cut -d "," -f 2,4)
  vdr=vdr:000

  echo "// $vdr_name"

  prio=$($merge && echo 1 || echo 0)
  for source in $sources; do
    if [ "${!source}" ]; then
      echo "$source:${!source}:$prio = $vdr_id"
      [ $prio = 0 -o $prio = 2 ] && break
      prio=$(($prio + 1))
    fi
  done
done > $CHANNEL_MAP
