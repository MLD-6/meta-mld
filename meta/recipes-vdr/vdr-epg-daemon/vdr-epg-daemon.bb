SUMMARY = "VDR Electronic Program Guide (EPG) Daemon"
SECTION = "multimedia"
LICENSE = "GPL-2.0-only"
PR = "r1"

SRC_URI += " \
	file://docker-compose.yml \
	file://epgd.conf \
	file://create-channelmap.sh \
	file://channelmap-ids.csv \
	"

RDEPENDS:${PN} = "podman podman-compose"
RRECOMMENDS:${PN} = "channellogos"

do_install () {
	install -d ${D}${datadir}/epgd
	install -m 0644 ${WORKDIR}/docker-compose.yml ${D}${datadir}/epgd

	install -d ${D}${sysconfdir}/epgd
	install -m 0644 ${WORKDIR}/channelmap-ids.csv ${D}${sysconfdir}/epgd
	install -m 0644 ${WORKDIR}/epgd.conf ${D}${sysconfdir}/epgd

	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/create-channelmap.sh ${D}${bindir}
}

CONFFILES:${PN} += " \
  ${sysconfdir}/epgd \
  "
FILES:${PN} += " \
	${datadir}/epgd \
  ${sysconfdir}/epgd \
	${bindir} \
	"

pkg_postinst:${PN}:append () {
	if [ -z "$D" ]; then
		if ! id -u epgd 2>/dev/null >&2; then
			useradd --system --home / --no-create-home --shell /bin/false --user-group epgd
		fi
		create-channelmap.sh || true
		cd ${datadir}/epgd
		echo "PUID=$(id -u epgd)" > .env
		echo "PGID=$(id -g epgd)" >> .env
		podman compose up -d
	fi
}
