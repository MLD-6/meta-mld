DESCRIPTION = "VDR ddci2 plugin"
SUMMARY = "Creates DVB CI adapters from "stand alone" ca-devices (e.g. DigitalDevices)"
HOMEPAGE = "https://github.com/jasmin-j/vdr-plugin-ddci2"
AUTHOR = "Jasmin Jessich <jasmin@anw.at"
SECTION = "extra"
#COMPATIBLE_MACHINE = "(genericx86-64)"

PACKAGE_ARCH = "${TUNE_PKGARCH}_extra"

PR = "r0"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

SRC_URI = "git://github.com/jasmin-j/vdr-plugin-ddci2.git;protocol=https;branch=master"

PV = "1.0+git${SRCPV}"
SRCREV = "05dd98824092859afd2aa7a4996c8f258affd975"

S = "${WORKDIR}/git"

inherit vdr-plugin

do_install () {
	oe_runmake  DESTDIR=${D} install
}

