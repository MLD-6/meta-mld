SUMMARY = "VDR neutrinoepg plugin"
PV = "git"
PR="r4"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRCREV = "c3615ffcb0f5e5c2d380052fffd4a5d53293cde3"
SRC_URI = "git://vdr-projects.e-tobi.net/git/vdr-plugin-neutrinoepg;protocol=https;branch=master"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += "vdr-font-symbols"

do_install() {
	oe_runmake DESTDIR=${D} install
}
