SUMMARY = "VDR mcli plugin o access DVB-streams produced by the NetCeiver-hardware"
AUTHOR = "Peter Bieringer <pb@bieringer.de>"
SECTION = "vdr"
PV = "0.9.7"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/vdr-projects/vdr-plugin-mcli.git;protocol=https;branch=master"
SRCREV = "0e6fc80f8a38d40e0174c19996e435bed943e9f7"

SRC_URI += " \
	file://run.sh \
	file://vdr-mcli-makefile.patch \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	gettext \
	libxml++ \
	"

EXTRA_OEMAKE = ' \
    SDKSTAGE="${STAGING_DIR_HOST}" \
	'

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_mcli.sh
}

FILES:${PN} += " \
	${sharedir}/vdr/run.d/* \
	"
