#!/bin/sh

case "$1" in
	start)
		args=$(setting get vdr.plugins.mcli.args)

		if [ "$args" ]; then
			echo -e "[satip]\n$args" > /etc/vdr/conf.d/50_mcli.conf
		fi
		;;
esac
