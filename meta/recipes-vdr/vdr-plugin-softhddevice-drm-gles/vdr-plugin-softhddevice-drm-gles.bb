SUMMARY = "VDR HD output device "
PV = "git"
PR = "r8"
COMPATIBLE_MACHINE = "(hardkernel-odroidn2plus|hardkernel-odroid-n2l|raspberrypi4|raspberrypi5|rock-pi-4b)"

LICENSE = "AGPL-3.0-only"


SRC_URI = "git://github.com/rellla/vdr-plugin-softhddevice-drm-gles.git;protocol=https;branch=drm-atomic-gles"
SRCREV = "b45194ae9e990a80f1228e3d94226274b60fcd2b"

SRC_URI += " \
	file://frontend.sh \
	file://run.sh \
	file://vdr-plugin-softhddevice-drm-gles.menu \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

RREPLACES:${PN} += "vdr-plugin-frontend"
RCONFLICTS:${PN} += "vdr-plugin-frontend"
RPROVIDES:${PN} += "vdr-plugin-frontend"

DEPENDS += " \
	ffmpeg \
	freetype \
	virtual/libgl \
	virtual/libgles2 \
	virtual/egl \
	libmms \
	glm \
	"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[${PLUGIN_NAME}]" > ${D}${sysconfdir}/vdr/conf.d/30_${PLUGIN_NAME}.conf
	install -d ${D}${sharedir}/vdr/frontend.d
	install -m 0755 ${WORKDIR}/frontend.sh  ${D}${sharedir}/vdr/frontend.d/50_softhddevice-drm-gles.sh
	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_softhddevice-drm-gles.sh
	install -d ${D}${sharedir}/menu
	install -m 0644 ${WORKDIR}/vdr-plugin-softhddevice-drm-gles.menu ${D}${sharedir}/menu
}

FILES:${PN} += " \
	${sharedir}/vdr/frontend.d/* \
	${sharedir}/vdr/run.d/* \
	${sharedir}/menu/* \
	" 
