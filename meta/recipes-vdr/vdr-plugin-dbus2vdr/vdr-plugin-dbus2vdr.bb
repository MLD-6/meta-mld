SUMMARY = "VDR satip plugin"
AUTHOR = "Rolf Ahrenberg"
SECTION = "mld/console/multimedia"
PV = "git"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = "\
	file://10_makefile.patch \
	file://de.tvdr.vdr.conf \
	git://github.com/seahawk1986/vdr-plugin-dbus2vdr.git;protocol=https;branch=master\
	"
SRCREV = "560ff605e65e735af4a60d7eb62973b23c45a0a4"


S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	dbus \
    libpng \
    jpeg \
	"

EXTRA_OEMAKE = " \
	SDKSTAGE="${STAGING_DIR_HOST}" \
	STRIP=/bin/true \
	'DEFINES += -DNO_PNGPP' \
	"

do_install() {
	oe_runmake DESTDIR=${D} install
	
	install -m 0644 -D ${WORKDIR}/de.tvdr.vdr.conf ${D}${sysconfdir}/dbus-1/system.d/de.tvdr.vdr.conf
}

