SUMMARY = "VDR control plugin"
DESCRIPTION = "The control plugin brings the ability to VDR to control the hole OSD over a telnet client on port 2002."
AUTHOR = "Jan Rieger <vdr@ricomp.de> (up to 0.0.2a) \
					Urig @ vdr-portal          (0.0.2a-kw3 - fork from 0.0.2a) \
					Winfried Koehler <nvdec A.T. quantentunnel D.O.T. de > (2021.03.04-wirbel - once again forked"

SECTION = "mld/console/multimedia"
PV = "git"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/wirbel-at-vdr-portal/vdr-plugin-control.git;protocol=https;branch=main"
SRCREV = "09fb0a510445041c5df875452c736816e2a1b298"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	curl \
	pugixml \
	openssl \
	"

EXTRA_OEMAKE = " \
	STRIP=/bin/true \
	"

do_install() {
	oe_runmake DESTDIR=${D} install-lib install-i18n
}
