SUMMARY = "VDR restfulapi plugin"
DESCRIPTION = "A VDR plugin to provide functions for a web remote control"
HOMEPAGE = "https://www.yavdr.org"
PV = "git"
PR = "r1"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://github.com/yavdr/vdr-plugin-restfulapi.git;protocol=https;branch=master"
SRCREV = "be8a3a60af7e8926cb28c06e6b6d2adc2c2ed968"

SRC_URI += " \
	file://10_restfulapi_dvb_t2.patch \
	file://11_restfulapi_housekeeping_busybox.patch \
	file://12_info_channel.patch \
	file://13_disable_signal_info.patch \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += "cxxtools"

sharedir = "${prefix}/share"

EXTRA_OEMAKE = " \
	USE_LIBMAGICKPLUSPLUS=0 \
	"
CXXFLAGS += "-g"

do_install() {
	oe_runmake DESTDIR=${D} install-lib install-i18n
}
