SUMMARY = "VDR skinflatplus plugin"
AUTHOR = "vdr.skinflatplus@schirrmacher.eu"
PV = "git"
PR = "r13"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://github.com/MegaV0lt/vdr-plugin-skinflatplus.git;protocol=https;branch=master"
SRCREV = "c6ec36fcfdd74399d458c6238827dcdb7c9031fa"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	curl \
	jansson \
	graphicsmagick \
	"
RRECOMMENDS:${PN} += "jq"

EXTRA_OEMAKE = " \
	STRIP=/bin/true \
	SDKSTAGE="${STAGING_DIR_HOST}" \
	"

do_install() {
	oe_runmake DESTDIR=${D} install
	install -d ${D}${sharedir}/vdr/plugins/skinflatplus/icons/default
	install -m 0755 ${S}/icons/default/*.png ${D}${sharedir}/vdr/plugins/skinflatplus/icons/default
	ln -sf ${sharedir}/vdr/channellogos/ ${D}${sharedir}/vdr/plugins/skinflatplus/logos
}

FILES:${PN} += " \
	${sysconfdir}/vdr/themes/ \
	${sharedir}/vdr/plugins/skinflatplus/ \
	"
