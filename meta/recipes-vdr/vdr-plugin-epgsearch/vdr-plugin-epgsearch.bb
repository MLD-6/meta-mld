SUMMARY = "VDR epgsearch plugin"
HOMEPAGE = "https://www.yavdr.org"
PV = "git"
PR = "r1"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRCREV = "ea061dfa957112c8e4c32ee304fe8a425429b836"
SRC_URI = "git://salsa.debian.org/vdr-team/vdr-plugin-epgsearch.git;protocol=https;branch=master"

SRC_URI += " \
	file://00_epgsearch_VDR271.patch \
	file://10_epgsearch_menue.patch \
	file://epgsearchcats.conf \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

RDEPENDS:${PN} += "perl"

EXTRA_OEMAKE = " \
	AUTOCONFIG=0 \
	"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -m 0644 -D ${S}/scripts/*.conf ${D}${sysconfdir}/vdr/plugins
	install -m 0644 ${WORKDIR}/epgsearchcats.conf ${D}${sysconfdir}/vdr/plugins/epgsearch
	rmdir ${D}${sysconfdir}/vdr/plugins/epgsearch/conf.d

	install -m 0755 -D ${S}/scripts/*.pl ${D}${bindir}/
	install -m 0755 -D ${S}/scripts/*.sh ${D}${bindir}/

	install -d ${D}${localstatedir}/lib/dpkg/info
}

CONFFILES:${PN} += " \
    ${sysconfdir}/vdr/plugins/epgsearch/* \
    "
BACKUPFILES:${PN} += " \
		${sysconfdir}/vdr/plugins/epgsearch/epgsearch.conf \
		${sysconfdir}/vdr/plugins/epgsearch/epgsearchcats.conf \
		${sysconfdir}/vdr/plugins/epgsearch/epgsearchmenu.conf \
		${sysconfdir}/vdr/plugins/epgsearch/timersdone.conf \
    "

FILES:${PN} += "  \
	${sysconfdir}/vdr/plugins/epgsearch/* \
	"
