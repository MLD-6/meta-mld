SUMMARY = "VDR extrecmenung plugin"
PV = "git"
PR = "r3"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://gitlab.com/kamel5/extrecmenung.git;protocol=https;branch=master"
SRCREV = "cfb55db2da37e2fd5daa9d14143f36bf057d9b9f"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += "vdr-font-symbols"

EXTRA_OEMAKE = ' \
	SDKSTAGE="${STAGING_DIR_HOST}" \
	'

do_install() {
	oe_runmake DESTDIR=${D} install
}
