SUMMARY = "VDR Plugin to activated an imon LCD with VDR Information"
HOMEPAGE = "https://github.com/vdr-projects/vdr-plugin-imonlcd/tree/master"
AUTHOR = "Andreas Brachold <vdr07@deltab.de>"
DESCRIPTION = "Plugin to activated an imon LCD with VDR Information"
PV = "1.0.3+git${SRCPV}"

LICENSE = "GPL-2.0-or-later"

SRC_URI = "git://github.com/vdr-projects/vdr-plugin-imonlcd.git;protocol=https;branch=master"
SRCREV = "529b0bcd90af649a0e7b8907f1f76910e2f77505"

S = "${WORKDIR}/git"

inherit vdr-plugin

do_install() {
	oe_runmake DESTDIR=${D} install
}
