SUMMARY = "VDR fritzbox plugin"
HOMEPAGE = "https://github.com/jowi24/vdr-fritz"
AUTHOR = "Joachim Wilke"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a \
                    file://libconv++/COPYING;md5=751419260aa954499f7abaabaa882bbe \
                    file://libfritz++/COPYING;md5=751419260aa954499f7abaabaa882bbe \
                    file://liblog++/COPYING;md5=751419260aa954499f7abaabaa882bbe \
                    file://libnet++/COPYING;md5=751419260aa954499f7abaabaa882bbe"

SRC_URI = "gitsm://github.com/jowi24/vdr-fritz.git;protocol=https;branch=master"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "368a393a3e56f49d5f8b3c4411acf511e1796690"

SRC_URI += " \
	file://00_fritzbox_VDR271.patch \
	"

S = "${WORKDIR}/git"

# NOTE: some of these dependencies may be optional, check the Makefile and/or upstream documentation
DEPENDS = "boost libjpeg-turbo libgcrypt glibc-locale"

# needed for Conversion to UTF System
RDEPENDS:${PN} += "glibc-gconv-utf-16"

inherit vdr-plugin

do_configure () {
	# nothing required
	:
}

do_compile () {
	# nothing required
	oe_runmake
}

do_install () {
	# nothing required
	oe_runmake install 'DESTDIR=${D}'
}
