SUMMARY = "VDR recsearch plugin"
PV = "git"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/flensrocker/vdr-plugin-recsearch.git;protocol=https;branch=master"
SRCREV = "071ca720d59638dccd3723292c5970ee8352ad30"

S = "${WORKDIR}/git"

inherit vdr-plugin

#DEPENDS += "vdr-font-symbols"

EXTRA_OEMAKE = ' \
	SDKSTAGE="${STAGING_DIR_HOST}" \
	'

do_install() {
	oe_runmake DESTDIR=${D} install
}
