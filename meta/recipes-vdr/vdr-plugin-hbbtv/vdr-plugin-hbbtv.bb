SUMMARY = "VDR hddtv plugin"
PV = "git"
PR = "r0"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/Zabrimus/vdr-plugin-hbbtv.git;protocol=https;branch=master"
SRCREV = "5a92e99f12ed0e5ae95cbff5bc75f28d4abe9876"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += "ffmpeg cmake-native"

do_install() {
	oe_runmake DESTDIR=${D} install-lib install-i18n

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[${PLUGIN_NAME}]\n-s -p /usr/local/bin/hbbtv.sh" > ${D}${sysconfdir}/vdr/conf.d/50_${PLUGIN_NAME}.conf
}
