#!/bin/sh
#
# Sendet eine Nachricht an den VDR

DAEMON=false
HOST=localhost
while [ $# != 0 ] ; do
  case $1 in
    -d) DAEMON=true
      shift
      ;;
    -H) HOST=$2
      shift 2
      ;;
    -h) ARGS=
      break
      ;;
    *) ARGS=$ARGS" $1"
      shift
      ;;
  esac
done

if [ -z "$ARGS" ] ; then
  echo "Usage: $0 [-H HOSTNAME] [-d] command..."
  echo "  -H HOSTNAME        destination hostname or IP (default: localhost)"
  echo "  -d                 run in background"
  exit
fi

if $DAEMON; then
  svdrpsend $HOST 6419 "$ARGS" 1>/dev/null 2>&1 &
else
  svdrpsend $HOST 6419 "$ARGS" 2>/dev/null
fi
