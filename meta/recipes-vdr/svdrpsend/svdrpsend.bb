DESCRIPTION = "Send commands to the Video Disk Recorder (VDR)."
SUMMARY = "svdrpsend"
HOMEPAGE = "http://open7x0.org"
SECTION = "console/multimedia"
LICENSE = ""
LIC_FILES_CHKSUM = "file://svdrpsend.c;endline=15;md5=a668599d0b8622165b7a5856b345c6a3"

SRC_URI = " \
	file://svdrpsend.c \
	file://svdrpsend.sh \
	"

S = "${WORKDIR}"

do_compile () {
	${CC} svdrpsend.c -o svdrpsend ${CFLAGS} ${LDFLAGS}
}

do_install () {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/svdrpsend  ${D}${bindir}
	install -m 0755 ${WORKDIR}/svdrpsend.sh   ${D}${bindir}
}
