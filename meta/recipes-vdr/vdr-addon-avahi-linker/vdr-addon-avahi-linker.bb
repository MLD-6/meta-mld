SUMMARY = "Link network exports automaticaly"
DESCRIPTION = "Link network exports automaticaly to data sub dirs, if exports present."
PV = "git"
PR = "r5"

LICENSE = "CLOSED"

SRC_URI = " \
	file://10_vdr-update-monitor.patch \
	file://11_avahi-linker.patch \
	file://dbus2vdr.py \
	file://default.cfg \
	file://staticmount.cfg \
	file://targetdirs.cfg \
	file://vdr-addon-avahi-linker.service \
	file://vdr-net-monitor.service \
	file://vdr-update-monitor.service \
	file://prevent-umount-on-pause.service \
	file://nfs-client.target \
	git://github.com/yavdr/vdr-addon-avahi-linker.git;protocol=https;branch=master \
	"
SRCREV = "d41138555ae6fa96e7c6d667172b8fd1af3841bb"

S = "${WORKDIR}/git"

inherit systemd python3-dir

RDEPENDS:${PN} += " \
	python3-core \
	python3-avahi \
	python3-pygobject \
	python3-dbus \
	nfs-server \
	autofs \
	avahi-daemon \
	avahi-autoipd \
	avahi-dnsconfd \
	avahi-utils \
	dbus \
	vdr-plugin-dbus2vdr \
	" 

DEPENDS += " \
	python3-avahi \
	python3-pygobject \
	"

do_install() {
	oe_runmake DESTDIR=${D} all

	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/vdr-addon-avahi-linker.service ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/nfs-client.target ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/vdr-net-monitor.service ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/vdr-update-monitor.service ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/prevent-umount-on-pause.service ${D}${systemd_unitdir}/system

	install -d ${D}${sysconfdir}/avahi-linker
	install -m 0644 ${WORKDIR}/default.cfg ${D}${sysconfdir}/avahi-linker/default.cfg
	install -m 0644 ${WORKDIR}/staticmount.cfg ${D}${sysconfdir}/avahi-linker/staticmount.cfg
	install -m 0644 ${WORKDIR}/targetdirs.cfg ${D}${sysconfdir}/avahi-linker/targetdirs.cfg

	install -d ${D}${bindir}/
	install -m 0755 ${S}/usr/bin/avahi-linker ${D}${bindir}/avahi-linker
	install -m 0755 ${S}/usr/bin/vdr-net-monitor ${D}${bindir}/vdr-net-monitor
	install -m 0755 ${S}/usr/bin/prevent-umount-on-pause ${D}${bindir}/prevent-umount-on-pause
	install -m 0755 ${S}/usr/bin/vdr-update-monitor ${D}${bindir}/vdr-update-monitor

	install -m 0755 -D ${WORKDIR}/dbus2vdr.py ${D}${libdir}/python${PYTHON_BASEVERSION}/dbus2vdr.py
}

SYSTEMD_SERVICE:${PN} = "vdr-addon-avahi-linker.service nfs-client.target vdr-net-monitor.service vdr-update-monitor.service prevent-umount-on-pause.service" 

CONFFILES:${PN} += " \
	${sysconfdir}/avahi-linker/* \
	"
FILES:${PN} += " \
	${systemd_unitdir}/system/* \
	${bindir}/avahi-linker \
	${sysconfdir}/avahi-linker/* \
	${libdir}/python${PYTHON_BASEVERSION}/* \
	"

pkg_postinst:${PN}:append() {
	sed "s/-hosts/\/etc\/auto.net/" -i $D/etc/auto.master
}

pkg_prerm:${PN}:append() {
	sed "s/\/etc\/auto.net/-hosts/" -i $D/etc/auto.master
}
