SUMMARY = "VDR remote plugin"
AUTHOR = "Oliver Endriss"
PV = "0.7.0"
PR="r7"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "http://www.escape-edv.de/endriss/vdr/vdr-remote-${PV}.tgz"
SRC_URI[sha256sum] = "e90a065db896e030dbb8c8dd99b9b70f3e3ade1017bedcbe6c50e3ce53512af0"

SRC_URI += " \
	file://55-x10.rules \
	"

S = "${WORKDIR}/remote-${PV}"

inherit vdr-plugin

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/udev/rules.d 
	install -m 0755 ${WORKDIR}/55-x10.rules ${D}${sysconfdir}/udev/rules.d 
}

FILES:${PN} += " \
	${sysconfdir}/udev/rules.d/55-x10.rules \
	"
