SUMMARY = ""
PV = "git"
PR = "r1"

LICENSE = "AGPL-3.0-only"

SRC_URI = "git://github.com/horchi/vdr-plugin-seduatmo.git;protocol=https;branch=master"
SRCREV = "7fa415dff944eab25c74e4e07d64473a101661e5"


S = "${WORKDIR}/git"

inherit vdr-plugin

#DEPENDS += " \
#    poco \
#    libskindesignerapi \
#	"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[${PLUGIN_NAME}]" > ${D}${sysconfdir}/vdr/conf.d/30_${PLUGIN_NAME}.conf
}

