SUMMARY = "VDR satip plugin"
AUTHOR = "Rolf Ahrenberg"
SECTION = "mld/console/multimedia"
PV = "git"
PR = "r24"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://github.com/rofafor/vdr-plugin-satip.git;protocol=https;branch=master"
SRCREV = "02a842f95a09a74d7eba90648c97693638007141"

SRC_URI += " \
	file://run.sh \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	curl \
	pugixml \
	openssl \
	"

EXTRA_OEMAKE = " \
	STRIP=/bin/true \
	"

do_install() {
	oe_runmake DESTDIR=${D} install-lib install-i18n

	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_satip.sh
}

FILES:${PN} += " \
	${sharedir}/vdr/run.d/* \
	"
