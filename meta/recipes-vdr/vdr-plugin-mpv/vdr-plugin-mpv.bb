SUMMARY = "Mediaplayer plugin for VDR"
AUTHOR = "ua0lnj"
PV = "1.8.1"
PR = "r6"

LICENSE = "AGPL-3.0-only"

SRC_URI = "git://github.com/ua0lnj/vdr-plugin-mpv.git;protocol=https;branch=master"
SRCREV = "fd34db6fa562e3a3af8f815dcaf19c99d58d517f"
SRC_URI[sha256sum] = "62d069d87f75e32d10eec0d4e37a1a2294d361d244e37e062dc212500bfb49a3"

SRC_URI += " \
	file://10_mainmenuentry.patch \
	file://11_volume-linearize.patch \
	file://12_step10s.patch \
	file://13_svdrp-help.patch \
	file://14_enable-ytdl.patch \
	file://15_stop-on-back.patch \
	file://frontend.sh \
	file://run.sh \
	file://vdr-plugin-mpv.menu \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += "mpv libdrm xinerama"
RDEPENDS:${PN} += "mpv libdrm"

CXXFLAGS += "-DUSE_LIBMPV -DUSE_DRM"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sharedir}/vdr/frontend.d
	install -m 0755 ${WORKDIR}/frontend.sh  ${D}${sharedir}/vdr/frontend.d/60_mpv.sh
	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/50_mpv.sh
	install -d ${D}${datadir}/menu
	install -m 0644 ${WORKDIR}/vdr-plugin-mpv.menu ${D}${datadir}/menu
}

FILES:${PN} += " \
	${sharedir}/vdr/frontend.d/* \
	${sharedir}/vdr/run.d/* \
	${datadir}/menu/* \
	"
