#!/bin/sh

case "$1" in
	start)
		mkdir -p /data/video

		setting call vdr.plugins.mpv.clear

		cat >> /etc/vdr/setup.conf.add <<- EOF
			mpv.SoftVol = 1
			mpv.UsePassthrough = 0
		EOF

		graphic=$(setting get display.driver)
		if [ "$graphic" = "nvidia" ]; then
			video="vdpau"
		elif [ "$graphic" = "intel" ]; then
			video="vaapi"
		else
			video="gpu"
		fi
		video=$(setting get "vdr.plugins.mpv.decoder||'$video'")
		args=$(setting get -d "-b /data/video -a auto -v $video -h $video" vdr.plugins.mpv.args)

		echo -e "[mpv]\n$args" > /etc/vdr/conf.d/50_mpv.conf
		;;
esac
