#!/bin/sh

case "$1" in
  start)
    ;;
  resume)
    if [ $(setting get 'vdr.plugins.mpv.get().length') != 0 ]; then
      svdrpsend.sh MESG "Starting video..."
      setting call vdr.plugins.mpv.play
    fi
    ;;
  suspend)
    setting call vdr.plugins.mpv.clear
    ;;
  stop)
    ;;
esac
