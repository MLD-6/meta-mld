SUMMARY = "VDR dvbapi plugin"
PV = "git"
PR = "r0"

PACKAGE_ARCH = "${TUNE_PKGARCH}_extra"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/manio/vdr-plugin-dvbapi.git;protocol=https;branch=master"
SRCREV = "28fc0c9670427d74010365772077b507bdd5e096"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += "libdvbcsa"
 
EXTRA_OEMAKE = " \
	LIBDVBCSA_NEW=1 \
	"

do_install() {
	oe_runmake DESTDIR=${D} install-lib install-i18n
}
