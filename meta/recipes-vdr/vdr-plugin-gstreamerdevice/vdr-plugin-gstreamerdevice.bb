SUMMARY = "VDR HD output device "
#COMPATIBLE_MACHINE = "(raspberrypi|raspberrypi2|raspberrypi3|raspberrypi4|raspberrypi4-64)"
PV = "git"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/rofehr/gstreamerdevice.git;protocol=https;branch=kms"
SRCREV = "f1ec39c6eaef4cd883a36813669d6372fd008d44"

#SRC_URI = "git://github.com/rofehr/gstreamerdevice.git;branch=kms"
#SRCREV = "ef26c76da155f0e0aa3e38b5d608fc52418d8a45"

SRC_URI += " \
	file://frontend.sh \
	file://run.sh \
	file://vdr-plugin-gstreamerdevice.menu \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

RDEPENDS:${PN} += "xserver-xorg"
#RDEPENDS:${PN} += "alsa"
RREPLACES:${PN} += "vdr-plugin-frontend"
RCONFLICTS:${PN} += "vdr-plugin-frontend"
RPROVIDES:${PN} += "vdr-plugin-frontend"

DEPENDS += " \
	gstreamer1.0 \
	gstreamer1.0-libav \
	cairo \
	libxcomposite \
	libxrender \
	libxpm \
	"

EXTRA_OEMAKE = " \
	STRIP=/bin/true \
	"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[${PLUGIN_NAME}]" > ${D}${sysconfdir}/vdr/conf.d/30_${PLUGIN_NAME}.conf
	install -d ${D}${sysconfdir}/vdr/remote.conf.d
	#install -m 0644 -D ${WORKDIR}/XKeySym.conf ${D}${sysconfdir}/vdr/remote.conf.d
	install -d ${D}${sharedir}/vdr/frontend.d
	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/70_gstreamerdevice.sh
	install -m 0755 ${WORKDIR}/frontend.sh  ${D}${sharedir}/vdr/frontend.d/50_gstreamerdevice.sh
	install -d ${D}${sharedir}/menu
	install -m 0644 ${WORKDIR}/vdr-plugin-gstreamerdevice.menu ${D}${sharedir}/menu
}

FILES:${PN} += " \
	${sysconfdir}/vdr/remote.conf.d/* \
	${sharedir}/vdr/run.d/* \
	${sharedir}/vdr/frontend.d/* \
	${sharedir}/menu/* \
	"
