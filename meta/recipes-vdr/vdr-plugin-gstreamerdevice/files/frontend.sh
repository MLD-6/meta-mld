#!/bin/sh

case "$1" in
  start)
    ${0%/*/*}/frontend.sh resume
    trap 'trap "" TERM; kill -s TERM -- -$$' TERM # kill childs on SIGTERM
    tail -f /dev/null & wait # infiny wait
    ${0%/*/*}/frontend.sh suspend
    ;;
  resume)
    #test "$VDR_REFRESH_RATE" && setmode -o "$XORG_MAIN_SCREEN" -r $VDR_REFRESH_RATE
    svdrpsend.sh "PLUG gstreamerdevice ATTA" >/dev/null
    ;;
  suspend)
    svdrpsend.sh "PLUG gstreamerdevice DETA" >/dev/null
    #test "$XORG_REFRESH_RATE" && setmode -o "$XORG_MAIN_SCREEN" -r $XORG_REFRESH_RATE
    ;;
  stop)
    ;;
esac
