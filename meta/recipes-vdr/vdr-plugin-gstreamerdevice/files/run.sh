#!/bin/sh

case "$1" in
	start)
		if [ "$(setting get apps.app.title)" != "VDR" ]; then
			/usr/share/vdr/frontend.sh suspend
		fi
		;;
esac
