SUMMARY = "VDR osd2web plugin"
DESCRIPTION = "A VDR plugin to provide a web OSD"
HOMEPAGE = "https://github.com/horchi/vdr-plugin-osd2web"
PR = "r3"

LICENSE = "GPL-1.0-only"
LIC_FILES_CHKSUM = "file://LICENSE;md5=84dcc94da3adb52b53ae4fa38fe49e5d"

SRC_URI = "git://github.com/horchi/vdr-plugin-osd2web.git;protocol=https;branch=master"
SRCREV = "b2d61d9ecad3a213b5b8c9425d3eb83a21e51bcc"

SRC_URI += " \
	file://10_osd2web_Makefile.patch \
	"
SRC_URI += " \
	file://startBrowser.sh \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	libwebsockets \
	libtinyxml2 \
	jansson \
	libexif \
	"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/plugins
	install -m 0755 -D ${WORKDIR}/startBrowser.sh ${D}${sysconfdir}/vdr/plugins/osd2web
}
