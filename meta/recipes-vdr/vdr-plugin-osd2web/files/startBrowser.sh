#!/bin/sh

URL="$1"
display="${2:-:1}"
skin=horchiTft
theme=graycd

eval $(setting get --sh vdr.plugins.osd2web)

DISPLAY="$display" /usr/bin/surf -f "$URL/$skin/index.html?theme=$theme&onlyView" > /dev/null 2>&1 &
pid=$!
trap "kill -TERM $pid" TERM
wait $pid
