#!/bin/sh

case "$1" in
	start)
		cat >> /etc/vdr/setup.conf.add <<- EOF
			live.ShowInfoBox = 0
			live.ShowLogo = 0
			live.StartPage = multischedule
			live.Theme = grey-black
			live.UseAuth = 0
			!live.UseStreamdev = $(test ! -f /usr/lib/vdr/libvdr-streamdev-client*; echo $?)
		EOF
		;;
esac
