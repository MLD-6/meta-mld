SUMMARY = " Live, the 'Live Interactive VDR Environment', \
            is a plugin providing the possibility to interactively \
            control the VDR and some of it's plugins by a web interface.."
AUTHOR = "  Thomas Keil <tkeil@datacrystal.de> \
            Sascha Volkenandt <sascha@akv-soft.de> \
            \
            Currently maintained by: \
            Markus Ehrnsperger ( MarkusE @ https://www.vdr-portal.de)"
PV = "3.4.2"
PR = "r2"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/MarkusEh/vdr-plugin-live.git;protocol=https;branch=master"
SRCREV = "28fe75b9f96ece2f01905cc24bc375e8c18cefd5"

SRC_URI += " \
	file://run.sh \
	file://grey-black_theme.css \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
    tntnet \
    tntnet-native \
    cxxtools-native \
    "

do_install() {
    oe_runmake DESTDIR=${D} PREFIX=/usr install

	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_live.sh
	install -d ${D}${sharedir}/vdr/plugins/live/themes/grey-black/css
	install -m 0755 ${WORKDIR}/grey-black_theme.css ${D}${sharedir}/vdr/plugins/live/themes/grey-black/css/theme.css
}

FILES:${PN} += " \
	${sharedir}/vdr/* \
	"
