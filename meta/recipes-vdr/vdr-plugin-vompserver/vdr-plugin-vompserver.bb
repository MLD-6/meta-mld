SUMMARY = "vompserver vdr plugin"
AUTHOR = " Chris Tallon <chris@loggytronic.com>"
PV = "0.5.1"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://git.vomp.tv/vompserver.git;protocol=https;branch=master"
SRCREV = "6544d026851dc6d6df7927a8bbb8529410b7896c"

SRC_URI += " \
	file://00_vompserver_VDR271.patch \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
    "

do_install() {
	oe_runmake DESTDIR=${D} install
}
