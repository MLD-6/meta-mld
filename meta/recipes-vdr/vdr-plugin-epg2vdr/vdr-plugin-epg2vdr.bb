SUMMARY = "Imports the EPG of an other VDR."
AUTHOR = "Frank Schmirler"
PV = "1.2.17"
PR = "r7"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://github.com/horchi/vdr-plugin-epg2vdr.git;protocol=https;branch=master"
SRCREV = "071afd7526278eea0c4439fd48618d548cd36614"

SRC_URI += " \
	file://run.sh \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

RSUGGESTS:${PN} += "vdr-epg-daemon"

DEPENDS += "python3 python3-native mariadb mariadb-native imlib2 libtinyxml2 openssl libarchive jansson"

EXTRA_OEMAKE += "INCLUDES='-I${STAGING_INCDIR} -I${STAGING_INCDIR}/mysql -I${STAGING_INCDIR}/python3.12'"
EXTRA_OEMAKE += "LDFLAGS='${LDFLAGS} --sysroot=${STAGING_DIR_TARGET}'"

DEBUG_PREFIX_MAP:remove = "-fcanon-prefix-map"


do_compile:prepend() {
	export PATH="${WORKDIR}/recipe-sysroot-native/usr/bin/python3-native:$PATH"
}

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_epg2vdr.sh
	install -d ${D}${sysconfdir}/vdr/plugins/epg2vdr/epgsearch
	install -m 0644 -D ${S}/configs/epgsearch/* ${D}${sysconfdir}/vdr/plugins/epg2vdr/epgsearch
}

FILES:${PN} += " \
	${sharedir}/vdr/run.d \
	"
