SUMMARY = "MarkAd marks advertisements in VDR recordings"
AUTHOR = "Jochen Dolze <vdr@dolze.de>"
PV = "4.2.10"
PR = "r5"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/kfb77/vdr-plugin-markad.git;protocol=https;branch=V04"
SRCREV = "80e0dbc36e0634eaadc05bafd3ca2aa971bd6c6c"

FILESEXTRAPATHS:prepend := "${THISDIR}/../../files:"

SRC_URI += " \
	file://markad.conf \
	file://removeindex.sh \
	file://removemarks.sh \
	file://removeresume.sh \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	ffmpeg \
	vdr-plugin-svdrpservice \
	"

RDEPENDS:${PN} += " \
	vdr-plugin-svdrpservice \
	"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/reccmds.conf.d
	install -m 0644 ${WORKDIR}/markad.conf ${D}${sysconfdir}/vdr/reccmds.conf.d/
	install -m 0755 ${WORKDIR}/removeindex.sh ${D}${bindir}/
	install -m 0755 ${WORKDIR}/removemarks.sh ${D}${bindir}/
	install -m 0755 ${WORKDIR}/removeresume.sh ${D}${bindir}/
}
