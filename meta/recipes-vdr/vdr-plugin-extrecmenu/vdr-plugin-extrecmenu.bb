SUMMARY = "VDR extrecmenu plugin"
PV = "git"
PR = "r5"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRCREV = "77d10faec3c7b0abe25ba3b161dc3b4e2cad042b"
SRC_URI = "git://vdr-projects.e-tobi.net/git/vdr-plugin-extrecmenu;protocol=https;branch=master"

SRC_URI += " \
	file://00_extrecmenu_VDR271.patch \
	file://extrecmenu-vdr-2.3.x-v2.patch \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += "vdr-font-symbols"

EXTRA_OEMAKE = ' \
	SDKSTAGE="${STAGING_DIR_HOST}" \
	'

do_install() {
	oe_runmake DESTDIR=${D} install
}
