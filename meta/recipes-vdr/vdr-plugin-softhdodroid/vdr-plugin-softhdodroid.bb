SUMMARY = "VDR HD output device "
PV = "git"
PR = "r5"
COMPATIBLE_MACHINE = "(odroid-n2|hardkernel-odroidn2plus)"

LICENSE = "AGPL-3.0-only"


SRC_URI = "git://github.com/jojo61/vdr-plugin-softhdodroid.git;protocol=https;branch=master"
SRCREV = "d37cfc5867a7110474b3f3a7dfdf5e48f72bc422"

SRC_URI += " \
	file://makefile.patch \
	file://frontend.sh \
	file://run.sh \
	file://vdr-plugin-softhdodroid.menu \	
	"

S = "${WORKDIR}/git"

inherit vdr-plugin



RREPLACES:${PN} += "vdr-plugin-frontend"
RCONFLICTS:${PN} += "vdr-plugin-frontend"
RPROVIDES:${PN} += "vdr-plugin-frontend"

DEPENDS += " \
	freetype \
	ffmpeg \
	glm \
	libglu \
	virtual/libgl \
	virtual/libgles2 \
	virtual/egl \
	libmms \
	"

#EXTRA_OEMAKE = " \
#    KODIBUILD=1 \
#	"

INSANE_SKIP:${PN}:append = "already-stripped dev-deps ldflags"

#RDEPENDS:${PN}+="libMali"
#DEPENDS_${PN} += "kernel-module-mali-kbase"
#RDEPENDS_${PN} += "kernel-module-mali-kbase"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[${PLUGIN_NAME}]" > ${D}${sysconfdir}/vdr/conf.d/30_${PLUGIN_NAME}.conf

	install -d ${D}${sharedir}/vdr/frontend.d
	install -m 0755 ${WORKDIR}/frontend.sh  ${D}${sharedir}/vdr/frontend.d/50_softhdodroid.sh
	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_softhdodroid.sh
	install -d ${D}${sharedir}/menu
	install -m 0644 ${WORKDIR}/vdr-plugin-softhdodroid.menu ${D}${sharedir}/menu

}

FILES:${PN} += " \
	${sharedir}/vdr/frontend.d/* \
	${sharedir}/vdr/run.d/* \
	${sharedir}/menu/* \
	"
