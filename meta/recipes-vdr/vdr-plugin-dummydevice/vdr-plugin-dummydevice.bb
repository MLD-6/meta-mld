SUMMARY = "VDR dummydevice plugin"
DESCRIPTION = "Output device that does nothing. All data is silently discarded."
HOMEPAGE = "http://phivdr.dyndns.org/vdr/"
AUTHOR = "Petri Hintukainen <phintuka@users.sourceforge.net>"
PV = "2.0.0"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "http://phivdr.dyndns.org/vdr/vdr-dummydevice/vdr-dummydevice-${PV}.tgz"
SRC_URI[sha256sum] = "5c0049824415bd463d3abc728a3136ee064b60a37b5d3a1986cf282b0d757085"

S = "${WORKDIR}/dummydevice-${PV}"

inherit vdr-plugin

RREPLACES:${PN} += "vdr-plugin-frontend"
RCONFLICTS:${PN} += "vdr-plugin-frontend"
RPROVIDES:${PN} += "vdr-plugin-frontend"

do_install() {
	oe_runmake DESTDIR=${D} install-lib install-i18n
}
