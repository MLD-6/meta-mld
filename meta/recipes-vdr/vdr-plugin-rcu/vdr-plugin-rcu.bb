SUMMARY = "VDR remote control unit plugin"
DESCRIPTION = "Fernbedienungs Empfänger und Ansteuerung eines 7-Segment Displays"
PV = "git"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/vdr-projects/vdr-plugin-rcu.git;protocol=https;branch=master"
SRCREV = "995bda921526d8aa4881ac702e2e7cb7dc8bde9a"

S = "${WORKDIR}/git"

inherit vdr-plugin

do_install() {
	oe_runmake DESTDIR=${D} install
}
