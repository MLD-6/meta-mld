SUMMARY = "VDR HD output device with intel vaapi support"
COMPATIBLE_MACHINE = "(genericx86-64)"
PV = "git"
PR = "r13"

LICENSE = "AGPL-3.0-only"
LIC_FILES_CHKSUM = "file://AGPL-3.0.txt;md5=c959e6f173ca1387f63fbaec5dc12f99"

SRC_URI = "git://github.com/ua0lnj/vdr-plugin-softhddevice.git;protocol=https;branch=latest"
SRCREV = "b57dc9a8840b124654c5451f1be3a13b69a99424"

SRC_URI += " \
	file://frontend.sh \
	file://run.sh \
	file://vdr-plugin-softhddevice.menu \
	file://vdr-plugin-softhddevice.service \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin
inherit systemd

SYSTEMD_SERVICE:${PN} = "vdr-plugin-softhddevice.service"

RDEPENDS:${PN} += "xserver-xorg"
RRECOMMENDS:${PN} += "intel-vaapi-driver intel-media-driver" 
RREPLACES:${PN} += "vdr-plugin-frontend"
RCONFLICTS:${PN} += "vdr-plugin-frontend"
RPROVIDES:${PN} += "vdr-plugin-frontend"

DEPENDS += " \
	ffmpeg \
	xcb-util-wm \
	libva \
	libglu \
	glew \
	freeglut \
	glm \
	freetype \
	intel-vaapi-driver \
	intel-media-driver \
	"

EXTRA_OEMAKE = " \
	CUVID=0 \
	OPENGLOSD=1 \
	VDPAU=0 \
	VAAPI=1 \  
	OPENGL=1 \
	AVRESAMPLE=0 \
	SWSCALE=1 \
	EGL=1 \
	"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[softhddevice]" > ${D}${sysconfdir}/vdr/conf.d/30_softhddevice.conf
	install -d ${D}${sharedir}/vdr/frontend.d
	install -m 0755 ${WORKDIR}/frontend.sh  ${D}${sharedir}/vdr/frontend.d/60_softhddevice.sh
	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_softhddevice.sh
	install -d ${D}${sharedir}/menu
	install -m 0644 ${WORKDIR}/vdr-plugin-softhddevice.menu ${D}${sharedir}/menu
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/vdr-plugin-softhddevice.service ${D}${systemd_unitdir}/system
}

pkg_postinst:${PN}() {
#!/bin/sh
cat >> /etc/vdr/setup.conf.add <<- EOF
	softhddevice.1080i.Deinterlace = 3
	softhddevice.1080i.SkipChromaDeinterlace = 0
	softhddevice.1080i_fake.Deinterlace = 3
	softhddevice.1080i_fake.SkipChromaDeinterlace = 0
	softhddevice.576i.Deinterlace = 3
	softhddevice.576i.SkipChromaDeinterlace = 0
	softhddevice.720p.Deinterlace = 1
	softhddevice.720p.SkipChromaDeinterlace = 0
	softhddevice.UHD.Deinterlace = 2
	softhddevice.UHD.SkipChromaDeinterlace = 0
	softhddevice.AudioSoftvol = 1
	softhddevice.BlackPicture = 1
	softhddevice.Contrast = 10
	softhddevice.MakePrimary = 1
	softhddevice.Saturation = 10
	softhddevice.Suspend.Close = 1
EOF
}

FILES:${PN} += " \
	${sharedir}/vdr/frontend.d/* \
	${sharedir}/vdr/run.d/* \
	${sharedir}/menu/* \
	${systemd_unitdir}/system/* \
	"
