#!/bin/sh

case "$1" in
	start)
		if setting get display.driver | grep -q iHD; then
			video='va-api-egl'
		else
			#video='va-api'
			video='va-api-egl'
		fi
		args=$(setting get -d "-f -w alsa-driver-broken -v $video" vdr.plugins.softhddevice.args)

		# disable VDR frontend if another app is active
		if [ "$(setting get apps.app.title)" != "VDR" ] || ! xset -d :0 q &>/dev/null; then
			args="$args -D"
		fi

		echo -e "[softhddevice]\n$args" > /etc/vdr/conf.d/30_softhddevice.conf
		;;
esac
