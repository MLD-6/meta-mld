#!/bin/sh

case "$1" in
  resume)
    svdrpsend.sh "PLUG suspendoutput RESUME" >/dev/null
    ;;
  suspend)
    svdrpsend.sh "PLUG suspendoutput SUSPEND" >/dev/null
    ;;
  stop)
    ;;
esac
