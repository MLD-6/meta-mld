#!/bin/sh

case "$1" in
	start)
		suspend=""
		if [ "$(setting get apps.app.title)" != "VDR" ]; then
			suspend="--suspend"
		fi
		args=$(setting get -d "-M -T -b -n" vdr.plugins.suspendoutput.args)
		echo -e "[suspendoutput]\n$args\n$suspend" > /etc/vdr/conf.d/50_suspendoutput.conf
		;;
esac
