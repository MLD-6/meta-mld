SUMMARY = "Suspend vdr output."
AUTHOR = "Petri Hintukainen"
PV = "2.1.0"
PR = "r2"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://github.com/CodeKing/vdr-plugin-suspendoutput.git;protocol=https;branch=master"
SRCREV = "e720326b01710cd577de6d65d1d65ccdd8b0cfba"

SRC_URI:append = " \
	file://00_suspendoutput_VDR271.patch \
	file://00_nooutput-on-blank.patch \
	file://01_mpg2c-build.patch \
	file://02_suspend-on-startup.patch \
	"

#SRC_URI:append = " \
#	file://frontend.sh \
#	file://run.sh \
#	"

S = "${WORKDIR}/git"

inherit vdr-plugin

do_install() {
	oe_runmake DESTDIR=${D} install

#	install -d ${D}${sharedir}/vdr/frontend.d
#	install -m 0755 ${WORKDIR}/frontend.sh  ${D}${sharedir}/vdr/frontend.d/50_suspendoutput.sh
#	install -d ${D}${sharedir}/vdr/run.d
#	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_suspendoutput.sh
}

#FILES:${PN} += " \
#	${sharedir}/vdr/frontend.d/* \
#	${sharedir}/vdr/run.d/* \
#	"
