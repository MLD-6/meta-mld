#!/bin/sh

if [ $# -ne 1 ]; then
	echo "Usage: $0 {start | stop}" >&2
	exit 1
fi

# wait in case VDR packages get installed
timeout 30 sh -c 'while ps | grep -q "[a]pt-get install .* vdr"; do sleep 1; done'

for script in $(ls ${0%.*}.d/*.sh 2>/dev/null | sort); do
	. $script $@
done
