#!/bin/sh

if [ $# -ne 1 ]; then
    echo "Usage: $0 {start | resume | suspend | stop}" >&2
    exit 1
fi

for script in $(ls ${0%.*}.d/*.sh 2>/dev/null | sort); do
    $script $@
done
