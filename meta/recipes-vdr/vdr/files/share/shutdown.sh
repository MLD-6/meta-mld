#!/bin/sh

exec 2>&1

if [ $# -lt 5 ]; then
	echo "Usage: $0 utctime difftime channel filename reason"
	exit 1
fi

cat > /etc/vdr/nexttimer <<- EOF
	utctime="$1"
	difftime="$2"
	channel="$3"
	filename="$(echo $4 | sed 's/\\/\\\\/g;s/\"/\\\"/g')"
	reason="$5"
EOF

echo "Shutdown..."
for script in $(ls ${0%.*}.d/*.sh 2>/dev/null | sort); do
	echo "${script##*/} says:"
	$script $@
	if [ $? = 0 ]; then
		echo " do shutdown"
	else
		echo " do not shutdown"
		exit 1
	fi
done

shutdown -h now
