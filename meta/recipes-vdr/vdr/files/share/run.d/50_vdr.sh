#!/bin/sh

case "$1" in
	start)
		eval $(setting get --sh vdr)

		# create data dirs
		mkdir -p /data/tv /data/photo/vdr /var/cache/vdr

		# update remote.conf
		echo "# Automatic generated file. Do not change!" > /etc/vdr/remote.conf
		cat /etc/vdr/remote.conf.d/*.conf 2>/dev/null >> /etc/vdr/remote.conf
		if [ "$showRemoteInfo" != "false" ]; then
			setting set vdr.showRemoteInfo false
			cat /etc/vdr/remote.conf.d/info.txt >>/var/spool/vdr.info
		fi

		# update reccmds.conf
		echo "# Automatic generated file. Do not change!" > /etc/vdr/reccmds.conf
		cat /etc/vdr/reccmds.conf.d/*.conf 2>/dev/null >> /etc/vdr/reccmds.conf

		# update commands.conf
		echo "# Automatic generated file. Do not change!" > /etc/vdr/commands.conf
		cat /etc/vdr/commands.conf.d/*.conf 2>/dev/null >> /etc/vdr/commands.conf

		# create channels.conf link
		channellist=$(setting get vdr.channels.active)
		mkdir -p /etc/vdr/channels
		touch "/etc/vdr/channels/${channellist}.conf"
		ln -fs channels/"$channellist".conf /etc/vdr/channels.conf

		# update setup.conf
		while read line; do
			if [ -z "${line##!*}" ]; then
				line="${line#!}"
				sed "/^${line%% *}/d" -i /etc/vdr/setup.conf
			fi
			if ! grep -q "^${line%% *}" /etc/vdr/setup.conf; then
				echo "$line" >> /etc/vdr/setup.conf
			fi
		done 2>/dev/null < /etc/vdr/setup.conf.add
		rm -f /etc/vdr/setup.conf.add

		# wait for dvb adapters
		setting call dvb.waitForDevices

		# start vdr in background
		vdr >/dev/tty5 </dev/tty5 &

		# wait until vdr is startet
		timeout 30 sh -c 'while [ -z "$(svdrpsend.sh QUIT)" ]; do sleep 1; done' || true
		;;
	stop)
		;;
esac
