#!/bin/sh

case "$1" in
  resume)
    if [ -d /etc/keyd -a "$(fgconsole)" = "5" ]; then
      cp /etc/vdr/keyd.conf /etc/keyd/vdr.conf
      keyd reload
    fi
   ;;
  suspend)
    if [ -f /etc/keyd/vdr.conf ]; then
      rm /etc/keyd/vdr.conf
      keyd reload
    fi
    ;;
esac
