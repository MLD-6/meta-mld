#!/bin/sh

case "$1" in
  resume)
    svdrpsend.sh "REMO on" >/dev/null
    svdrpsend.sh "HITK menu menu back" >/dev/null
    ;;
  suspend)
    svdrpsend.sh "REMO off" >/dev/null
    ;;
esac
