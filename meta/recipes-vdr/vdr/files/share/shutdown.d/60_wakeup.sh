#!/bin/sh
# set wakeup time for the next recording

test -e /etc/vdr/nexttimer && . /etc/vdr/nexttimer

# current time
curentTime=`date +%s`

# how many seconds before the next event the system should be booted
eval $(setting get --sh vdr.wakeup)
bootTime=${bootTime:-120}
downTime=${downTime:-60}
setting set vdr.wakeup "{bootTime: $bootTime, downTime: $downTime}"

if [ "$utctime" ] ; then
	if [ $utctime -eq 0 ] ; then 
		# no timer set
		wakeTime=$(( $curentTime - $bootTime ))
	else
		if [ $difftime -lt $(( $bootTime + $downTime )) ] ; then
			echo " Time to next wakeup time is to short"
			exit 1
		fi
		wakeTime=$(( $utctime - $bootTime ))
	fi

	if [ -e /sys/class/rtc/rtc0/wakealarm ]; then
		echo 0 > /sys/class/rtc/rtc0/wakealarm || exit 2
		echo $wakeTime > /sys/class/rtc/rtc0/wakealarm || exit 2
		echo $wakeTime > /sys/class/rtc/rtc0/wakealarm
	elif [ -e /proc/acpi/alarm ]; then
		date -d @$wakeTime "+%Y-%m-%d %H:%M:%S" >/proc/acpi/alarm || exit 2
		date -d @$wakeTime "+%Y-%m-%d %H:%M:%S" >/proc/acpi/alarm
	else
		echo " No timer device found"
		if [ $utctime -ne 0 ] ; then 
			exit 2
		fi
	fi
fi
exit 0
