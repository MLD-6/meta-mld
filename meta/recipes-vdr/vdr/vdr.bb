DESCRIPTION = "Video Disk Recorder (VDR) is a digital sat-receiver program using Linux and DVB technologies. It allows to record broadcasts, as well as output the stream to TV."
SUMMARY = "Video Disk Recorder"
HOMEPAGE = "http://www.tvdr.de"
AUTHOR = "Klaus Schmidinger"
SECTION = "console/multimedia"
PV = "${VDR_VERSION}"
PR = "${VDR_REVISION}"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

#SRC_URI = "git://gitlab.com/vdr-projects/vdr.git;protocol=https;branch=stable/2.6"
SRC_URI = "git://git.tvdr.de/vdr.git;branch=master"
SRCREV = "285574eeaaf68938feba49ba6ec66f4fd1f52e1f"

SRC_URI += " \
	file://02_vdr_zapcockpit.patch \
	file://15_config.h.diff \
	file://15_menu.c.diff \
	file://17_vdr_skinlcarsng_numrecordings.patch \
	file://21_vdr_menuselection.patch \
	file://26_vdr_menuorg.patch \
 	file://29_vdr_ttxtsub.patch \
	file://30_vdr_rec-order.patch \
 	file://32_vdr_naludump.patch \
	file://33_vdr_allow-remote-comments.patch \
	file://34_vdr_disable-swith-primary-device-message.patch \
	file://35_vdr_svdrphosts-config.patch \
	file://36_vdr_ignore-pluginmissing.patch \
	"
# SRC_URI += " \
# 	file://02_vdr_zapcockpit.patch \
# 	file://05_vdr_editrecording.patch \
# 	file://06_vdr_avcdescriptor.patch \
# 	file://12_vdr_pin.patch \
# 	file://15_vdr_mainmenuhook.patch \
# 	file://16_vdr_wareagleicon.patch \
# 	file://21_vdr_menuselection.patch \
# 	file://22_vdr_svdrphosts.patch \
# 	file://23_vdr_maxdevice.patch \
# 	file://24_vdr_dvb_power_saving_feature.patch \
# 	file://27_vdr_permashift.patch \
# 	file://28_vdr_hide_first_recording_level.patch \
# 	file://31_vdr_HelmutB_ClearObsoleteChannels.patch \
# 	file://99_vdr_HelmutB_CamTweak_v245.patch \
#	"

S = "${WORKDIR}/git"

FILESEXTRAPATHS:prepend := "${THISDIR}/../../files:"

SRC_URI += " \
	file://about.txt \
	file://vdr.service \
	file://etc/camtweaks.conf \
	file://etc/keyd.conf \
	file://etc/setup.conf \
	file://etc/remote.conf.d/info.txt \
	file://etc/remote.conf.d/KBD.conf \
	file://etc/remote.conf.d/LIRC.conf \
	file://etc/remote.conf.d/XKeySym.conf \
	file://etc/commands.conf.d/about.conf \
	file://etc/commands.conf.d/system.conf \
	file://etc/commands.conf.d/vdr.conf \
	file://etc/channels/DVB-S19.2E_Astra-FTA-DE.conf \
	file://etc/channels/DVB-S19.2E_Astra-FTA-DE-HD.conf \
	file://etc/channels/DVB-S19.2E_Astra-FTA-DE-HD+-SKY.conf \
	file://etc/conf.d/00_vdr.conf \
	file://etc/themes/lcars-BlueGlass.theme \
	file://etc/themes/lcars-LBSense.theme \
	file://etc/themes/sttng-blue.theme \
	file://etc/themes/sttng-cool.theme \
	file://share/frontend.sh \
	file://share/frontend.d/40_vdr.sh \
	file://share/frontend.d/60_keyd.sh \
	file://share/frontend.d/70_vdr-info.sh \
	file://share/recording.sh \
	file://share/run.sh \
	file://share/run.d/50_vdr.sh \
	file://share/shutdown.sh \
	file://share/shutdown.d/50_vdr.sh \
	file://share/shutdown.d/60_wakeup.sh \
	"

S = "${WORKDIR}/git"

inherit vdr systemd

DEPENDS = " \
	fontconfig \
	freetype \
	ttf-bitstream-vera \
	gettext \
	jpeg \
	libcap \
	virtual/libintl \
	ncurses \
	"

RDEPENDS:${PN} += " \
	svdrpsend \
	coreutils \
	binutils \
	vdr-font-symbols \
	glibc-gconv-iso8859-9 \
	"
RRECOMMENDS:${PN} +="vdr-plugin-menuorg vdr-plugin-restfulapi"

CXXFLAGS = "-fPIC"

patch() {
	ls ${S}/po/*.po | while read po; do
		iconv -f $(sed -n 's/"Content\-Type\: text\/plain; charset\=\(.*\)\\n"/\1/p' $po) -t utf8 $po | sed 's/\("Content\-Type\: text\/plain\; charset\=\).*/\1utf-8\\n"/' > $po.tmp
		mv -f $po.tmp $po
	done
}

do_patch:append() {
    bb.build.exec_func('patch', d)
}

do_configure() {
	cat > Make.config <<-EOF
		## The C compiler options:
		CFLAGS   = -Wall
		CXXFLAGS = -Wall
		### The directory environment:
		ARGSDIR  = ${sysconfdir}/vdr/conf.d
		CONFDIR  = ${sysconfdir}/vdr
		PREFIX   = ${prefix}
		VIDEODIR = /data/tv
	EOF
}

do_compile () {
	oe_runmake 'DESTDIR=${D}' vdr
}

do_install () {
	oe_runmake 'DESTDIR=${D}' install-i18n install-includes install-pc

	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/vdr.service                 ${D}${systemd_unitdir}/system

	install -d ${D}${libdir}/vdr

	install -d ${D}${bindir}
	install -m 0755 ${S}/vdr                               ${D}${bindir}/vdr

	install -d ${D}${sysconfdir}/vdr
	touch ${D}${sysconfdir}/vdr/environment
	install -m 0644 ${S}/camresponses.conf                 ${D}${sysconfdir}/vdr
	install -m 0644 ${S}/channels.conf                     ${D}${sysconfdir}/vdr
	install -m 0644 ${S}/diseqc.conf                       ${D}${sysconfdir}/vdr
	install -m 0644 ${S}/keymacros.conf                    ${D}${sysconfdir}/vdr
	install -m 0644 ${S}/scr.conf                          ${D}${sysconfdir}/vdr
	install -m 0644 ${S}/sources.conf                      ${D}${sysconfdir}/vdr
	install -m 0644 ${S}/svdrphosts.conf                   ${D}${sysconfdir}/vdr
	install -m 0644 ${WORKDIR}/etc/camtweaks.conf          ${D}${sysconfdir}/vdr
	install -m 0644 ${WORKDIR}/etc/setup.conf              ${D}${sysconfdir}/vdr
	install -m 0644 ${WORKDIR}/etc/keyd.conf               ${D}${sysconfdir}/vdr

	install -d ${D}${sysconfdir}/vdr/remote.conf.d
	install -m 0644 -D ${WORKDIR}/etc/remote.conf.d/*      ${D}${sysconfdir}/vdr/remote.conf.d

	install -d ${D}${sysconfdir}/vdr/commands.conf.d
	install -m 0644 -D ${WORKDIR}/etc/commands.conf.d/*    ${D}${sysconfdir}/vdr/commands.conf.d

	install -d ${D}${sysconfdir}/vdr/channels
	install -m 0644 -D ${WORKDIR}/etc/channels/*           ${D}${sysconfdir}/vdr/channels

	install -d ${D}${sysconfdir}/vdr/conf.d
	install -m 0644 ${WORKDIR}/etc/conf.d/00_vdr.conf      ${D}${sysconfdir}/vdr/conf.d
	
	install -d ${D}${sysconfdir}/vdr/themes
	install -m 0644 -D ${WORKDIR}/etc/themes/*             ${D}${sysconfdir}/vdr/themes

	install -d ${D}${sharedir}/vdr
	install -m 0755 ${WORKDIR}/about.txt                   ${D}${sharedir}/vdr/aboutmld.txt
	install -d ${D}${sharedir}/vdr/frontend.d
	install -m 0755 ${WORKDIR}/share/frontend.sh           ${D}${sharedir}/vdr
	install -m 0755 -D ${WORKDIR}/share/frontend.d/*       ${D}${sharedir}/vdr/frontend.d
	install -d ${D}${sharedir}/vdr/recording.d
	install -m 0755 ${WORKDIR}/share/recording.sh          ${D}${sharedir}/vdr
	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/share/run.sh                ${D}${sharedir}/vdr
	install -m 0755 -D ${WORKDIR}/share/run.d/*            ${D}${sharedir}/vdr/run.d
	install -d ${D}${sharedir}/vdr/shutdown.d
	install -m 0755 ${WORKDIR}/share/shutdown.sh           ${D}${sharedir}/vdr
	install -m 0755 -D ${WORKDIR}/share/shutdown.d/*       ${D}${sharedir}/vdr/shutdown.d

	install -d ${D}${mandir}/man1 ${D}${mandir}/man5
	install -m 0644 ${S}/vdr.1                             ${D}${mandir}/man1/
	install -m 0644 ${S}/vdr.5                             ${D}${mandir}/man5/
}

SYSTEMD_SERVICE:${PN} = "vdr.service" 

CONFFILES:${PN} += " \
  ${sysconfdir}/vdr/* \
  "
FILES:${PN} = "  \
	${systemd_unitdir}/system/vdr.service  \ 
	${sharedir}/vdr/* \
	${libdir}/vdr \
	${bindir}/vdr \
	${sysconfdir}/vdr/* \
	"

FILES:${PN}-dbg += "${PLUGINDIR}/.debug/*"
FILES:${PN}-locale = "${sharedir}/locale"
