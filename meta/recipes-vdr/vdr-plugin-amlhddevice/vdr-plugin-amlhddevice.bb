SUMMARY = "amlhddevice - A VDR Plugin to manage your Channellists"
PV = "git"
PR = "r2"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/vdr-projects/vdr-plugin-amlhddevice.git;protocol=https;branch=master"
SRCREV = "b0fe61c70c862afcb5defb2cede3dac32587f475"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	libamcodec\
	"

#EXTRA_OEMAKE = ' \
#	SDKSTAGE="${STAGING_DIR_HOST}" \
#	'

do_install() {
	oe_runmake DESTDIR=${D} install
}
