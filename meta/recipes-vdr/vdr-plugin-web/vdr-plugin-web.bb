SUMMARY = "VDR web plugin"
PV = "git"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/Zabrimus/vdr-plugin-web.git;protocol=https;branch=master"
SRCREV = "9c97d3deb27af96a3f0afa6e553bfb436d51b146"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	graphicsmagick \
	"
RDEPENDS:${PN} += "cefbrowser remotetranscode"

do_install() {
	oe_runmake DESTDIR=${D} install-lib install-i18n

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[${PLUGIN_NAME}]\n-c /etc/cefbrowser/sockets.ini" > ${D}${sysconfdir}/vdr/conf.d/50_${PLUGIN_NAME}.conf
}
