SUMMARY = "VDR skinenigmang plugin"
PV = "git"
PR="r6"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

SRCREV = "dbcd3e6bb9ec0087787d2bedc4528d5925c90d71"
SRC_URI = "git://github.com/vdr-projects/vdr-plugin-skinenigmang.git;protocol=https;branch=master"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	freetype \
	imagemagick \
	vdr-font-symbols \
	"

EXTRA_OEMAKE = "DESTDIR=\"${D}\" 'INCLUDES=-I${STAGING_INCDIR}/freetype2 -I${STAGING_INCDIR}/ImageMagick-7 -I${S}' LIBS=-L${STAGING_LIBDIR} "

do_install() {
	oe_runmake DESTDIR=${D} install
}

FILES:${PN} += "${sysconfdir}/vdr/themes/*"
