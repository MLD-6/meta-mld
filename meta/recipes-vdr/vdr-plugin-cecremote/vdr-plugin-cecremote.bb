SUMMARY = "VDR cecremote plugin"
AUTHOR = "Uli Eckhart"
PV = "git"
PR = "r13"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://git.uli-eckhardt.de/vdr-plugin-cecremote.git;protocol=https;branch=master"
SRCREV = "0a2aaf741b05aefbf36335579e6bd83b428b8fef"

SRC_URI += " \
	file://config.patch \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
	pugixml \
	libcec \
	"

EXTRA_OEMAKE = "INCLUDES=-I${STAGING_INCDIR}/libcec"

do_compile:prepend() {
	mkdir -p ${S}/po
}

do_install() {
	oe_runmake DESTDIR=${D} install-lib install-i18n

	install -d ${D}${sysconfdir}/vdr/plugins/cecremote
	install -m 0644 -D ${S}/contrib/* ${D}${sysconfdir}/vdr/plugins/cecremote
}

CONFFILES:${PN} += " \
  ${sysconfdir}/vdr/plugins/cecremote/cecremote.xml \
  "
FILES:${PN} += "  \
  ${sysconfdir}/vdr/plugins/cecremote/* \
	"
