SUMMARY = "VDR HD output device for Raspberry Pi"
COMPATIBLE_MACHINE = "(raspberrypi|raspberrypi2|raspberrypi3)"
PV = "git"
PR="r30"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://github.com/reufer/rpihddevice.git;protocol=https;branch=master"
SRCREV = "d806a57c14ab5b44a2b072ecf871161bd7106e31"


SRC_URI += " \
	file://egl.patch \
	"

SRC_URI += " \
	file://frontend.sh \
	file://run.sh \
	file://vdr-plugin-rpihddevice.menu \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

#RDEPENDS:${PN} += "alsa"
RREPLACES:${PN} += "vdr-plugin-frontend"
RCONFLICTS:${PN} += "vdr-plugin-frontend"
RPROVIDES:${PN} += "vdr-plugin-frontend"

DEPENDS += " \
	ffmpeg \
	freetype \
	userland \
	virtual/libgl \
	virtual/libgles2 \
	virtual/egl \
	libtheora \
	libva-utils \
	xz \
	"

EXTRA_OEMAKE = ' \
	SDKSTAGE="${STAGING_DIR_HOST}" \
	'

export LDFLAGS = " \
	-L${S}/ilclient \
	-L${STAGING_DIR_HOST}/lib \
	-L${STAGING_DIR_HOST}/usr/lib \
	"

export INCLUDES = " \
	${@bb.utils.contains('MACHINE_FEATURES', 'vc4graphics', ' -D__GBM__', '', d)} \
	-isystem${STAGING_DIR_HOST}/usr/include/interface/vcos/pthreads \
	-isystem${STAGING_DIR_HOST}/usr/include/interface/vmcs_host/linux \
	"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[${PLUGIN_NAME}]" > ${D}${sysconfdir}/vdr/conf.d/30_${PLUGIN_NAME}.conf
	install -d ${D}${sharedir}/vdr/frontend.d
	install -m 0755 ${WORKDIR}/frontend.sh  ${D}${sharedir}/vdr/frontend.d/50_rpihddevice.sh
	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_rpihddevice.sh
	install -d ${D}${sharedir}/menu
	install -m 0644 ${WORKDIR}/vdr-plugin-rpihddevice.menu ${D}${sharedir}/menu
}

pkg_postinst:${PN}() {
#!/bin/sh
cat >> /etc/vdr/setup.conf.add <<- EOF
	rpihddevice.FrameRate = 1
	streamdev-client.Timeout = 15
EOF
}

FILES:${PN} += " \
	${sharedir}/vdr/frontend.d/* \
	${sharedir}/vdr/run.d/* \
	${sharedir}/menu/* \
	"
