#!/bin/sh

case "$1" in
	start)
		device=$(setting get audio.active[0].name | cut -d" " -f 4)
		if [ ! "$device" ]; then
			setting call audio.devices | grep -q "HDMI" && device="HDMI" || device="Headphones"
		fi
		if [ "$device" = "Headphones" ]; then
			cat >> /etc/vdr/setup.conf.add <<- EOF
				!rpihddevice.AudioPort = 0
			EOF
		fi
		if [ "$device" = "HDMI" ]; then
			cat >> /etc/vdr/setup.conf.add <<- EOF
				!rpihddevice.AudioPort = 1
			EOF

			arrangement=$(setting get audio.active[0].arrangement)
			if [ 0$arrangement -eq 2 ]; then
				cat >> /etc/vdr/setup.conf.add <<- EOF
					!rpihddevice.AudioFormat = 2
				EOF
			fi
			if [ 0$arrangement -gt 2 ]; then
				cat >> /etc/vdr/setup.conf.add <<- EOF
					!rpihddevice.AudioFormat = 1
				EOF
			fi
		fi

		# disable VDR frontend if another app is active
		cat >> /etc/vdr/setup.conf.add <<- EOF
			!PrimaryDVB = $(test "$(setting get apps.app.title)" = "VDR" && echo 1 || echo 2)
		EOF
		;;
esac
