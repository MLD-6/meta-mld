SUMMARY = "VDR skinnopacity plugin"
PV = "git"
PR="r3"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRCREV = "45651524a8cbc5e24807967605adf6619afb1773"
SRC_URI = "git://gitlab.com/kamel5/skinsimple.git;protocol=https;branch=master"


S = "${WORKDIR}/git"

inherit vdr-plugin

RDEPENDS:${PN} += "imagemagick"

DEPENDS += " \
	freetype \
	imagemagick \
	vdr-font-symbols \
	"
EXTRA_OEMAKE = " \
	STRIP=/bin/true \
	SDKSTAGE="${STAGING_DIR_HOST}" \
	IMAGELIB="imagemagick" \
	"

do_install() {
	oe_runmake DESTDIR=${D} install
}

FILES:${PN} += " \
	${sysconfdir}/vdr/themes/* \
	${sysconfdir}/vdr/plugins/skinsimple/* \
	${datadir}/vdr/plugins/skinsimple/* \
	"
