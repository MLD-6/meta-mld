SUMMARY = "mlist plugin for VDR"
AUTHOR = "Joachim Wilke"
PV = "1.0.2"
PR = "r4"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=393a5ca445f6965873eca0259a17f833"

SRCREV = "37ee0d20cc644604bdd04f47cf5c59425885d7c8"
SRC_URI = "git://github.com/jowi24/vdr-mlist;protocol=https;branch=master"

S = "${WORKDIR}/git"

inherit vdr-plugin

do_install() {
	oe_runmake DESTDIR=${D} install
}
