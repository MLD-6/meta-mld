SUMMARY = "VDR plugin wirbelscan"
HOMEPAGE = "https://www.gen2vdr.de/wirbel/wirbelscan/index2.html"
AUTHOR = "Winfried Koehler <w_scan@gmx-topmail.de>"
DESCRIPTION = "VDR plugin to scan for DVB channels"

PV = "2024.09.15"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "https://www.gen2vdr.de/wirbel/wirbelscan/vdr-wirbelscan-${PV}.tgz"
SRC_URI[sha256sum] = "22317c5a919834d70aee309248e7fb8b9f458819dee0e5ccdbedee7fdada8913"

SRC_URI += " \
	file://makefile-cxx.patch \
	file://add-status-request.patch \
	"

S = "${WORKDIR}/wirbelscan-${PV}"

DEPENDS += "librepfunc"

inherit vdr-plugin

do_install () {
	oe_runmake DESTDIR=${D} install
}
