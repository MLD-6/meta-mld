SUMMARY = "VDR HD output device "
PV = "git"
PR = "r4"
COMPATIBLE_MACHINE = "(raspberrypi|raspberrypi2|raspberrypi3|raspberrypi4|raspberrypi4-64|raspberrypi5|rock-pi-4b|hardkernel-odroidn2plus|tinker-board|tinker-board-s)"

LICENSE = "AGPL-3.0-only"


#SRC_URI = "git://github.com/rofehr/vdr-plugin-softhddevice-drm.git;protocol=https;branch=drm-atomic-gles"
#SRCREV = "9eb47219d5bbfbbe3b8c3b032f063d6e5c352a8f"

SRC_URI = "git://github.com/zillevdr/vdr-plugin-softhddevice-drm.git;protocol=https;branch=drm"
SRCREV = "c1cd90ceb88c5a1e46a42b7378cf58ff391abbc5"

SRC_URI += " \
	file://frontend.sh \
	file://run.sh \
	file://vdr-plugin-softhddevice-drm.menu \
	file://01_mediaplayer.patch \	
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

RREPLACES:${PN} += "vdr-plugin-frontend"
RCONFLICTS:${PN} += "vdr-plugin-frontend"
RPROVIDES:${PN} += "vdr-plugin-frontend"

DEPENDS += " \
	freetype \
	virtual/libgl \
	virtual/libgles2 \
	virtual/egl \
	libmms \
	glm \
	ffmpeg \
	"

EXTRA_OEMAKE = " \
	STRIP=/bin/true \
	"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[${PLUGIN_NAME}]" > ${D}${sysconfdir}/vdr/conf.d/30_${PLUGIN_NAME}.conf
	install -d ${D}${sharedir}/vdr/frontend.d
	install -m 0755 ${WORKDIR}/frontend.sh  ${D}${sharedir}/vdr/frontend.d/50_softhddevice-drm.sh
	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_rpihddevice.sh
	install -d ${D}${sharedir}/menu
	install -m 0644 ${WORKDIR}/vdr-plugin-softhddevice-drm.menu ${D}${sharedir}/menu
}

FILES:${PN} += " \
	${sharedir}/vdr/frontend.d/* \
	${sharedir}/vdr/run.d/* \
	${sharedir}/menu/* \
	"
