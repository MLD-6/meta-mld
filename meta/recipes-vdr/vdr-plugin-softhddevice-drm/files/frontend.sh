#!/bin/sh

case "$1" in
  start)
    ${0%/*/*}/frontend.sh resume
    trap 'trap "" TERM; kill -s TERM -- -$$ 2>/dev/null' TERM # kill childs on SIGTERM
    sleep infinity & wait
    ${0%/*/*}/frontend.sh suspend
    ;;
  resume)
    if ! svdrpsend.sh "PRIM" | grep ^250 | grep -q "softhddevice-drm"; then
      softdev=$(svdrpsend.sh "LSTD" | grep ^250 | grep "softhddevice-drm" | sed "s/250-/250 /g" | tail -n1 | cut -f 2 -d " ")
      svdrpsend.sh "PRIM ${softdev}" >/dev/null
    fi
    ;;
  suspend)
    otherdev=$(svdrpsend.sh "LSTD" | grep ^250 | grep -v "softhddevice-drm" | sed "s/250-/250 /g" | head -n1 | cut -f 2 -d " ")
    svdrpsend.sh "PRIM ${otherdev}" >/dev/null
    ;;
  stop)
    ;;
esac
