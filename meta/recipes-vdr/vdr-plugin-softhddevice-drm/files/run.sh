#!/bin/sh

case "$1" in
	start)
		# disable VDR frontend if another app is active
		cat >> /etc/vdr/setup.conf.add <<- EOF
			!PrimaryDVB = $(test "$(setting get apps.app.title)" = "VDR" && echo 1 || echo 2)
		EOF

		eval $(setting get --sh display.active[0])
		args=$(setting get vdr.plugins.softhddevice.args)
		args="$args -d ${resolution:-1920x1080}@${refreshrate:-50}"
		echo -e "[softhddevice-drm]\n$args" > /etc/vdr/conf.d/30_softhddevice-drm.conf		
		;;
esac
