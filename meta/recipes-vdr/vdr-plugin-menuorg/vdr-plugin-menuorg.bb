SUMMARY = "VDR menuorg plugin"
HOMEPAGE = "http://www.vdr-wiki.de/wiki/index.php/Menuorg-plugin"
AUTHOR = "Thomas Creutz <thomas.creutz@gmx.de>, Tobias Grimm <vdr@e-tobi.net>"
PV = "git"
PR = "r1"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRCREV = "203218929551ec4c6f1dd45c6d8012c214eaa2c8"
SRC_URI = "git://vdr-projects.e-tobi.net/git/vdr-plugin-menuorg;protocol=https;branch=master"

SRC_URI += " \
	file://vdr-menuorg-fix-plugin-crator.patch \
	"
SRC_URI += " \
	file://run.sh \
	file://menu.xml \
	"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += "glibmm libxml++"

sharedir = "${prefix}/share"

do_install() {
	oe_runmake DESTDIR=${D} install-lib install-i18n

	install -d ${D}${sysconfdir}/vdr
	install -m 0644 ${WORKDIR}/menu.xml ${D}${sysconfdir}/vdr
	install -d ${D}${sharedir}/vdr/run.d
	install -m 0755 ${WORKDIR}/run.sh ${D}${sharedir}/vdr/run.d/30_menuorg.sh
	install -d ${D}${sysconfdir}/vdr/conf.d
	echo "[${PLUGIN_NAME}]\n-c /etc/vdr/menu.xml" > ${D}${sysconfdir}/vdr/conf.d/50_${PLUGIN_NAME}.conf
}

CONFFILES:${PN} += " \
  ${sysconfdir}/vdr/* \
  "
FILES:${PN} += " \
	${sharedir}/vdr/* \
	"
