#!/bin/sh

case "$1" in
	start)
		# update app start menu
		menu=/etc/vdr/menu.xml
		sed "/<menu name=\"Apps\">/,/<\/menu>/d; /<command name=\"Exit\"/d" -i $menu
		#if [ "$(setting get apps.app.title)" = "VDR" -a "$(setting get apps.autostart)" != "VDR" ]; then
		if [ "$(setting get apps.autostart)" != "VDR" ]; then
			sed "s|^\(</menus>\)|\t<command name=\"Exit\" execute=\"setting call apps.start\" />\n\1|" -i $menu
		fi
		setting call --sh apps.all | \
		while read values; do
			hide=
			eval $values
			if [ "$title" != "VDR" -a "$title" != "System-Setup" -a "$hide" != "true" ]; then
				if ! grep -q "<menu name=\"Apps\">" $menu; then
					sed "s|^\(\t<menu name=\"System\">\)|\t<menu name=\"Apps\">\n\t</menu>\n\1|" -i $menu
				fi
				sed "s|\(.*\)\(<menu name=\"Apps\">\)|\1\2\n\1\t<command name=\"$title\" execute=\"setting call apps.start '$title'\" />|" -i $menu
			fi
		done
		;;
esac
