SUMMARY = "Output for Targa VFD"
AUTHOR = "Andreas Brachold <vdr07 AT deltab de>"
PV = "0.3.2"
PR = "r1"

LICENSE = "GPL-3.0-only"

SRC_URI = "git://github.com/vdr-projects/vdr-plugin-targavfd.git;protocol=https;branch=master"
SRCREV = "36dda7933465fd556b3b0ec848923aebe15fbe87"
SRC_URI[sha256sum] = "62d069d87f75e32d10eec0d4e37a1a2294d361d244e37e062dc212500bfb49a3"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
    libusb \
    "

do_install() {
	oe_runmake DESTDIR=${D} install
}

