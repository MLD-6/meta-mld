SUMMARY = "roboTV is a Android TV based frontend for VDR. This is the VDR server part."
AUTHOR = " Alexander Pipelka   alexander.pipelka@gmail.com"
PV = "0.15.0"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/pipelka/vdr-plugin-robotv.git;protocol=https;branch=master"
SRCREV = "e9246b24cbdc855ff1bd87995f09b41cc77f41dc"

S = "${WORKDIR}/git"

inherit vdr-plugin

DEPENDS += " \
    "

do_install() {
	oe_runmake DESTDIR=${D} install
}
