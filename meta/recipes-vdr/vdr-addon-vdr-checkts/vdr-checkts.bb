DESCRIPTION = "vdr-checkts parses VDR recordings (only TS-recordings / vdr 1.7.x) and records any discontinuities in the transport stream by checking the continuity counter."
SUMMARY = "Check continuity of a VDR recording"
HOMEPAGE = "https://github.com/vdr-projects/vdr-checkts/"
AUTHOR = "e-tobi"
SECTION = "console/multimedia"

PV = "1.0+git${SRCPV}"
SRCREV = "0e7880576c0dcf84eaa53cd9c73bc2b5bf1e8d01"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYRIGHT;md5=a3eea6f28ebccaba1c87b05db00aaf6a"

SRC_URI = "git://github.com/vdr-projects/vdr-checkts.git;protocol=https;branch=master"

# "CC=g++" in Makefile kills compilations for all platforms other than x86_64
SRC_URI += "file://Makefile.patch"

S = "${WORKDIR}/git"

CXXFLAGS = "-fPIE"


do_compile () {
	oe_runmake
}

do_install () {
	install -d ${D}${bindir}
	install -m 0755 ${S}/vdr-checkts                               ${D}${bindir}/vdr-checkts
}
