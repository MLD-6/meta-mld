SUMMARY = "VDR skinnopacity plugin"
PV = "git"
PR="r4"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRCREV = "0cf4b3114831c1e6c28cdb1d60af8057edaf5a0f"
SRC_URI = "git://gitlab.com/kamel5/SkinNopacity.git;protocol=https;branch=master"

#SRC_URI += " \
#	file://vdr_2.4.0_compat.patch \
#	file://imagemagickwrapper.diff \
#	"

S = "${WORKDIR}/git"

inherit vdr-plugin

RDEPENDS:${PN} += "imagemagick"

DEPENDS += " \
	freetype \
	graphicsmagick \
	vdr-font-symbols \
	"

EXTRA_OEMAKE = " \
	STRIP=/bin/true \
	SDKSTAGE="${STAGING_DIR_HOST}" \
	IMAGELIB="graphicsmagick" \
	"

do_install() {
	oe_runmake DESTDIR=${D} install
	install -d ${D}${sharedir}/vdr/plugins/skinnopacity
	ln -sf ${sharedir}/vdr/channellogos/ ${D}${sharedir}/vdr/plugins/skinnopacity/logos
}

FILES:${PN} += " \
	${sysconfdir}/vdr/themes/ \
	${sysconfdir}/vdr/plugins/skinnopacity/ \
	${sharedir}/vdr/plugins/skinnopacity/ \
	"
