#!/bin/sh

case "$1" in
	start)
		args=$(setting get -d "-f -w alsa-driver-broken -v cuvid" vdr.plugins.softhddevice.args)

		# disable VDR frontend if another app is active
		if [ "$(setting get apps.app.title)" != "VDR" ] || ! xset -d :0 q &>/dev/null; then
			args="$args -D"
		fi

		echo -e "[softhddevice]\n$args" > /etc/vdr/conf.d/30_softhddevice.conf
		;;
esac
