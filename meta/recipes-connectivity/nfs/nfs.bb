SUMMARY:${PN}-client = "NFS client utils"
SUMMARY:${PN}-server = "NFS server utils"
DESCRIPTION:${PN}-client = "This package provides nfs client utilities"
DESCRIPTION:${PN}-server = "This package provides nfs utilities and server base configuration files"
HOMEPAGE = "http://nfs.sourceforge.net/"
SECTION = "console/network"

LICENSE="CLOSED"

SRC_URI += " \
	file://exports \
	"

RDEPENDS:${PN}-client = "nfs-utils-client"
RDEPENDS:${PN}-server = "nfs-utils-client nfs-utils"

PACKAGES = "${PN}-client ${PN}-server"

do_install() {
	install -d ${D}${sysconfdir}
	install -m 0644 ${WORKDIR}/exports ${D}${sysconfdir}
}

ALLOW_EMPTY:${PN}-client = "1"

CONFFILES:${PN}-server = " \
  ${sysconfdir} \
  "
FILES:${PN}-server = " \
  ${sysconfdir} \
  "
