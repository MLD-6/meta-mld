SUMMARY = "lircd2uinput deamon"
DESCRIPTION = "Forward LIRC button presses as uinput events"
PV = "git"
PR = "r0"

LICENSE = "GPL-2.0-only"

SRC_URI += " \
	file://lircd2uinput \
	file://lircd2uinput.service \
	"

inherit systemd

SYSTEMD_SERVICE:${PN} = "lircd2uinput.service"

RDEPENDS:${PN} = " \
	python3 \
	python3-dbus \
	python3-pygobject \
	python3-uinput \
	"

S = "${WORKDIR}/git"

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/lircd2uinput ${D}${bindir}/

	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/lircd2uinput.service ${D}${systemd_unitdir}/system
}

CONFFILES:${PN} += " \
  ${sysconfdir}/* \
  "
FILES:${PN} += " \
	${systemd_unitdir}/system/* \
	${bindir}/* \
	"
