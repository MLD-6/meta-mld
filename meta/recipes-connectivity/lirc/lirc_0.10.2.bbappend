FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://lircd-uinput.service"

EXTRA_OECONF += "--enable-uinput"

CONFFILES:${PN} += " \
  ${sysconfdir}/lirc/* \
  "

do_install:append() {
  if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'true', 'false', d)}; then
    install -m 0644 ${WORKDIR}/lircd-uinput.service ${D}${systemd_unitdir}/system/
  fi
  rm -f ${D}${systemd_unitdir}/system/lircd.socket
}

FILES:${PN}:remove = "${systemd_unitdir}/system/lircd.socket"
