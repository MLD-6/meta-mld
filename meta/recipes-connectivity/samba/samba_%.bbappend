FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
	file://smb.conf \
	file://service.mkdir.conf \
	"


do_install:append() {
	install -m644 ${WORKDIR}/smb.conf ${D}${sysconfdir}/samba
	install -d ${D}${systemd_unitdir}/system
	install -d ${D}/${systemd_unitdir}/system/nmb.service.d
	install -m 0644 ${WORKDIR}/service.mkdir.conf ${D}/${systemd_unitdir}/system/nmb.service.d/mkdir.conf
	install -d ${D}/${systemd_unitdir}/system/smb.service.d
	install -m 0644 ${WORKDIR}/service.mkdir.conf ${D}/${systemd_unitdir}/system/smb.service.d/mkdir.conf
}

FILES:${BPN}-common:append = "${sysconfdir}/samba"
FILES:${PN}-base:append = " \
	${systemd_unitdir}/system/nmb.service.d \
	${systemd_unitdir}/system/smb.service.d \
	"
