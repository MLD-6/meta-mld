FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://vdr-svdrp.service \
            file://tv.service \
            file://music.service \
            file://photo.service \
            file://video.service \
            file://avahi-daemon.conf \
            file://avahi-dnsconfd.action \
            file://avahi-autoipd.action \
            file://hosts \
            file://avahi-dbus.conf \
           "

EXTRA_OECONF += " \
    --enable-dbus \
    --with-autoipd-user=root \
    --with-autoipd-group=root \
    --with-avahi-user=root \
    --with-avahi-group=root "

do_install:append() {
    install -m 0644 -D ${WORKDIR}/vdr-svdrp.service ${D}${sysconfdir}/avahi/services/vdr-svdrp.service
    install -m 0644 -D ${WORKDIR}/tv.service ${D}${sysconfdir}/avahi/services/tv.service
    install -m 0644 -D ${WORKDIR}/video.service ${D}${sysconfdir}/avahi/services/video.service
    install -m 0644 -D ${WORKDIR}/music.service ${D}${sysconfdir}/avahi/services/music.service
    install -m 0644 -D ${WORKDIR}/photo.service ${D}${sysconfdir}/avahi/services/photo.service

    install -m 0644 -D ${WORKDIR}/avahi-daemon.conf ${D}${sysconfdir}/avahi/avahi-daemon.conf
    install -m 0644 -D ${WORKDIR}/avahi-dnsconfd.action ${D}${sysconfdir}/avahi/avahi-dnsconfd.action
    install -m 0644 -D ${WORKDIR}/avahi-autoipd.action ${D}${sysconfdir}/avahi/avahi-autoipd.action
    install -m 0644 -D ${WORKDIR}/hosts ${D}${sysconfdir}/avahi/hosts

    install -m 0644 -D ${WORKDIR}/avahi-dbus.conf ${D}${sysconfdir}/dbus-1/system.d/avahi-dbus.conf    

}

FILES:${BPN}-common:append= " \
	${sysconfdir}/avahi/services/* \
    ${sysconfdir}/avahi/* \
    ${sysconfdir}/dbus-1/system.d/* \
	"