LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://src/r8168.h;endline=27;md5=99e617438162efb5408a104180d6070e"

PV = "8.051.02+git${SRCPV}"

SRCREV = "503086686ea7b08b8b9b323ab52991987dfd9f6a"
SRC_URI = "git://github.com/mtorromeo/r8168.git;protocol=https;branch=master"

S = "${WORKDIR}/git"

inherit module

EXTRA_OEMAKE += "KERNELDIR=${STAGING_KERNEL_DIR}"

# prevent make trying to install them into hosts /lib/modules during do_compile
MAKE_TARGETS = "modules"

# it's called just install, not modules_install
MODULES_INSTALL_TARGET = "install"