SUMMARY = "yausbir lirc deamon"
DESCRIPTION = "lirc for yausbir remote control receiver"
PR = "r0"

LICENSE = "GPL-2.0-only"

SRCREV = "e82887b3db12c340efdd2ccd868b006d98795b80"
SRC_URI = "git://github.com/FireFlyVDR/lirc-drv-yausbir.git;protocol=https;branch=lirc-0.10.1"
SRC_URI += "file://Makefile_remove_m64.patch"

DEPENDS = "lirc libusb"
RDEPENDS:${PN} = "lirc"

S = "${WORKDIR}/git"

inherit pkgconfig

TARGET_CC_ARCH += "${LDFLAGS}"

do_compile() {
	oe_runmake 'CC=${CC}' ya_usbir.so
}

do_install() {
	install -d ${D}${datadir}/lirc/configs
	install -m 0644 ${S}/ya_usbir.conf ${D}${datadir}/lirc/configs

	install -d ${D}${sysconfdir}/lirc/lircd.conf.d
	install -m 0644 ${S}/yaUsbIR_V3_lircd.conf ${D}${sysconfdir}/lirc/lircd.conf.d/

	install -d ${D}${libdir}/lirc/plugins
	install -m 0755 -D ${S}/ya_usbir.so ${D}${libdir}/lirc/plugins
}

FILES:${PN} += " \
	${datadir}/lirc/configs/* \
	${sysconfdir}/lircd.conf.d/* \
	${libdir}/lirc/plugins/* \
	"
