SUMMARY = "FOX Toolkit Library"
HOMEPAGE = "http://www.fox-toolkit.org/"
PV = "1.6.57"
PR = "r0"

LICENSE = "LGPL-2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=fbc093901857fcd118f065f900982c24"

SRC_URI = "http://fox-toolkit.org/ftp/fox-${PV}.tar.gz"
SRC_URI[md5sum] = "675ddeac64eef88d9f7360abaa56b995"

S = "${WORKDIR}/fox-${PV}"

inherit autotools

DEPENDS = " \
    libxft \
    freetype \
"

REQUIRED_DISTRO_FEATURES = "x11"

EXTRA_OECONF = " \
    --enable-shared \
    --enable-static \
    --with-x \
    "
do_install:append() {
    # Install dev package files
    install -d ${D}${includedir}/fox
    install -m 0644 ${S}/include/* ${D}${includedir}/fox/
}
