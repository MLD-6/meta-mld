DESCRIPTION = "Detect required Xorg driver and install it at boot up"
SUMMARY = "Xorg driver detection and installation"
AUTHOR = "Claus Muus"
HOMEPAGE = "http://www.mld.de"
SECTION = "base/utils"
PR = "r3"

LICENSE="CLOSED"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI = " \
	file://xorg-driver-detect.sh \
	file://xorg-driver-detect.service \
	file://vdr_run.sh \
	file://xorg_conf.sh \
	"

inherit systemd

SYSTEMD_SERVICE:${PN} = "xorg-driver-detect.service"

FILES:${PN} += " \
	${bindir} \
	${libdir} \
	${systemd_unitdir}/system \
	${datadir}/vdr/run.d \
	${sysconfdir}/X11/xorg.conf.d \
	"

do_install () {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/xorg-driver-detect.sh ${D}${bindir}
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/xorg-driver-detect.service ${D}${systemd_unitdir}/system

	install -d ${D}${datadir}/vdr/run.d
	install -m 0755 ${WORKDIR}/vdr_run.sh ${D}${datadir}/vdr/run.d/30_intel.sh

	install -d ${D}/${sysconfdir}/X11/xorg.conf.d
	install -m 0755 ${WORKDIR}/xorg_conf.sh ${D}/${sysconfdir}/X11/xorg.conf.d/30_intel.sh
}
