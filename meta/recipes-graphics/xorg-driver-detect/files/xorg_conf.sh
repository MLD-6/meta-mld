#!/bin/sh

conffile=${0%.*}Device.add

rm -f $conffile

if lsmod | grep -q "^i915 "; then
	if setting get display.driver | grep -q iHD; then
		cat > $conffile <<- EOF
			  Driver      "modesetting"
			  Option      "DRI"            "3"
			  Option      "AccelMethod"    "glamor"
		EOF
	else
		# use intel driver on old (non iHD) intel systems
		cat > $conffile <<- EOF
			  #Driver      "intel"
			  Driver      "modesetting"
			  Option      "DRI"  "crocus"
		EOF
	fi
fi
