#!/bin/sh
# detect and install the needed nvidia driver package

{
	package=$(setting get display.detect | grep nvidia | head -n1)
	if [ "$package" ] && ! dpkg-query --show -f="\${db:Status-Abbrev}\${Package}\n" | grep -q "^ii $package$"; then
		export DEBIAN_FRONTEND=noninteractive
		apt-get -q update
		apt-get -q -y install $package
	fi
} | while read line; do
	echo -e "$line\r"
	[ "$line" ] || continue
	[ "${line##Get*}" ] || ([ "${line##*Packages*}" -a "${line##*InRelease*}" ] && plymouth display-message --text="Downloading $(echo "$line" | cut -d " " -f4)" || plymouth display-message --text="Downloading updates")
	[ "${line##Unpacking*}" ] || plymouth display-message --text="$(echo "$line" | cut -d " " -f1-2)"
	[ "${line##Setting up*}" ] || plymouth display-message --text="$(echo "$line" | cut -d " " -f1-3)"
done
