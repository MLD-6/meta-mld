#!/bin/sh

case "$1" in
	start)
		if lsmod | grep -q "^i915 "; then
			if ! setting get display.driver | grep -q iHD; then
				# disable libav iHD driver on old intel systems
				export LIBVA_DRIVER_NAME=i965
			fi
		fi
		;;
esac
