# Read this first: https://devtalk.nvidia.com/default/topic/915640/unix-graphics-announcements-and-news/multiple-glx-client-libraries-in-the-nvidia-linux-driver-installer-package/

do_install:append() {
	install -d ${D}/${sysconfdir}/X11/xorg.conf.d
	echo '    Driver      "nvidia"\n    Option      "NoLogo" "true"' > ${D}/${sysconfdir}/X11/xorg.conf.d/30_nvidiaDevice.add
#	install -m 755 ${WORKDIR}/30_nvidia.sh ${D}/${sysconfdir}/X11/xorg.conf.d/

	install -d ${D}${libdir}
	install -m 755 ${NVIDIA_SRC}/libEGL_nvidia.so.${PV} ${D}${libdir}/
	ln -sf libEGL_nvidia.so.${PV} ${D}${libdir}/libEGL_nvidia.so.0
	install -m 755 ${NVIDIA_SRC}/libGLESv1_CM_nvidia.so.${PV} ${D}${libdir}/
	ln -sf libGLESv1_CM_nvidia.so.${PV} ${D}${libdir}/libGLESv1_CM_nvidia.so.1
	install -m 755 ${NVIDIA_SRC}/libGLESv2_nvidia.so.${PV} ${D}${libdir}/
	ln -sf libGLESv2_nvidia.so.${PV} ${D}${libdir}/libGLESv2_nvidia.so.2
	install -m 755 ${NVIDIA_SRC}/libGLX_nvidia.so.${PV} ${D}${libdir}/
	ln -sf libGLX_nvidia.so.${PV} ${D}${libdir}/libGLX_nvidia.so.0
	ln -sf libGLX_nvidia.so.${PV} ${D}${libdir}/libGLX_indirect.so.0

	install -m 755 ${NVIDIA_SRC}/libnvidia-tls.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-glcore.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-eglcore.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-ml.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-fbc.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-glsi.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-encode.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-opencl.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-ptxjitcompiler.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-cfg.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-gtk2.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-gtk3.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libcuda.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvcuvid.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libvdpau_nvidia.so.${PV} ${D}${libdir}/libvdpau_nvidia.so

	# xf86-video-nvidia
	install -d ${D}${libdir}/xorg/modules/drivers
	install -m 755 ${NVIDIA_SRC}/nvidia_drv.so ${D}${libdir}/xorg/modules/drivers/
	install -d ${D}${libdir}/xorg/modules/extensions
	install -m 755 ${NVIDIA_SRC}/libglxserver_nvidia.so.${PV} ${D}${libdir}/xorg/modules/extensions/libglxserver_nvidia.so

	# nvidia
	install -d ${D}${bindir}
	install -m 755 -D ${NVIDIA_SRC}/nvidia-settings ${D}${bindir}/nvidia-settings
	install -m 755 -D ${NVIDIA_SRC}/nvidia-debugdump ${D}${bindir}/nvidia-debugdump
	install -m 755 -D ${NVIDIA_SRC}/nvidia-smi ${D}${bindir}/nvidia-smi
	install -m 755 -D ${NVIDIA_SRC}/nvidia-modprobe ${D}${bindir}/nvidia-modprobe
	install -m 755 -D ${NVIDIA_SRC}/nvidia-xconfig ${D}${bindir}/nvidia-xconfig
	install -m 755 -D ${NVIDIA_SRC}/nvidia-cuda-mps-server ${D}${bindir}/nvidia-cuda-mps-server
	install -m 755 -D ${NVIDIA_SRC}/nvidia-cuda-mps-control ${D}${bindir}/nvidia-cuda-mps-control
	install -m 755 -D ${NVIDIA_SRC}/nvidia-persistenced ${D}${bindir}/nvidia-persistenced

	# nvidia-doc
	install -d ${D}${datadir}/doc/nvidia ${D}${mandir}/man1
	install -m 644 -D ${NVIDIA_SRC}/html/* ${D}${datadir}/doc/nvidia/
	install -m 644 ${NVIDIA_SRC}/NVIDIA_Changelog ${D}${datadir}/doc/nvidia/
	install -m 644 ${NVIDIA_SRC}/README.txt ${D}${datadir}/doc/nvidia/
	install -m 444 -D ${NVIDIA_SRC}/nvidia-settings.1.gz ${D}${mandir}/man1/nvidia-settings.1.gz
	install -m 444 -D ${NVIDIA_SRC}/nvidia-smi.1.gz ${D}${mandir}/man1/nvidia-smi.1.gz
	install -m 444 -D ${NVIDIA_SRC}/nvidia-xconfig.1.gz ${D}${mandir}/man1/nvidia-xconfig.1.gz
	install -m 444 -D ${NVIDIA_SRC}/nvidia-cuda-mps-control.1.gz ${D}${mandir}/man1/nvidia-cuda-mps-control.1.gz
	install -m 444 -D ${NVIDIA_SRC}/nvidia-persistenced.1.gz ${D}${mandir}/man1/nvidia-persistenced.1.gz
}

# python populate_packages:prepend() {
#     d.appendVar("RDEPENDS:nvidia", " xorg-abi-video-23")
# }

do_make_scripts[noexec] = "1"

REQUIRED_DISTRO_FEATURES = "x11"

FILES:${PN} += " \
	${sysconfdir} \
	${libdir} \
	${bindir} \
	"

FILES:${PN}-doc += " \
	${mandir} \
	${datadir}/doc/nvidia \
	"

RDEPENDS:${PN} += " \
	xserver-xorg-module-libwfb \
	libvdpau \
	"

INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"
INSANE_SKIP:${PN}:append = "ldflags already-stripped"
INSANE_SKIP:libgl-nvidia:append = "ldflags"
FILES_SOLIBSDEV = ""
