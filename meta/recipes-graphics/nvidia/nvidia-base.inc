SUMMARY = "NVidia Binary Graphics Driver"
LICENSE = "CLOSED"

NVIDIA_ARCHIVE_NAME = "NVIDIA-Linux-x86_64-${PV}"
NVIDIA_SRC = "${WORKDIR}/${NVIDIA_ARCHIVE_NAME}"

SRC_URI = "https://download.nvidia.com/XFree86/Linux-x86_64/${PV}/${NVIDIA_ARCHIVE_NAME}.run"
SRC_URI[sha256sum] = "7e95065caa6b82de926110f14827a61972eb12c200e863a29e9fb47866eaa898"

SRC_URI += " \
	file://30_nvidia.sh \
	"

S = "${NVIDIA_SRC}/kernel"

do_unpack() {
	rm -rf ${NVIDIA_SRC}
	sh ${DL_DIR}/${NVIDIA_ARCHIVE_NAME}.run -x --target ${NVIDIA_SRC}
}

do_make_scripts[noexec] = "1"
