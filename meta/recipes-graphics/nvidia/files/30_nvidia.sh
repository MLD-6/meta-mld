#!/bin/sh

conffile=${0%.*}Device.add

rm -f $conffile

if lsmod | grep -q "^nvidia$"; then
	cat > $conffile <<- EOF
				Driver      "nvidia"
				Option      "NoLogo" "true"
	EOF
fi
