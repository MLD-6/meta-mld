SYSTEMD_SERVICE:${PN} = ""
FILES:${PN} += "${systemd_unitdir}/system/xserver-nodm.service"

do_install:append () {
  sed -i 's:ARGS=":ARGS="vt2:' ${D}${sysconfdir}/default/xserver-nodm
}

# FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

# SRC_URI = " \
# 	file://xserver-nodm.socket \
#   "

# inherit systemd

# #SYSTEMD_SERVICE:${PN} = "xserver-nodm.socket"

# files = " \
#   ${systemd_unitdir}/system/xserver-nodm.service \
#   ${systemd_unitdir}/system/xserver-nodm.socket \
# 	"
# FILES:${PN} += "${files}"
# FILES:${PN}:rpi += "${files}"

# do_install:append () {
#   install -d ${D}/${systemd_unitdir}/system
# 	install -m 0644 ${S}/xserver-nodm.socket ${D}${systemd_unitdir}/system/
# }
