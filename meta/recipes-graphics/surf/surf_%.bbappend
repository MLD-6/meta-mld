FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
	file://0002-config-hide-background.patch \
	file://gtk.css \
	"

RDEPENDS:${PN} += "libgles2"

G = "/root/.config/gtk-3.0"
C = "/root/.surf/styles"

FILES:${PN} += " \
	${G}/gtk.css \
	${C}/default.css \
	"
CONFFILES:${PN} += " \
	${G}/gtk.css \
	${C}/default.css \
	"

do_install:append() {
 	install -d ${D}${G}
	install -m 0644 ${WORKDIR}/gtk.css ${D}${G}
 	install -d ${D}${C}
	touch ${D}${C}/default.css
}
