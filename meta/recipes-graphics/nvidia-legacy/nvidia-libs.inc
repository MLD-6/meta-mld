do_install() {
	install -d ${D}/${sysconfdir}/X11/xorg.conf.d
	echo '    Driver      "nvidia"\n    Option      "NoLogo" "true"' > ${D}/${sysconfdir}/X11/xorg.conf.d/30_nvidiaDevice.add
#	install -m 755 ${WORKDIR}/30_nvidia.sh ${D}/${sysconfdir}/X11/xorg.conf.d/
	echo 'Section "Files"\n  ModulePath  "/usr/lib/xorg/nvidia,/usr/lib/xorg/modules"\nEndSection' > ${D}/${sysconfdir}/X11/xorg.conf.d/60_files.conf
	echo 'Section "ServerFlags"\n  Option  "IgnoreABI" "true"\nEndSection' > ${D}/${sysconfdir}/X11/xorg.conf.d/60_serverflags.conf

	install -d ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}/drivers/video
	install -m 755 ${NVIDIA_SRC}/kernel/*.ko ${D}${nonarch_base_libdir}/modules/${KERNEL_VERSION}/drivers/video

	install -d ${D}${libdir}
	install -m 755 ${NVIDIA_SRC}/libnvidia-tls.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-glcore.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-eglcore.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-ml.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-fbc.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-glsi.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-encode.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-opencl.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-compiler.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-cfg.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-ifr.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvidia-wfb.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/tls/libnvidia-tls.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libcuda.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libnvcuvid.so.${PV} ${D}${libdir}/
	install -m 755 ${NVIDIA_SRC}/libvdpau.so.${PV} ${D}${libdir}/libvdpau.so
	install -m 755 ${NVIDIA_SRC}/libvdpau_nvidia.so.${PV} ${D}${libdir}/libvdpau_nvidia.so

	# xf86-video-nvidia
	install -d ${D}${libdir}/xorg/nvidia/drivers
	install -m 755 ${NVIDIA_SRC}/nvidia_drv.so ${D}${libdir}/xorg/nvidia/drivers/
	install -d ${D}${libdir}/xorg/nvidia/extensions
	install -m 755 ${NVIDIA_SRC}/libglx.so.${PV} ${D}${libdir}/xorg/nvidia/extensions/libglx.so

	# nvidia
	install -d ${D}${bindir}
	install -m 755 -D ${NVIDIA_SRC}/nvidia-settings ${D}${bindir}/nvidia-settings
	install -m 755 -D ${NVIDIA_SRC}/nvidia-debugdump ${D}${bindir}/nvidia-debugdump
	install -m 755 -D ${NVIDIA_SRC}/nvidia-smi ${D}${bindir}/nvidia-smi
	install -m 755 -D ${NVIDIA_SRC}/nvidia-modprobe ${D}${bindir}/nvidia-modprobe
	install -m 755 -D ${NVIDIA_SRC}/nvidia-xconfig ${D}${bindir}/nvidia-xconfig
	install -m 755 -D ${NVIDIA_SRC}/nvidia-cuda-mps-server ${D}${bindir}/nvidia-cuda-mps-server
	install -m 755 -D ${NVIDIA_SRC}/nvidia-cuda-mps-control ${D}${bindir}/nvidia-cuda-mps-control
	install -m 755 -D ${NVIDIA_SRC}/nvidia-persistenced ${D}${bindir}/nvidia-persistenced

	install -d ${D}${datadir}/nvidia
	install -m 644 ${NVIDIA_SRC}/nvidia-application-profiles-${PV}-key-documentation ${D}${datadir}/nvidia/

	# nvidia-doc
	install -d ${D}${datadir}/doc/nvidia
	install -m 644 -D ${NVIDIA_SRC}/html/* ${D}${datadir}/doc/nvidia/
	install -m 644 ${NVIDIA_SRC}/NVIDIA_Changelog ${D}${datadir}/doc/nvidia/
	install -m 644 ${NVIDIA_SRC}/README.txt ${D}${datadir}/doc/nvidia/
	install -d ${D}${mandir}/man1
	install -m 644 -D ${NVIDIA_SRC}/nvidia-settings.1.gz ${D}${mandir}/man1/nvidia-settings.1.gz
	install -m 644 -D ${NVIDIA_SRC}/nvidia-smi.1.gz ${D}${mandir}/man1/nvidia-smi.1.gz
	install -m 644 -D ${NVIDIA_SRC}/nvidia-xconfig.1.gz ${D}${mandir}/man1/nvidia-xconfig.1.gz
	install -m 644 -D ${NVIDIA_SRC}/nvidia-cuda-mps-control.1.gz ${D}${mandir}/man1/nvidia-cuda-mps-control.1.gz
	install -m 644 -D ${NVIDIA_SRC}/nvidia-persistenced.1.gz ${D}${mandir}/man1/nvidia-persistenced.1.gz
}

# python populate_packages:prepend() {
#     d.appendVar("RDEPENDS:nvidia-legacy", " xorg-abi-video-23")
# }

do_make_scripts[noexec] = "1"

REQUIRED_DISTRO_FEATURES = "x11"

FILES:${PN} += " \
	${sysconfdir} \
	${libdir} \
	${bindir} \
	${datadir}/nvidia \
	"

FILES:${PN}-doc += " \
	${mandir} \
	${datadir}/doc/nvidia \
	"

RDEPENDS:${PN} += " \
	xserver-xorg-module-libwfb \
	libvdpau \
	"
RREPLACES:${PN} += "nvidia"
RCONFLICTS:${PN} += "nvidia"
RPROVIDES:${PN} += "nvidia"

INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"
INSANE_SKIP:${PN} += "ldflags already-stripped"
FILES_SOLIBSDEV = ""
