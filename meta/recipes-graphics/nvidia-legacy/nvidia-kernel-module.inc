inherit module

do_compile[depends] += "virtual/kernel:do_shared_workdir"
EXTRA_OEMAKE += "SYSSRC=${STAGING_KERNEL_DIR} SYSOUT=${STAGING_KERNEL_BUILDDIR} ARCH=x86_64"
MAKE_TARGETS = "module"
