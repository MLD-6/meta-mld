SUMMARY = "NVidia Legacy Binary Graphics Driver"
LICENSE = "CLOSED"
PV = "340.108"
PR = "r1"

python () {
    kernel_version = d.getVar('KERNEL_VERSION') or '6.6'
    d.setVar('KV', kernel_version.split('.')[0]+'.'+kernel_version.split('.')[1])
}

NVIDIA_ARCHIVE_NAME = "NVIDIA-Linux-x86_64-${PV}-patched-${KV}"
NVIDIA_SRC = "${WORKDIR}/${NVIDIA_ARCHIVE_NAME}"

SRC_URI = "https://github.com/MeowIce/nvidia-legacy/raw/${PV}/${KV}/${NVIDIA_ARCHIVE_NAME}.run"
SRC_URI[sha256sum] = "45c3c9d666e61b748a55acbbbe8b71018d177df2cfa58be828886a3768124ada"

SRC_URI += " \
	file://30_nvidia.sh \
	"

S = "${NVIDIA_SRC}/kernel"

do_unpack() {
	rm -rf ${NVIDIA_SRC}
	sh ${DL_DIR}/${NVIDIA_ARCHIVE_NAME}.run -x --target ${NVIDIA_SRC}
}

do_make_scripts[noexec] = "1"
