SUMMARY = "A session manager for X"
DESCRIPTION = "A session manager for X, that provides just the right boilerplate to create a session and launch the browser "
HOMEPAGE = "https://www.minidvblinux.de"
BUGTRACKER = "https://www.minidvblinux.de/bug"
LICENSE = "CLOSED"

SECTION = "x11"
RCONFLICTS:${PN} = "matchbox-common"

SRC_URI = " \
	file://x-session \
	file://background.png \
	file://10background.sh \
	"
S = "${WORKDIR}"

RDEPENDS:${PN} = "feh"

inherit update-alternatives

ALTERNATIVE:${PN} = "x-session-manager"
ALTERNATIVE_TARGET[x-session-manager] = "${bindir}/x-session"
ALTERNATIVE_PRIORITY = "50"

do_install() {
	install -d ${D}/${bindir}
	install -m 0755 ${S}/x-session ${D}/${bindir}
	install -d ${D}/${datadir}/feh/
	install -m 0755 ${S}/background.png ${D}/${datadir}/feh/
	install -d ${D}/${sysconfdir}/X11/Xsession.d/
	install -m 0755 ${S}/10background.sh ${D}/${sysconfdir}/X11/Xsession.d/
}

FILES:${PN} += " \
	${bindir}/x-session \
	${datadir}/feh/background.png \
	${sysconfdir}/X11/Xsession.d/10background.sh \
	"
