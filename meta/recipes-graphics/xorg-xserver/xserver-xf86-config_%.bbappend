
FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI = " \
	file://xorg.conf.empty \
	file://xorg.conf.sh \
	file://xorg.conf.d/30_edid.sh \
	file://xorg.conf.d/30_mirror.sh \
	file://xorg.conf.d/30_resolution.sh \
	file://xorg.conf.d/50_screen.sh \
	file://xorg.conf.d/60_extensions.sh \
	file://xorg.conf.d/60_layout.sh \
	file://xorg.conf.d/70_kblayout.sh \
	file://xorg.conf.d/70_touchpad.sh \
	file://ready.sh \
	file://resolution.sh \
	file://xserver-nodm.service.xorg.conf \
	file://setmode \
	"

RDEPENDS:${PN} += "read-edid"

files = " \
	${sysconfdir}/X11/xorg.conf \
	${sysconfdir}/X11/xorg.conf.d/* \
	${sysconfdir}/X11/xorg.conf.sh \
	${sysconfdir}/X11/Xsession.d/ \
	${systemd_unitdir}/system/xserver-nodm.service.d/xorg.conf \
	${bindir}/setmode \
	"
FILES:${PN} += "${files}"
FILES:${PN}:rpi += "${files}"

do_install () {
	install -d ${D}/${sysconfdir}/X11/xorg.conf.d
	install -m 0644 ${S}/xorg.conf.empty ${D}/${sysconfdir}/X11/xorg.conf
	install -m 0755 ${S}/xorg.conf.sh ${D}/${sysconfdir}/X11/
	install -m 0755 -D ${S}/xorg.conf.d/*.sh ${D}/${sysconfdir}/X11/xorg.conf.d/
	install -d ${D}/${sysconfdir}/X11/Xsession.d/
	install -m 0755 ${WORKDIR}/ready.sh ${D}/${sysconfdir}/X11/Xsession.d/00ready.sh
	install -m 0755 ${WORKDIR}/resolution.sh ${D}/${sysconfdir}/X11/Xsession.d/20resolution.sh
	install -d ${D}/${systemd_unitdir}/system/xserver-nodm.service.d
	install -m 0644 ${S}/xserver-nodm.service.xorg.conf ${D}/${systemd_unitdir}/system/xserver-nodm.service.d/xorg.conf
	install -d ${D}/${bindir}
	install -m 0755 ${S}/setmode ${D}/${bindir}/
}
