#!/bin/sh

rm -f ${0%.*}.conf

eval $(setting get --sh display.touch)

conffile=${0%.*}.conf

if [ "$invert_x" = "true" ]; then
	cat >> $conffile <<- EOF
		Section "InputClass"
		  Identifier  "touchpad catchall"
		  Option      "InvertX" "true"
		EndSection
	EOF
fi

if [ "$invert_y" = "true" ]; then
	cat >> $conffile <<- EOF
		Section "InputClass"
		  Identifier  "touchpad catchall"
		  Option      "InvertY" "true"
		EndSection
	EOF
fi

if [ "$min_x" -a "$max_x" -a "$min_y" -a "$max_y" ]; then
	cat >> $conffile <<- EOF
		Section "InputClass"
		  Identifier  "touchpad catchall"
		  Option      "Calibration" "$min_x $max_x $min_y $max_y"
		EndSection
	EOF
fi

