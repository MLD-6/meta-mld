#!/bin/sh

rm -f ${0%.*}Screen?.add

setting get --sh display.active | \
while read values; do
	eval $values
	if [ "$i" = "0" ]; then
		main_screen=$screen
		main_resolution=$resolution
	elif [ "$screen" -a "$mirror" = "true" ]; then
		conffile=${0%.*}Screen${i}.add
		cat > $conffile <<- EOF
			  Option      "metamodes" "$main_screen: ${main_resolution:-nvidia-auto-select} +0+0, $screen: ${resolution:-nvidia-auto-select} +0+0"
		EOF
	fi
done
