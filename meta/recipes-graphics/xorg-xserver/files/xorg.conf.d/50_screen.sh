#!/bin/sh

rm -f ${0%.*}?.conf
rm -f ${0%.*}?ServerLayout.add

active="$(setting get --sh display.active)"
echo "${active:-i=0}" | \
while read values; do
	eval $values
	if [ "$i" = "0" -o "$screen" -a "$mirror" != "true" ]; then
		confpath=${0%/*}
		conffile=${0%.*}${i}.conf
		cat > $conffile <<- EOF
			Section "Screen"
			  Identifier  "Screen$i"
			  Device      "Device$i"
			  Monitor     "Monitor$i"
			  Option      "metamodes" "$screen:"
			$(cat $confpath/*Screen.add $confpath/*Screen$i.add 2>/dev/null)
			EndSection

			Section "Device"
			  Identifier  "Device$i"
			  Option      "DPI" "100x100"
			  Screen      $i
			$(cat $confpath/*Device.add $confpath/*Device$i.add 2>/dev/null)
			EndSection

			Section "Monitor"
			  Identifier  "Monitor$i"
			  Option      "DPMS" "off"
			$(cat $confpath/*Monitor.add $confpath/*Monitor$i.add 2>/dev/null)
			EndSection
		EOF

		if [ "$i" != "0" ]; then
			layoutfile=${0%.*}${i}ServerLayout.add
			cat > $layoutfile <<- EOF
				Screen      1 "Screen$i" RightOf "Screen$( $i - 1 )"
			EOF
		fi
	fi
done
