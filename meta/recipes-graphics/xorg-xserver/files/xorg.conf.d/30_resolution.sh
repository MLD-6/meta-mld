#!/bin/sh

rm -f ${0%.*}Screen?.add
rm -f ${0%.*}Monitor?.add

setting get --sh display.active | \
while read values; do
	eval $values
	if [ "$resolution" ]; then
		refreshrate=${refreshrate:-50}
		conffile=${0%.*}Screen${i}.add
		cat > $conffile <<- EOF
			  SubSection "Display"
			    Modes       "${resolution}" "${resolution}_${refreshrate}" "${resolution}_${refreshrate}.00"
			  EndSubSection
		EOF

		conffile=${0%.*}Monitor${i}.add
		cvt $(test "${resolution%%*i}" || echo -i) ${resolution%x*} ${resolution#*x} $refreshrate | sed "s/^/  /" > $conffile
	fi
done
