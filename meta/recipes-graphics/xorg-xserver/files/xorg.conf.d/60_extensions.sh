#!/bin/sh

conffile=${0%.*}.conf
cat > $conffile <<- EOF
	Section "Extensions"
	  Option      "Composite" "Disable"
	$(cat ${0%/*}/*Extensions.add 2>/dev/null)
	EndSection
EOF
