#!/bin/sh

exit

rm -f ${0%.*}.conf

locale=$(setting get system.locale)

if [ "$locale" ]; then
	conffile=${0%.*}.conf
	cat > $conffile <<- EOF
		Section "InputClass"
		  Identifier  "system-keyboard"
		  Option      "XkbLayout" "$(echo $locale | cut -b 1-2)"
		EndSection
	EOF
fi
