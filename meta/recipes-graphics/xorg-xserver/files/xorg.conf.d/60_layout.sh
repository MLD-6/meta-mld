#!/bin/sh

rm -f ${0%.*}.conf

conffile=${0%.*}.conf
cat > $conffile <<- EOF
	Section "ServerLayout"
		Identifier  "Layout0"
		Option      "BlankTime" "0"
		Option      "OffTime" "0"
		Option      "StandbyTime" "0"
		Option      "SuspendTime" "0"
		Screen      0 "Screen0"
	$(cat ${0%/*}/*ServerLayout.add 2>/dev/null)
	EndSection
EOF
