#!/bin/sh

rm -f ${0%.*}Device?.add

setting get --sh display.active | \
while read values; do
	eval $values
	if [ "$edid" ]; then
		echo -n "$edid" 1 | while read -n2 code; do test "$code" && printf "\\x$code"; done > /tmp/edid_$screen.bin
		conffile=${0%.*}Device${i}.add
		cat > $conffile <<- EOF
			  Option      "CustomEDID" "${screen}:/tmp/edid_${screen}.bin"
			  Option      "IgnoreEDID" "false"
			  Option      "UseEDID" "true"
		EOF
	fi
done
