#!/bin/sh

for script in $(ls ${0%.*}.d/*.sh 2>/dev/null | sort); do
  $script
done
