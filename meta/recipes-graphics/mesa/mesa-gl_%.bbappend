PACKAGECONFIG:append = " dri3 gallium"

# If we're using libmali, then we need to bring it in for the KHR/khrplatform.h file
DEPENDS .= "${@' libmali' if d.getVar('PREFERRED_PROVIDER_virtual/libgles1') == 'libmali' else ''}"
RDEPENDS:libgl-mesa-dev .= "${@' libmali-dev' if d.getVar('PREFERRED_PROVIDER_virtual/libgles1') == 'libmali' else ''}"

do_install:append () {
    if ${@'true' if d.getVar('PREFERRED_PROVIDER_virtual/libgles1') == 'libmali' else 'false'} ; then
        rm -rf ${D}${includedir}/KHR/*
    fi
}

# If we require libmali, this becomes MACHINE_ARCH specific
DEFAULT_PACKAGE_ARCH := "${PACKAGE_ARCH}"
MALI_PACKAGE_ARCH[vardepsexclude] = "MACHINE_ARCH"
MALI_PACKAGE_ARCH = "${MACHINE_ARCH}"
PACKAGE_ARCH[vardepsexclude] = "MALI_PACKAGE_ARCH"
PACKAGE_ARCH = "${@'${MALI_PACKAGE_ARCH}' if d.getVar('PREFERRED_PROVIDER_virtual/libgles1') == 'libmali' else '${DEFAULT_PACKAGE_ARCH}'}"
