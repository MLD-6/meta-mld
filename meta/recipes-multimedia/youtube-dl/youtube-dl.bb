DESCRIPTION = "YouTube downloader tool youtube-dl"
SUMMARY = "YouTube downloader"
SECTION = "multimedia"
BPV = "2021.12.17"
PR = "r0"
SRCREV = "${AUTOREV}"
PV = "${BPV}+git${SRCPV}"

LICENSE="PD"
LIC_FILES_CHKSUM = "file://LICENSE"

SRC_URI = "git://github.com/ytdl-org/youtube-dl.git;protocol=https;branch=master"

DEPENDS = "python3 zip-native"
RDEPENDS:${PN} = "python3"

S = "${WORKDIR}/git"

inherit pkgconfig

FILES:${PN} += " \
	${bindir}/* \
	"

EXTRA_OEMAKE += "PYTHON='/usr/bin/env python3'"

do_compile() {
	oe_runmake youtube-dl
}

do_install () {
	install -d ${D}${bindir}
	install -m 0755 ${S}/youtube-dl ${D}${bindir}
}
