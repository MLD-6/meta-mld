SUMMARY = "Init sound cards"
DESCRIPTION = "Enable all sound devices and push up the mixer"
HOMEPAGE = "https://www.minidvblinux.de"
BUGTRACKER = "https://www.minidvblinux.de/bug"

LICENSE = "GPL-2.0-only"

SRC_URI += " \
	file://alsa-init.service \
	file://alsa-init.sh \
	file://alsa-update-asound-conf.sh \
	"

RDEPENDS:${PN} += "alsa-utils-alsactl alsa-utils-amixer alsa-utils-aplay"

FILES:${PN} += " \
	${sbindir}/* \
	${systemd_unitdir}/* \
	"

do_install() {
	install -d ${D}${systemd_unitdir}/system/sound.target.wants
	install -m 0644 ${WORKDIR}/alsa-init.service ${D}${systemd_unitdir}/system
	ln -s ../alsa-init.service ${D}${systemd_unitdir}/system/sound.target.wants/

	install -d ${D}${sbindir}
	install -m 0755 ${WORKDIR}/alsa-init.sh ${D}${sbindir}
	install -m 0755 ${WORKDIR}/alsa-update-asound-conf.sh ${D}${sbindir}
}
