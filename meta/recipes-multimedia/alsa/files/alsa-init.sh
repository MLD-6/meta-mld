#!/bin/sh

statefile=/var/lib/alsa/asound.state
id=$(aplay -l | md5sum | cut -d' ' -f1)

if [ ! -e $statefile ] || grep -q "^# Dummy file" $statefile || [ "$id" != "$(setting get audio.id)" ]; then
	alsactl init
	i=0
	while [ -d /proc/asound/card$i ]; do
		amixer -c $i scontrols | while read line; do
			if echo "$line" | grep -q -i "mic\|capture\| in"; then
				amixer -q -c $i set "'${line#*\'}" 0% mute nocap 2>/dev/null
			else
				amixer -q -c $i set "'${line#*\'}" 100% unmute 2>/dev/null
			fi
		done
		i=$(($i+1))
	done
	# alsactl -f $statefile store
	setting set audio.id $id

	if [ ! -e /etc/asound.conf -o ! "$(setting get audio.active[0].name)" ]; then
		alsa-update-asound-conf.sh
	fi
fi
