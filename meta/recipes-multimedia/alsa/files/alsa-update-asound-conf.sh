#!/bin/sh
#
# setting up the alsa configuration.

if [ "$1" = "-h" ]; then
	echo "Usage: $0 [DEVICE:CHANNELS] ..."
	echo "    DEVICE    Device to activate. (device channel or 'all' or empty for present hdmi)"
	echo "    CHANNELS  count of channels to use. (2 or 6 or 8)"
	exit
fi

rm -f /etc/asound.conf

cards=
device1="${1%:*}"
channels1="${1#*:}"
if [ "$device1" = "all" ]; then
	cards="$(LC_ALL=C aplay -l | grep "^card" | sed "s/card \([0-9]\+\).*device \([0-9]\+\).*/\1 \2 ${channels1:-2}/")"
else
	if [ "$device1" ]; then
		cards="$cards"$'\n'"$(echo "$device1" | sed "s/\([0-9]\+\) \([0-9]\+\).*/\1 \2 $channels1/")"
	else
		# present hdmi devices
		cards="$(setting get --sh audio.present | while read values; do eval $values; test "$card" -a "$device" && echo "$card $device ${channels1:-2}"; done)"
		if [ ! "$cards" ]; then
			# first hdmi device
			cards="$(LC_ALL=C aplay -l | grep "^card" | grep -i "hdmi" | head -n1 | sed "s/card \([0-9]\+\).*device \([0-9]\+\).*/\1 \2 ${channels1:-2}/")"
			# all hdmi devices
			# cards="$( (LC_ALL=C aplay -l | grep "^card" | grep -i "hdmi" || LC_ALL=C aplay -l | grep "^card" | head -n1) | sed "s/card \([0-9]\+\).*device \([0-9]\+\).*/\1 \2 ${channels1:-2}/")"
		fi
	fi	
	while [ $# != 0 ]; do
		shift
		device="${1%:*}"
		channels="${1#*:}"
		test "$device" -a "$channels" || break
		cards="$cards"$'\n'"$(echo "$device" | sed "s/\([0-9]\+\) \([0-9]\+\).*/\1 \2 $channels/")"
	done
fi
cards="$(echo "$cards" | sed "/^$/d")"

if [ ! "$cards" ]; then
	exit
fi

if [ $(echo "$cards" | wc -l) = 1 -a ! "$channels1" ]; then
	echo -e "$cards" | { 
		read card device channels
		cat <<- EOF
			pcm.!default {
			  type hw
			  card $card
			  device $device
			}
		EOF
	} > /etc/asound.conf
	exit
fi

max_channels=$(echo -e "$cards\n0 0 6" | cut -d" " -f3 | sort | tail -n1)

cat > /etc/asound.conf << EOF
pcm.!default {
  type upmix
  slave.pcm "plug:all"
  channels $max_channels
}

pcm.all {
  type route
  slave.pcm "multi"
$(
	offset=0
	echo "$cards" | while read card device channels; do
		test "$channels" || continue
		echo
		case $channels in
			2)
				cat <<- EOF2
				  ttable.0.$((0+$offset)) 0.5 # FL -> L
				  ttable.1.$((1+$offset)) 0.5 # FR -> R
				  ttable.2.$((0+$offset)) 0.2 # RL -> L
				  ttable.3.$((1+$offset)) 0.2 # RR -> R
				  ttable.4.$((0+$offset)) 0.3 # FC -> L
				  ttable.4.$((1+$offset)) 0.3 # FC -> R
				  ttable.5.$((0+$offset)) 0.1 # LFE -> L
				  ttable.5.$((1+$offset)) 0.1 # LFE -> R
					$(
						if [ $max_channels = 8 ]; then
							cat <<- EOF3
							  ttable.6.$((0+$offset)) 0.1 # SL -> L
							  ttable.7.$((1+$offset)) 0.1 # SR -> R
							EOF3
						fi
					)
				EOF2
				;;
			6)
				cat <<- EOF2
				  ttable.0.$((0+$offset)) 1.0 # FL -> FL
				  ttable.1.$((1+$offset)) 1.0 # FR -> FR
					$(
						if [ $max_channels = 6 ]; then
							cat <<- EOF3
							  ttable.2.$((2+$offset)) 1.0 # SL -> L
							  ttable.3.$((3+$offset)) 1.0 # SR -> R
							EOF3
						else
							cat <<- EOF3
							  ttable.2.$((2+$offset)) 0.7 # RL -> RL
							  ttable.3.$((3+$offset)) 0.7 # RR -> RR
							  ttable.6.$((2+$offset)) 0.3 # SL -> RL
							  ttable.7.$((3+$offset)) 0.3 # SR -> RR
							EOF3
						fi
					)
				  ttable.4.$((4+$offset)) 1.0 # FC -> FC
				  ttable.5.$((5+$offset)) 1.0 # LFE -> LFE
				EOF2
				;;
			8)
				cat <<- EOF2
				  ttable.0.$((0+$offset)) 1.0 # FL -> FL
				  ttable.1.$((1+$offset)) 1.0 # FR -> FR
				  ttable.2.$((2+$offset)) 1.0 # RL -> RL
				  ttable.3.$((3+$offset)) 1.0 # RR -> RR
				  ttable.4.$((4+$offset)) 1.0 # FC -> FC
				  ttable.5.$((5+$offset)) 1.0 # LFE -> LFE
				  ttable.6.$((6+$offset)) 1.0 # SL -> SL
				  ttable.7.$((7+$offset)) 1.0 # SR -> SR
				EOF2
				;;
		esac
		offset=$(($offset + $channels))
	done
)
}

pcm.multi {
  type multi
$(
	offset=0
	echo "$cards"$'\n'"-1 0 $max_channels" | while read card device channels; do
		test "$channels" || continue
		echo
		cat <<- EOF2
		  slaves.$offset.pcm "$(test $card = -1 && echo "null" || echo "hw:$card,$device")"
		  slaves.$offset.channels $channels
		EOF2
		i=0
		while [ $i -lt $channels ]; do
			cat <<- EOF2
			  bindings.$(($i+$offset)).slave $offset
			  bindings.$(($i+$offset)).channel $i
			EOF2
			i=$(($i+1))
		done
		offset=$(($offset + $channels))
	done
)
}
EOF
