SUMMARY = "cefbrowser to show HbbTV application and stream videos for the vdr-plugin-web"
SECTION = "multimedia"
LICENSE = "CLOSED"
PV = "git"
PR = "r2"


ARCH:aarch64 = "arm64"
ARCH:arm = "arm"
ARCH:x86-64 = "64"

SRC_URI = "git://github.com/Zabrimus/cefbrowser.git;protocol=https;branch=master;name=cefbrowser"
SRCREV_cefbrowser = "0115619c4d88d6be2fb772a964d09f41fee1defa"

SRC_URI += "git://github.com/gabime/spdlog.git;protocol=https;branch=v1.x;tag=v1.15.0;name=spdlog;destsuffix=git/subprojects/spdlog-1.15.0"
SRC_URI[spdlog.sha256sum] = "9962648c9b4f1a7bbc76fd8d9172555bad1871fdb14ff4f842ef87949682caa5"

SRC_URI += "https://wrapdb.mesonbuild.com/v2/spdlog_1.15.0-1/get_patch;name=spdlog_patch;downloadfilename=spdlog_1.15.0-1_patch.zip"
SRC_URI[spdlog_patch.sha256sum] = "72bd578ff0eb4f5a84e5bd63ddc7b2ece84aad5abb516b54c8750306ae78df28"

SRC_URI += "git://github.com/pulzed/mINI.git;protocol=https;branch=master;tag=0.9.14;name=mINI;destsuffix=git/subprojects/mINI-0.9.14"
SRC_URI[mINI.sha256sum] = "e9cdba30e2c9f43d72dc3e3460507be439b5bcb35933e06f0969c775d4ff12ab"

SRC_URI += "git://gitlab.com/eidheim/tiny-process-library.git;protocol=https;branch=master;name=tiny_process_library;destsuffix=git/subprojects/tiny-process-library-4bf0e59e64f18d3080a1ce4e853775de2181e993"
SRCREV_tiny_process_library = "4bf0e59e64f18d3080a1ce4e853775de2181e993"

SRC_URI += "https://www.sqlite.org/2023/sqlite-amalgamation-3420000.zip;name=sqlite"
SRC_URI[sqlite.sha256sum] = "1cc824d0f5e675829fa37018318fda833ea56f7e9de2b41eddd9f7643b5ec29e"

CEF_VERSION = "126.2.7+g300bb05+chromium-126.0.6478.115"
SRC_URI += "https://cef-builds.spotifycdn.com/cef_binary_${@d.getVar('CEF_VERSION').replace('+', '%2B')}_linux${ARCH}_minimal.tar.bz2;downloadfilename=cef_binary_${CEF_VERSION}_linux${ARCH}.tar.bz2;name=cef_${ARCH}"
SRC_URI[cef_64.sha256sum] = "a8585cf36d6b4afcdcaa193985d8dce121d9711545310ee87b147ccbc021e6e1"
SRC_URI[cef_arm64.sha256sum] = "8de1c71d50facafd4930b448ec2e4c42010722c290dde1b974a1dd1de55cb326"
SRC_URI[cef_arm.sha256sum] = "c48c171baf994fc1df11bf06a7f35bae652eb1d4706985ce3a6c5a47a50a3762"

SRCREV_FORMAT = "cefbrowser spdlog spdlog_patch mINI tiny_process_library sqlite cef"

SRC_URI += " \
	file://cefbrowser.service \
	"

S = "${WORKDIR}/git"

inherit meson pkgconfig systemd

SYSTEMD_SERVICE:${PN} = "cefbrowser.service"

DEPENDS = "cmake-native zlib openssl libgcrypt glib-2.0 nss cups libxcomposite libxdamage libxrandr libxkbcommon pango atk alsa-lib mesa"

do_unpack:append() {
    bb.build.exec_func('unpack', d)
}

unpack() {
	cp -r ${WORKDIR}/spdlog-1.15.0 ${S}/subprojects/
	cd ${S}/subprojects/spdlog-1.15.0
	patch -p1 -i ${S}/subprojects/packagefiles/spdlog-1.15.0/3301.patch
	patch -p1 -i ${S}/subprojects/packagefiles/spdlog-1.15.0/meson-build.patch
	cp -r ${S}/subprojects/packagefiles/mini-0.9.14/* ${S}/subprojects/mINI-0.9.14
	cp -r ${S}/subprojects/packagefiles/tiny-process-library-* ${S}/subprojects/
	cp -r ${WORKDIR}/sqlite-amalgamation-3420000 ${S}/subprojects/
	cp -r ${S}/subprojects/packagefiles/sqlite*/* ${S}/subprojects/sqlite-amalgamation-3420000/
	cp -r ${WORKDIR}/cef_binary_${CEF_VERSION}_linux64_minimal ${S}/subprojects/cef
}

do_install() {
	meson install --no-rebuild

	install -d ${D}${datadir}/cefbrowser
	cp -r ${WORKDIR}/build/Release/* ${D}${datadir}/cefbrowser

	install -d ${D}${sysconfdir}/cefbrowser
	install -m 0644 ${S}/config/sockets.ini ${D}${sysconfdir}/cefbrowser

	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/cefbrowser.service ${D}${systemd_unitdir}/system
}

CONFFILES:${PN} += " \
	${sysconfdir}/cefbrowser \
	"
FILES:${PN} += " \
	${datadir}/cefbrowser \
	${sysconfdir}/cefbrowser \
	"
