SUMMARY = "amlhddevice - A VDR Plugin to manage your Channellists"
PV = "git"
PR = "r1"

LICENSE = "CLOSED"

SRC_URI = "git://github.com/osmc/libamcodec.git;protocol=https;branch=master"
SRCREV = "4eebd67b142837fbb418abf5390060d4639440ba"

S = "${WORKDIR}/git"

inherit lib_package pkgconfig

do_install() {
	oe_runmake DESTDIR=${D} install
    install -D -m 0644 ${S}/amcodec/include/codec.h ${D}${includedir}/amcodec/include/codec.h
    #install -D -m 0644 ${WORKDIR}/libamcodec.pc ${D}${libdir}/pkgconfig/amcodec.pc
}

FILES:${PN} += " \
	${libdir}/xine/plugins/2.10/*.so \
	${libdir}/xine/plugins/2.10/*.types \
	${libdir}/xine/plugins/2.10/post/*.so \
	${datadir}/xine-lib/* \
	"
    
#/home/roland/mld-dev/build/tmp-glibc/work/core2-64-mld-linux/libamcodec/git-r1/git/amcodec/include/