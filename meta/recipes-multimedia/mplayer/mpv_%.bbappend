RRECOMMENDS:${PN} += "youtube-dl"

DEPENDS += " \
	libxrandr \
	"

#DEPENDS:remove:raspberrypi4 = "ffmpeg"
#DEPENDS:append:raspberrypi4 = "rpi-ffmpeg"

EXTRA_OECONF:append = " \
	--enable-libmpv-shared \
	"

PACKAGECONFIG:append:genericx86-64 = " vdpau vaapi opengl"

PACKAGECONFIG:append:raspberrypi4 = " drm"
