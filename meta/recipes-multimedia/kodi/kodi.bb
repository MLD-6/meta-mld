SUMMARY = "Meta package for Kodi Flatpak"
DESCRIPTION = "Ultimate entertainment center"

RDEPENDS:${PN} = "flatpak"

inherit packagegroup
inherit allarch

pkg_postinst:${PN}() {
  if [ -z "$D" ]; then
    flatpak install -y --noninteractive tv.kodi.Kodi
  fi
}

pkg_prerm:${PN}() {
  if [ -z "$D" ]; then
    flatpak uninstall -y --noninteractive tv.kodi.Kodi
  fi
}
