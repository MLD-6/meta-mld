FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
  file://71-infrared.rules \
  "

do_install:append() {
	install -d ${D}$${base_libdir}/udev/rules.d
	install -m 0644 ${WORKDIR}/71-infrared.rules ${D}${base_libdir}/udev/rules.d/
}

CONFFILES:rc-keymaps += " \
  ${sysconfdir}/rc_maps.cfg \
  "
