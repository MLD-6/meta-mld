#!/bin/sh

url=$(setting call mediathekhbbtv.url)
alsa=$(setting get audio.active[0].name | sed "s/\([0-9]\+\) \([0-9]\+\).*/hw:\1,\2/")
resolution=$(setting get "display.active[0].resolution||1920" | cut -dx -f1)
zoom=$(awk "BEGIN {print $resolution / 1280}")

cp /etc/mediathek-hbbtv/keyd.conf /etc/keyd/mediathek-hbbtv.conf
keyd reload

unclutter --timeout 1 &
unc=$!

if flatpak list --app | grep -q org.chromium.Chromium; then
  su - user -c "dbus-run-session flatpak run --file-forwarding org.chromium.Chromium --start-fullscreen --force-device-scale-factor=$zoom --alsa-output-device='$alsa' $url"
elif flatpak list --app | grep -q com.google.Chrome; then
  su - user -c "dbus-run-session flatpak run --file-forwarding com.google.Chrome --start-fullscreen --force-device-scale-factor=$zoom --alsa-output-device='$alsa' $url"
else
  /usr/bin/surf -f $url
fi

kill $unc 

rm /etc/keyd/mediathek-hbbtv.conf
keyd reload
