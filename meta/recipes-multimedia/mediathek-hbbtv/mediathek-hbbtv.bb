SUMMARY = "Mediathek HbbTV"
DESCRIPTION = "Open Mediathek HbbTV services in a browser"
PR = "r0"

LICENSE = "GPL-2.0-only"

SRC_URI += " \
	file://mediathek-hbbtv.menu \
	file://mediathek-hbbtv.sh \
	file://keyd.conf \
	"

RRECOMMENDS:${PN} += "unclutter-xfixes"

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/mediathek-hbbtv.sh ${D}${bindir}
	install -d ${D}${datadir}/menu
	install -m 0644 ${WORKDIR}/mediathek-hbbtv.menu ${D}${datadir}/menu
	install -d ${D}${sysconfdir}/mediathek-hbbtv
	install -m 0644 ${WORKDIR}/keyd.conf ${D}${sysconfdir}/mediathek-hbbtv
}

FILES:${PN} += " \
	${bindir}/ \
	${datadir}/menu/ \
	${sysconfdir}/mediathek-hbbtv/ \
	"
