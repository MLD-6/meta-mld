SUMMARY = "ffnvcodec"
HOMEPAGE = "git.videolan.org"
LICENSE = "CLOSED"

PR = "r1"

SRC_URI = "git://git.videolan.org/git/ffmpeg/nv-codec-headers.git;protocol=https;branch=master"
SRCREV = "c5e4af74850a616c42d39ed45b9b8568b71bf8bf"


S = "${WORKDIR}/git"

inherit pkgconfig

do_install() {
	oe_runmake DESTDIR=${D} install PREFIX=/usr
}
