SUMMARY     = "Channellogo Paket"
DESCRIPTION = "Download mostly Logo's for the Channelview. It will be usable by several Plugins/Skin"
HOMEPAGE    = "https://github.com/3PO/Senderlogos"
AUTHOR      = "3PO <wtaymans@redhat.com>"
SECTION     = "multimedia"

LICENSE = "GPL-2.0-only"

SRC_URI = "https://github.com/3PO/Senderlogos/archive/refs/heads/master.zip;protocol=https"
SRC_URI[sha256sum] = "2ab246e2313a9c9355e1ba4e6cf88c76f80da13bf7741abd6c1268043499cd3b"

S = "${WORKDIR}/Senderlogos-master"

inherit vdr
inherit allarch

do_install() {
	install -d ${D}${sharedir}/vdr/channellogos
	rm -rf ${S}/Alternativlogos
	rm -rf ${S}/backgrounds
	rm -rf ${S}/comedy\ cent*
	rm -rf ${S}/l-tv
	rm -rf ${S}/nick
	rm -rf ${S}/separatorlogos
	rm -rf ${S}/viva
	install -m 0755 -D ${S}/* ${D}${sharedir}/vdr/channellogos
}

FILES:${PN} = "  \
	${sharedir}/vdr/channellogos/* \
	"
