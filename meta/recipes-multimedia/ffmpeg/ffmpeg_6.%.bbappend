PR = "r3"

DEPENDS += " \
    nv-codec-headers \
    libzvbi \
    "

PACKAGECONFIG:append = " vdpau vaapi gpl x264 openssl"

PACKAGECONFIG:append = " x265"

EXTRA_OECONF += " \
    --enable-cuda \
    --enable-cuvid \
    --enable-nvdec \
    --enable-nvenc \
    --enable-nonfree \
    --enable-libzvbi \
    $PROGRAM_LIST \
    "
