SUMMARY = "RemoteTranscode - A tool for distributed video transcoding for the vdr-plugin-web"
HOMEPAGE = "https://github.com/Zabrimus/remotetranscode"
SECTION = "multimedia"

LICENSE = "CLOSED"


SRC_URI = "git://github.com/Zabrimus/remotetranscode.git;protocol=https;branch=master;name=remotetranscode"
SRCREV_remotetranscode = "247832eb424f33ba979b71496655cf288fdb3001"

SRC_URI += "git://github.com/gabime/spdlog.git;protocol=https;branch=v1.x;tag=v1.15.0;name=spdlog;destsuffix=git/subprojects/spdlog-1.15.0"
SRC_URI[spdlog.sha256sum] = "9962648c9b4f1a7bbc76fd8d9172555bad1871fdb14ff4f842ef87949682caa5"

SRC_URI += "https://wrapdb.mesonbuild.com/v2/spdlog_1.15.0-1/get_patch;name=spdlog_patch;downloadfilename=spdlog_1.15.0-1_patch.zip"
SRC_URI[spdlog_patch.sha256sum] = "72bd578ff0eb4f5a84e5bd63ddc7b2ece84aad5abb516b54c8750306ae78df28"

SRC_URI += "git://github.com/pulzed/mINI.git;protocol=https;branch=master;tag=0.9.14;name=mINI;destsuffix=git/subprojects/mINI-0.9.14"
SRC_URI[mINI.sha256sum] = "e9cdba30e2c9f43d72dc3e3460507be439b5bcb35933e06f0969c775d4ff12ab"

SRC_URI += "git://gitlab.com/eidheim/tiny-process-library.git;protocol=https;branch=master;name=tiny_process_library;destsuffix=git/subprojects/tiny-process-library-4bf0e59e64f18d3080a1ce4e853775de2181e993"
SRCREV_tiny_process_library = "4bf0e59e64f18d3080a1ce4e853775de2181e993"

SRCREV_FORMAT = "remotetranscode spdlog spdlog_patch mINI tiny_process_library"

SRC_URI += "file://remotetranscode.service"

S = "${WORKDIR}/git"

inherit meson pkgconfig systemd

SYSTEMD_SERVICE:${PN} = "remotetranscode.service"

DEPENDS = "cmake-native openssl ffmpeg"
RDEPENDS:${PN} += "ffmpeg"

do_configure:prepend() {
	cp -r ${WORKDIR}/spdlog-1.15.0 ${S}/subprojects/
	cd ${S}/subprojects/spdlog-1.15.0
	patch -p1 -i ${S}/subprojects/packagefiles/spdlog-1.15.0/3301.patch
	patch -p1 -i ${S}/subprojects/packagefiles/spdlog-1.15.0/meson-build.patch
	cp -r ${S}/subprojects/packagefiles/mini-0.9.14/* ${S}/subprojects/mINI-0.9.14
	cp -r ${S}/subprojects/packagefiles/tiny-process-library-* ${S}/subprojects/
}

do_install() {
	meson install --no-rebuild
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/build/Release/remotrans ${D}${bindir}
	install -d ${D}${datadir}/remotetranscode
	install -m 0644 -D ${WORKDIR}/build/Release/movie/* ${D}${datadir}/remotetranscode
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/remotetranscode.service ${D}${systemd_unitdir}/system
}

FILES:${PN} += " \
	${bindir} \
	${datadir}/remotetranscode \
	${systemd_unitdir}/system \
	"
