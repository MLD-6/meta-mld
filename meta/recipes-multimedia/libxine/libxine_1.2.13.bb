require libxine.inc

PPDIR = "2.13"

FILES:${PN} += " \
	${libdir}/xine/plugins/2.11/*.so \
	${libdir}/xine/plugins/2.11/*.types \
	${libdir}/xine/plugins/2.11/post/*.so \
	${datadir}/xine-lib/* \
	"

SRC_URI[md5sum] = "9e1be39857b7a3cd7cc0f2b96331ff22"
SRC_URI[sha256sum] = "5f10d6d718a4a51c17ed1b32b031d4f9b80b061e8276535b2be31e5ac4b75e6f"
