DESCRIPTION = "libxine is a versatile multimedia library decoding a lot of common audio and video formats." 
SECTION = "libs"
PRIORITY = "optional"
LICENSE = "CLOSED"
DEPENDS = " \
    ffmpeg \
    zlib \
    libogg \
    libvorbis \
    libmad \
    libmodplug \
    libpng \
    alsa-lib \
    virtual/libiconv \
    virtual/libx11 \
    libxv \
    libxext \
    directfb \
    pulseaudio \
    libtool \
"

INC_PR = "r2"

inherit autotools gettext binconfig pkgconfig

SRC_URI = "${SOURCEFORGE_MIRROR}/xine/xine-lib-${PV}.tar.xz"
SRC_URI += " \
    file://xine-binutils.2.39-fix.patch \
    "

S = "${WORKDIR}/xine-lib-${PV}"

SOV = "1.2.13"

EXTRA_OECONF = " \
    --enable-vdr \
    --disable-vaapi \
    --disable-iconvtest \
    --disable-asf \
    --disable-faad \
    --disable-aalib \
    --disable-vcd \
    --without-sdl \
    --disable-altivec \
    --disable-vis \
    --disable-mlib \
    --disable-dxr3 \
    --disable-rpath \
    --enable-fb \
    --enable-directfb \
    --with-x --x-includes=${STAGING_INCDIR}/X11 \
    --x-libraries=${STAGING_LIBDIR} \
    "

#LIBTOOL = "${HOST_SYS}-libtool"
#EXTRA_OEMAKE = "'LIBTOOL=${LIBTOOL}'"

PACKAGES_DYNAMIC = "libxine-plugin-* libxine-font-*"
