SUMMARY = "Lyrion Music Server (SqueezeServer)"
DESCRIPTION = "Lyrion Music Server (formerly Logitech Media Server, also known as SqueezeServer) is a streaming musik server for Squeezebox audio players and the SqueezePlay."
SECTION = "multimedia"
LICENSE = "CLOSED"

SRC_URI += " \
	file://docker-compose.yml \
	file://server.prefs \
	"

RDEPENDS:${PN} = "podman podman-compose"

do_install () {
	install -d ${D}${datadir}/lms
	install -m 0644 ${WORKDIR}/docker-compose.yml ${D}${datadir}/lms

	install -d ${D}${sysconfdir}/lms/prefs
	install -m 0644 ${WORKDIR}/server.prefs ${D}${sysconfdir}/lms/prefs
}

CONFFILES:${PN} += " \
  ${sysconfdir}/lms \
  "
FILES:${PN} += " \
	${datadir}/lms \
	${sysconfdir}/lms \
	"

pkg_postinst:${PN}:append () {
	if [ -z "$D" ]; then
		if [ ! id -u lms 2>/dev/null >&2 ]; then
			useradd --system --home / --no-create-home --shell /bin/false --user-group lms
		fi
		cd ${datadir}/lms
		echo "PUID=$(id -u lms)" > .env
		echo "PGID=$(id -g lms)" >> .env
		echo "IP=$(setting get system.ip[0])" >> .env
		podman compose up -d
	fi
}
