DESCRIPTION = "This daemon is used to download EPG data from the internet and manage it in a maria database."
SUMMARY = "Electronic Program Guide (EPG) Daemon"
AUTHOR = "Jörg Wendel"
HOMEPAGE = "https://github.com/horchi/vdr-epg-daemon"
SECTION = "multimedia"
BPV = "1.3.24"
PR = "r0"
SRCREV = "${AUTOREV}"
PV = "${BPV}+git${SRCPV}"
TOOLCHAIN = "clang"

LICENSE="PD"
LIC_FILES_CHKSUM = " \
	file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a \
	file://epglv/COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a \
	file://http/3rdParty/fontello/LICENSE.txt;md5=9fd60b8712e613eebcc564ee45e3bfb6 \
	file://http/3rdParty/jquery-scrollstop-master/LICENSE.md;md5=5adc401662a7161ed90970f774b76ce4 \
	file://http/www/font/LICENSE.txt;md5=9fd60b8712e613eebcc564ee45e3bfb6"

#SRC_URI = "https://projects.vdr-developer.org/git/vdr-epg-daemon.git/snapshot/vdr-epg-daemon-master.tar.gz"
SRC_URI = "git://github.com/horchi/vdr-epg-daemon.git;protocol=https;branch=master"
SRCREV = "544018b793f4ec757cb4428d4b2235181b907a22"

SRC_URI += " \
	file://epg-daemon.service \
	file://epg-daemon-http.service \
	file://channelmap.conf \
	file://epgd.conf \
    "

S = "${WORKDIR}/git"

inherit vdr systemd

DEPENDS = " \
	libarchive \
	mariadb \
	libxml2 \
	curl \
	systemd \
	libxslt \
	libzip \
	jansson \
	jpeg \
	openssl \
	imlib2 \
	python3-native \
	libmicrohttpd \
	jansson \
	ldconfig-native \
	glibc \
	clang \
	"

RDEPENDS_${PN} = "vdr"

EXTRA_OEMAKE = " \
	MYSQL=0 \
	DEBUG=0 \
	SYSD_NOTIFY=1 \
	SYSDLIB_210=1 \
	USE_CLANG=1 \
	PYTHON=python3 \
	"

#do_compile() {
#	oe_runmake 
#}

do_install () {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/epgd
	install -m 0644 ${WORKDIR}/channelmap.conf ${D}${sysconfdir}/epgd/
	install -m 0644 ${WORKDIR}/epgd.conf ${D}${sysconfdir}/epgd/
    
	install -d ${D}${systemd_system_unitdir}
	install -m 0644 ${WORKDIR}/epg-daemon.service ${D}${systemd_system_unitdir}
	install -m 0644 ${WORKDIR}/epg-daemon-http.service ${D}${systemd_system_unitdir}
	install -d ${D}${bindir}
	install -m 0755 ${S}/epg-daemon ${D}${bindir}
	install -m 0755 epglv/mysqlepglv.so "$pkgdir/$(mysql_config --plugindir)/mysqlepglv.so"
}

CONFFILES:${PN} += " \
	${sysconfdir}/epgd/* \
	"

BACKUPFILES:${PN} += " \
	${sysconfdir}/epgd/* \
	"

FILES:${PN} += "${systemd_system_unitdir}/epg-daemon.service \
	${bindir}/epgd \
	${sysconfdir}/epgd/* \
	${bindir}/* \
	"
