DESCRIPTION = "The Zapping VBI library, in short ZVBI, provides functions to \
capture and decode VBI data. It is written in plain ANSI C with few dependencies \
on other tools and libraries."
HOMEPAGE = "http://zapping.sourceforge.net/ZVBI/index.html"
SECTION = "libs/multimedia"
PR = "r1"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"


SRC_URI = "git://github.com/zapping-vbi/zvbi.git;protocol=https;branch=main"
SRCREV = "a48ab3a0d72efe9968ebafa34c425c892e4afa50"

S = "${WORKDIR}/git"

inherit autotools pkgconfig 

DEPENDS = "libpng"

EXTRA_OECONF = "--without-x"

do_install() {
    oe_runmake DESTDIR=${D} install
}


