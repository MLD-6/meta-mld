DESCRIPTION = "YouTube downloader tool yt-dlp"
SUMMARY = "YouTube downloader"
SECTION = "multimedia"
BPV = "2025.01.15"
PR = "r0"
SRCREV = "${AUTOREV}"
PV = "${BPV}+git${SRCPV}"

LICENSE="PD"
LIC_FILES_CHKSUM = "file://LICENSE"

SRC_URI = "git://github.com/yt-dlp/yt-dlp.git;protocol=https;branch=master"

DEPENDS = "python3 zip-native"
RDEPENDS:${PN} = "python3"
RREPLACES:${PN} += "youtube-dl"
RCONFLICTS:${PN} += "youtube-dl"
RPROVIDES:${PN} += "youtube-dl"

S = "${WORKDIR}/git"

inherit pkgconfig

FILES:${PN} += " \
	${bindir}/* \
	"

EXTRA_OEMAKE += "PYTHON='/usr/bin/env python3'"

do_compile() {
	oe_runmake yt-dlp
}

do_install () {
	install -d ${D}${bindir}
	install -m 0755 ${S}/yt-dlp ${D}${bindir}
	ln -s yt-dlp ${D}${bindir}/youtube-dl
}
