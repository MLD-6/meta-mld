SUMMARY = "SqueezePlay is a player for the Lyrion Music Server."
PV = "9.0.0"
PR = "r2"

LICENSE = "Logitech-Public-Source-License"
NO_GENERIC_LICENSE[Logitech-Public-Source-License] = "squeezeplay/LICENSE"
LIC_FILES_CHKSUM = "file://squeezeplay/LICENSE;md5=4ee23c52855c222cba72583d301d2338"

SRC_URI = "git://github.com/ralph-irving/squeezeplay.git;protocol=http;branch=master;name=squeezeplay"
SRCREV_squeezeplay = "f3e3441d5d6e29532fb5b99612c8f3a17c7497ec"
SRC_URI[squeezeplay.sha256sum] = "725435f7637f6e7db2b319a79c8fc9ad342cacb0bfc9a18906978db52fa5c0f3"
SRC_URI += "git://github.com/pssc/Squeezeplay-800x480Skin.git;protocol=http;branch=master;name=800x480Skin;destsuffix=git/src/squeezeplay/share/applets/800x480Skin"
SRCREV_800x480Skin = "08b780d7ac18d09c0a2850dc4d76cda9cb2cf203"
SRC_URI[800x480Skin.sha256sum] = "a17e4557c2eec02423bf912e8c2bd3647d68838a823c1b9b050655fd04ee4822"
SRCREV_FORMAT = "squeezeplay 800x480Skin"

SRC_URI += " \
	file://09_configure.in.patch \
	file://08_pkconfig.patch \
	file://10_makefile.patch \
	file://14_joggler.patch \
	file://15_poweroff.patch \
	file://16_standby.patch \
	file://18_wallpaper.patch \
	file://19_keycodes.patch \
	file://21_restart.patch \
	file://22_800x480.patch \
	file://23_apps_starter.patch \
	file://26_tremor.patch \
	file://28_volume.patch \
	file://29_applets.patch \
	file://30_common.patch \
	file://31_framework.patch \
	file://32_decode.patch \
	file://33_luamd5.patch \
	"
# SRC_URI += " \
# 	file://13_decode.patch \
# 	file://25_thumb.patch \
# 	"

SRC_URI += " \
	file://squeezeplay \
	file://squeezeplay.menu \
	file://squeezeplay.conf \
	"

S = "${WORKDIR}/git/src"

python __anonymous() {
  d.appendVarFlag('do_compile', 'network', '1') 
}

DEPENDS += "zlib libpng jpeg expat libsdl-ttf libsdl-image libsdl-gfx lame libogg libmad fdk-aac flac alsa-lib tremor libpthread-stubs libxrandr readline imagemagick-native"

RDEPENDS:${PN} += "zlib libpng jpeg expat libsdl-ttf libsdl-image libsdl-gfx freetype lame libogg fdk-aac flac alsa-lib libxrandr"
RRECOMMENDS:${PN} += "unclutter-xfixes net-tools"
RSUGGESTS:${PN} += "lms"

INSANE_SKIP:${PN} += "rpaths dev-so already-stripped"

inherit autotools pkgconfig

do_configure() {
	cd ${S}/squeezeplay
	autoconf -i -f
}

TARGET_CC_ARCH += "${LDFLAGS}"

do_compile() {
	if [ ! -e ${S}/.scaled ]; then
		for size in 720x576 1280x720 1920x1080 3840x2160; do
			ls ${S}/squeezeplay_desktop/share/applets/SetupWallpaper/wallpaper/pcp_* | while read f; do convert.im7 -interlace none $f -resize $size\! $(echo $f | sed "s/pcp_/jl${size}_/"); done
		done
	fi
	export LIBPATH="${STAGING_DIR_TARGET}/usr/lib"
	# oe_runmake -C ${S} -j1 -f Makefile.linux$(test ${TARGET_ARCH} = arm && echo arm) HOST=${HOST_SYS} #i686-mld-linux
	oe_runmake -C ${S} -j1 -f Makefile.linux HOST=${HOST_SYS} #i686-mld-linux
}

do_install() {
	install -d ${D}${libdir}/squeezeplay
	cp -r ${WORKDIR}/build/linux/* ${D}${libdir}/squeezeplay

	sed "s|^INSTALL_DIR=.*|INSTALL_DIR=${libdir}/squeezeplay|" -i ${D}${libdir}/squeezeplay/bin/squeezeplay.sh
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/squeezeplay ${D}${bindir}

	install -d ${D}${datadir}/menu
	install -m 0644 ${WORKDIR}/squeezeplay.menu ${D}${datadir}/menu

	install -d ${D}${sysconfdir}/keyd/apps.d
	install -m 0644 ${WORKDIR}/squeezeplay.conf ${D}${sysconfdir}/keyd/apps.d
}

FILES:${PN} += " \
	${datadir}/menu \
	/var/lib/squeezeplay \
	"
FILES:${PN}-dev += " \
	${libdir}/squeezeplay/include \
	${libdir}/squeezeplay/lib/pkgconfig \
	"
FILES:${PN}-staticdev += " \
	${libdir}/squeezeplay/lib/*.a \
	"
FILES:${PN}-doc += " \
	${libdir}/squeezeplay/man \
	${libdir}/squeezeplay/share/man \
	"
