SUMMARY = "Sundtek dvb driver"
DESCRIPTION = "Userspace driver for the Sundtek dvb device."
LICENSE = "Proprietary"
PV = "240822.144415"

SRC_URI = "https://www.sundtek.de/media/sundtek_installer_${PV}.sh"
SRC_URI[sha256sum] = "85b2237331be1b26253659019f400c096a8ade1c6bb0a801edba8f4e6ecac820"

SRC_URI += " \
	file://sundtek.service \
	"

S = "${WORKDIR}/build"

inherit systemd

SYSTEMD_SERVICE:${PN} = "sundtek.service"

unpack(){
	rm -rf ${WORKDIR}/src
	mkdir ${WORKDIR}/src
	cd ${WORKDIR}/src
	sh ${WORKDIR}/sundtek_installer_${PV}.sh -e
}

do_unpack:append() {
    bb.build.exec_func('unpack', d)
}

do_compile () {
	rm -r ${S}
	mkdir -p ${S}
	test "${TUNE_ARCH}" = "arm" && arch="armsysvhf"
	test "${TUNE_ARCH}" = "x86_64" && arch="64bit"
	test "${TUNE_ARCH}" = "aarch64" && arch="arm64"
	tar -xzf ${WORKDIR}/src/${arch}/installer.tar.gz -C ${S}
	chmod 744 ${S}/opt/bin/mediasrv
}

do_install () {
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/sundtek.service ${D}${systemd_unitdir}/system/

	install -d ${D}${bindir}
	install -m 0755 ${S}/opt/bin/mediaclient ${D}${bindir}/
	install -m 0755 ${S}/opt/bin/mediasrv ${D}${bindir}/

	install -d ${D}${libdir}
	install -m 0644 -D ${S}/opt/lib/lib* ${D}${libdir}/
	install -d ${D}${libdir}/sundtek/audio
	if [ -e ${S}/opt/bin/audio/libalsa.so ]; then
		install -m 0644 -D ${S}/opt/bin/audio/* ${D}${libdir}/sundtek/audio/
	fi

	install -d ${D}${sysconfdir}/udev/rules.d
	install -m 0644 ${S}/etc/udev/rules.d/80-mediasrv.rules ${D}${sysconfdir}/udev/rules.d/
	sed "s#/opt/#/usr/#" -i ${D}${sysconfdir}/udev/rules.d/80-mediasrv.rules
}

install_x86(){
	install -d ${D}${libdir}/sundtek/dvb
	install -m 0644 -D ${S}/opt/bin/dvb/* ${D}${libdir}/sundtek/dvb/
	
	install -d ${D}${libdir}/sundtek/plugins
	install -m 0644 -D ${S}/opt/bin/plugins/* ${D}${libdir}/sundtek/plugins/
}

do_install:append:genericx86-64 () {
	install_x86
}
do_install:append:qemux86-64 () {
	install_x86
}

INSANE_SKIP:${PN} += "already-stripped"
INSANE_SKIP:${PN} += "ldflags"

FILES_SOLIBSDEV = ""

FILES:${PN} += "  \
	${systemd_unitdir}/* \
	${bindir}/* \
	${libdir}/* \
	${sysconfdir}/* \
	"

pkg_postinst:${PN}() {
#!/bin/sh
dpkg --print-architecture | grep -q amd64 && ln -fs $D/lib $D/lib64
echo $D${libdir}/libmediaclient.so > $D${sysconfdir}/ld.so.preload
}

pkg_prerm:${PN}() {
#!/bin/sh
sed "/.*\/libmediaclient.so/d" -i $D${sysconfdir}/ld.so.preload
}
