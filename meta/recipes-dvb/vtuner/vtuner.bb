SUMMARY = "vtuner"
PV = "1.0.0"
PR = "r1"

LICENSE = "CLOSED"

SRC_URI = "git://github.com/yavdr/vtuner.git;protocol=https;branch=master"
SRCREV = "60037a299c346e54a4e5f7d94ab680ee271ec7c4"

S = "${WORKDIR}/git"

TARGET_CC_ARCH += "${LDFLAGS}"

do_install() {
	oe_runmake DESTDIR=${D} install
    rm -f -r ${D}/usr/src/vtunerc-0.0.0/
}
