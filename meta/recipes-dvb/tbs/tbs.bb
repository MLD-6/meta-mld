SUMMARY = "DVB-TBS Kernel Modules"
DESCRIPTION = "Kernel modules for DVB-TBS devices."
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=12f884d2ae1ff87c09e5b7ccc2c4ca7e"

SRC_URI = " \
  git://github.com/tbsdtv/media_build.git;protocol=https;branch=latest;name=mb \
  git://github.com/tbsdtv/linux_media.git;protocol=https;branch=latest;subdir=media;name=lm \
  "
SRCREV_mb = "0f49c76b80838ded04bd64c56af9e1f9b8ac1965"
SRCREV_lm = "a2e856bfb243942a9e9aaae004353501f513a5d7"
SRCREV_FORMAT = "${SRCNAME}"

SRC_URI += " \
	file://05_makefile.patch \
  "

S = "${WORKDIR}/git"

inherit module

DEPENDS += "patchutils-native linux-libc-headers"

EXTRA_OEMAKE += "SRCDIR=${STAGING_KERNEL_DIR} OUTDIR=${STAGING_KERNEL_BUILDDIR} VER=${KERNEL_VERSION}"

do_configure() {
  oe_runmake dir DIR=../media
  oe_runmake allyesconfig
  sed -r 's/(^CONFIG.*_RC.*=)./\1n/g' -i "${S}/v4l/.config"
  sed -r 's/(^CONFIG.*_IR.*=)./\1n/g' -i "${S}/v4l/.config"
  sed -r 's/(^CONFIG_VIDEO_MUX=)./\1n/g' -i "${S}/v4l/.config"
}

do_install () {
	oe_runmake install DESTDIR=${D}

  install -d ${D}${sysconfdir}/depmod.d
  echo "search extra updates kernel build-in" > ${D}${sysconfdir}/depmod.d/tbs.conf
}

FILES:${PN} += "  \
	${sysconfdir} \
  ${libdir} \
	"
