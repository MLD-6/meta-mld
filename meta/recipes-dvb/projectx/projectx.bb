SUMMARY = "ProjectX - a free Java based demux utility"
HOMEPAGE = "https://github.com/knowledgejunkie/Project-X/tree/dev"
AUTHOR = "dvbmatt"
SECTION = "multimedia"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/knowledgejunkie/Project-X.git;protocol=https;branch=master \
           "
PV = "0.91.0"

SRCREV = "591e88b198a6bce17d01050f80d1118bb3096b4b"

S = "${WORKDIR}/git"

inherit java-library

RDEPENDS:libprojectx-java += "openjre-8"
RPROVIDES:libprojectx-java += "projectx"
#PREFERRED_PROVIDER_virtual/java-native = "cacao-native"
#PREFERRED_PROVIDER_virtual/java-initial-native = "cacao-initial-native"

do_configure[noexec] = "1"

# Keep the java binraries contained in the Project-X source
do_deletebinaries[noexec] = "1"

do_compile () {
	bash ./build.sh
	cp ProjectX.jar projectx-${PV}.jar
}

do_install () {
	install -d ${D}${datadir_java}/lib
	install -m 644 ${S}/lib/commons-net-1.3.0.jar ${D}${datadir_java}/lib/
	install -m 644 ${S}/lib/jakarta-oro-2.0.8.jar ${D}${datadir_java}/lib/
}
