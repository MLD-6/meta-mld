SUMMARY = "OSCam: Open Source Conditional Access Module"
HOMEPAGE = "http://www.streamboard.tv/oscam/"
PV = "1.20+${SRCPV}"

PACKAGE_ARCH = "${TUNE_PKGARCH}_extra"

LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

SRC_URI += "git://repo.or.cz/oscam.git;protocol=https;branch=master"
SRCREV = "53196cb0bb2af43b3412032c4dc13ef5ab9eecde"
#SRCREV = "d4b2b405a881eecaf8b45e85ccc33f02244c2f13"
SRC_URI[sha256sum] = "30f8fb53840e7f7efb4a25fb0e7dbb8842d332722c2a7050a3bac65e320f4404"

SRC_URI += " \
	file://oscam.service \
	file://oscam.conf \
	file://oscam.user \
	"

S = "${WORKDIR}/git"

inherit cmake systemd

DEPENDS = "libusb1 openssl pcsc-lite"

EXTRA_OECMAKE = "-DDEFAULT_CS_CONFDIR=${sysconfdir} -DCMAKE_BUILD_TYPE=Debug"

do_configure:append() {
    sed -i -e '1 s|${TOPDIR}|<TOPDIR>|g' ${B}/config.c
}

do_install () {
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/oscam.service ${D}${systemd_unitdir}/system/

	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/build/oscam ${D}${bindir}/oscam

	install -d ${D}${sysconfdir}/oscam/example
	install -m 0644 -D ${S}/Distribution/doc/example/* ${D}${sysconfdir}/oscam/example/
	install -m 0644 ${WORKDIR}/oscam.conf ${D}${sysconfdir}/oscam/
	install -m 0644 ${WORKDIR}/oscam.user ${D}${sysconfdir}/oscam/

	install -d ${D}${mandir}/man1 ${D}${mandir}/man5
	install -m 0644 -D ${S}/Distribution/doc/man/*.1 ${D}${mandir}/man1/
	install -m 0644 -D ${S}/Distribution/doc/man/*.5 ${D}${mandir}/man5/
}

SYSTEMD_SERVICE:${PN} = "oscam.service" 

CONFFILES:${PN} += " \
    ${sysconfdir}/oscam/* \
    "

BACKUPFILES:${PN} += " \
                ${sysconfdir}/oscam/* \
    "

FILES:${PN} += "  \
	${systemd_unitdir}/system/oscam.service  \ 
	${bindir}/oscam \
	${sysconfdir}/oscam/* \
	"
