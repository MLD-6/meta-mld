SUMMARY = "Utility to scan for channels on DVB-S/S2 DVB-C DVB-T/T2"
AUTHOR = "wirbel"
HOMEPAGE = "https://www.gen2vdr.de/wirbel/"
SECTION = "multimedia"
PROVIDES += "w_scan"
PV = "20231015"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "https://www.gen2vdr.de/wirbel/w_scan_cpp/w_scan_cpp-${PV}.tar.bz2"
SRC_URI[sha256sum] = "dfe4a6ebff0912d52de3892d952f4e920d9250e326aec1073d47da12aaab5206"

SRC_URI += " \
	file://makefile.patch \
	"

S = "${WORKDIR}/w_scan_cpp-${PV}"

DEPENDS += "librepfunc jpeg pugixml freetype fontconfig libcap curl"

inherit autotools pkgconfig

python __anonymous() {
  d.appendVarFlag('do_compile', 'network', '1') 
}

do_compile() {
  if [ ! -d "${S}/vdr" ]; then
    make -C ${S} download
  fi
  oe_runmake -C ${S}
}

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${S}/w_scan_cpp ${D}${bindir}/w_scan
}
