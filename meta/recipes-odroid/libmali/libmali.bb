LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://END_USER_LICENCE_AGREEMENT.txt;md5=3918cc9836ad038c5a090a0280233eea"

SRC_URI = "git://github.com/LibreELEC/libmali.git;protocol=https;branch=master"
SRC_URI += " \
           file://libmali-setup \
           file://libmali-setup.service \
           "

# Modify these as desired
PV = "1.0+git"
SRCREV = "d4000def121b818ae0f583d8372d57643f723fdc"

S = "${WORKDIR}/git"

inherit cmake

DEPENDS += " libdrm"

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = " -DMALI_ARCH=aarch64-linux-gnu"

