DESCRIPTION = "MLD webif to set up the system."
SUMMARY = "MLD webif"
AUTHOR = "Claus Muus"
HOMEPAGE = "http://www.minidvblinux.de"
SECTION = "base/utils"
BPV = "6.0.0"
PR = "r3"
SRCREV = "${AUTOREV}"
PV = "${BPV}+git${SRCPV}"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://LICENSE;md5=4ee23c52855c222cba72583d301d2338"
# get list of all Licenses used by node_modules: "devtool add --npm-dev git://gitlab.com/MLD-6/webif.git"

SRC_URI = "git://gitlab.com/MLD-6/webif.git;protocol=https;branch=settings"
#SRC_URI[sha256sum] = "cf09dfcd238ecfaa9e36047dbf107d635b052836"

S = "${WORKDIR}/git"

inherit npm systemd

DEPENDS = "nodejs"
RDEPENDS:${PN} = "nodejs"
RRECOMMENDS:${PN} ="parted lshw remoteconnection supportlog"
PREFERRED_VERSION_nodejs = "20.%"

#SYSTEMD_SERVICE:${PN} = "webif.socket webif.service"

sysroot = "${WORKDIR}/recipe-sysroot-native"

python __anonymous() {
  d.appendVarFlag('do_compile', 'network', '1') 
}

do_compile () {
	export OPENSSL_CONF=${sysroot}/usr/lib/ssl-3/openssl.cnf SSL_CERT_DIR=${sysroot}/usr/lib/ssl-3/certs SSL_CERT_FILE=${sysroot}/usr/lib/ssl-3/cert.pem OPENSSL_ENGINES=${sysroot}/usr/lib/engines-3 OPENSSL_MODULES=${sysroot}/usr/lib/ossl-modules
	export NODE_OPTIONS=--openssl-legacy-provider
	npm install
	node_modules/.bin/quasar build
	
	mkdir -p production
	cp package.json production
	cd production
	npm install --production
}

do_install () {
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${S}/webif.socket ${D}${systemd_unitdir}/system
	install -m 0644 ${S}/webif.service ${D}${systemd_unitdir}/system
	install -d ${D}${systemd_unitdir}/system-preset
	echo "enable webif.socket" > ${D}${systemd_unitdir}/system-preset/98-webif.preset
	install -d ${D}${datadir}/menu
	install -m 0644 ${S}/webif.menu ${D}${datadir}/menu
	sed 's#\(x-\)\?www-browser#surf#' -i ${D}${datadir}/menu/webif.menu
	install -d ${D}${datadir}/webif
	install -m 0644 ${S}/app.js ${D}${datadir}/webif
	install -d ${D}${datadir}/webif/bin
	install -m 0755 -D ${S}/bin/* ${D}${datadir}/webif/bin
	cp -r ${S}/modules ${D}${datadir}/webif
	cp -r ${S}/www ${D}${datadir}/webif
	cp -r ${S}/production/node_modules ${D}${datadir}/webif
	install -d ${D}${bindir}
	ln -s ${datadir}/webif/bin/setting ${D}${bindir}
	ln -s ${datadir}/webif/bin/webif ${D}${bindir}
	install -d ${D}${sysconfdir}
	echo "{}" > ${D}${sysconfdir}/settings.json
}

CONFFILES:${PN} += " \
  ${sysconfdir}/* \
  "
FILES:${PN} += " \
	${bindir}/* \
	${datadir}/menu/* \
	${datadir}/webif/* \
	${systemd_unitdir}/system/* \
	${systemd_unitdir}/system-preset/* \
  ${sysconfdir}/* \
	"

# this postinstall script do not restart the webif service after upgrade
pkg_postinst:${PN}() {
#!/bin/sh
if systemctl >/dev/null 2>/dev/null; then
	if [ "$D" ]; then
		systemctl --root=$D enable webif.socket
	else
		systemctl daemon-reload
		systemctl preset webif.socket webif.service
	fi
fi
}

pkg_prerm:${PN}() {
#!/bin/sh
if systemctl >/dev/null 2>/dev/null; then
	if [ -z "$D" ]; then
		systemctl stop webif.socket webif.service
		systemctl disable webif.socket webif.service
	fi
fi
}
