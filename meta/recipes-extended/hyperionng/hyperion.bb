SUMMARY = "Hyperion"
PV = "git"
PR = "r1"

LICENSE = "CLOSED"

SRC_URI = "git://github.com/hyperion-project/hyperion.ng.git;protocol=https;branch=master"
SRCREV = "fd5a94a32adc49418fb9b11db8b1160b0a2dcda5"

SRC_URI += " \
	file://hyperion.service \
	"

S = "${WORKDIR}/git"

inherit python3native qt6-cmake systemd

TOOLCHAIN_HOST_TASK += "\
	nativesdk-packagegroup-qt6-toolchain-host \
	"

DEPENDS += " \
	python3 \
	qtbase  \
	qtwebsockets \
	qtserialport \
	libcec \
	libjpeg-turbo \
	openssl \
	libusb \
	flatbuffers \
	flatbuffers-native \
	xrandr \
	libxrender \
	libxcb \
	mbedtls \
	mbedtls-native \	
	protobuf \
	protobuf-native \
	libdrm \
	qtbase \
	"

EXTRA_OECMAKE += " \
	-DCMAKE_BUILD_TYPE=Release \
	-DENABLE_REMOTE_CTL=ON \
	-DENABLE_QT=ON \
	-DENABLE_AUDIO=OFF \
	-DQT_DEBUG_FIND_PACKAGE=ON \
	-DENABLE_CEC=OFF \
	-DENABLE_DEV_NETWORK=OM \
	-DENABLE_DEV_SERIAL=ON \
	-DENABLE_DEV_SPI=ON \
	-DENABLE_DEV_USB_HID=ON \
	-DENABLE_MDNS=OFF \
	-DENABLE_FLATBUF_SERVER=ON \
	-DENABLE_FORWARDER=ON \
	-DENABLE_FLATBUF_CONNECT=ON \
	-DENABLE_PROTOBUF_SERVER=ON \
	-DENABLE_DEV_WS281XPWM=OFF \
	-DENABLE_DEPLOY_DEPENDENCIES=OFF \
	-DCMAKE_POSITION_INDEPENDENT_CODE=ON \
	-DENABLE_DISPMANX=ON \
	-DUSE_SYSTEM_FLATBUFFERS_LIBS=ON \
	-DUSE_SYSTEM_MBEDTLS_LIBS=ON \
	-DUSE_SYSTEM_PROTO_LIBS=ON \
	-DENABLE_V4L2=ON \
	"

export INCLUDES += " \
	-I${STAGING_DIR_HOST}/usr/include \
	"

EXTRA_OECMAKE +=" \
	-DENABLE_X11=ON \
	-DX11=ON \
	-DPLATFORM=x11 \
	"

EXTRA_OECMAKE +=" \
	-DPLATFORM=rpi \
	"

RDEPENDS:${PN} += " \
	python3 \
	qtbase  \
	qtserialport \
	libcec \
	libjpeg-turbo \
	openssl \
	"


do_install() {

	install -d ${D}${sysconfdir}/hyperion
	install -m 0644 ${WORKDIR}/build/config/* ${D}${sysconfdir}/hyperion

	install -d ${D}${sysconfdir}/hyperion/effects
	install -m 0644 ${S}/effects/*.* ${D}${sysconfdir}/hyperion/effects
	install -d ${D}${sysconfdir}/hyperion/effects/schema
	install -m 0644 ${S}/effects/schema/* ${D}${sysconfdir}/hyperion/effects/schema

	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/hyperion.service ${D}${systemd_unitdir}/system

	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/build/bin/hyperiond ${D}${bindir}/hyperiond

	install -d ${D}${libdir}
	install -m 0755 ${WORKDIR}/build/lib/*.* ${D}${libdir}


	install -m 0755 ${WORKDIR}/build/bin/hyperion-framebuffer ${D}${bindir}/hyperion-framebuffer
	install -m 0755 ${WORKDIR}/build/bin/hyperion-dispmanx ${D}${bindir}/hyperion-dispmanx
	install -m 0755 ${WORKDIR}/build/bin/hyperion-qt ${D}${bindir}/hyperion-qt
	install -m 0755 ${WORKDIR}/build/bin/hyperion-v4l2 ${D}${bindir}/hyperion-v4l2
	install -m 0755 ${WORKDIR}/build/bin/hyperion-x11 ${D}${bindir}/hyperion-x11
	install -m 0755 ${WORKDIR}/build/bin/hyperion-remote ${D}${bindir}/hyperion-remote
	#install -m 0755 ${WORKDIR}/build/bin/hyperion-xcb ${D}${bindir}/hyperion-xcb
	
}

SYSTEMD_SERVICE:${PN} = "hyperion.service" 

FILES:${PN} += "  \
	${systemd_unitdir}/system/hyperion.service  \
	${bindir}/hyperiond \ 
	"
