DESCRIPTION = "HTML5 VNC client"
HOMEPAGE = "https://github.com/kanaka/noVNC"
SECTION = "web"
LICENSE = "MPL-2.0"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=d6767b40116d20878aac399694c49de0"

SRCREV = "a0e6e7b1d82b105889c9c092692218a786b55f27"
PV = "1.2.0+git${SRCPV}"

SRC_URI = "git://github.com/kanaka/noVNC.git;branch=master;protocol=https"

S = "${WORKDIR}/git"

do_compile() {
    :
}

RDEPENDS:${PN} += " \
	bash \
"

do_install() {
    install -m 755 -d ${D}${datadir}/novnc
   
    cp -R --no-dereference --preserve=mode,links ${S}/* ${D}${datadir}/novnc
 
    install -m 444 ${S}/vnc.html ${D}${datadir}/novnc/index.html
    install -m 444 ${S}/vnc_lite.html ${D}${datadir}/novnc/vnc_auto.html

    install -m 755 -d ${D}${bindir}
    install ${S}/utils/novnc_proxy ${D}${bindir}/novnc_server
}
