PR = "r1"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
	file://lcdd.service \
	file://lcdproc.service \
	"

inherit systemd

SYSTEMD_PACKAGES += "lcdd"
SYSTEMD_SERVICE:${PN} = "lcdproc.service"
SYSTEMD_SERVICE:lcdd = "lcdd.service"

do_install:append() {
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/lcdd.service ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/lcdproc.service ${D}${systemd_unitdir}/system
}

FILES:lcdd += " \
  ${systemd_unitdir}/system/lcdd.service \
  "
