SUMMARY = "Tntnet is a web application server for web applications written in C++."
PV = "git"
PR = "r2"

LICENSE = "CLOSED"

SRC_URI = "git://github.com/maekitalo/cxxtools.git;protocol=https;branch=master"
SRCREV = "43ba2ec90fcb082cb3066d26a6f7e4a15a1abbb1"

S = "${WORKDIR}/git"

inherit autotools pkgconfig

DEPENDS += " \
    openssl \
    "

do_compile() {
    oe_runmake
}

do_install() {
    oe_runmake install DESTDIR=${D}
}

FILES:${PN} = " \
    ${bindir}/* \
    ${libdir}/* \
    "
BBCLASSEXTEND = "native nativesdk"
