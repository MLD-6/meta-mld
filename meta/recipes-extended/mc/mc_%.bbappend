FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://mc"

FILES:${PN} += " \
	${prefix}/local/bin/* \
	"

do_install:append () {
	install -d ${D}${prefix}/local/bin
	install -m 0755 ${WORKDIR}/mc ${D}${prefix}/local/bin
}
