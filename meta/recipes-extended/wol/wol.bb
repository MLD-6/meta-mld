SUMMARY = "Enable wake on lan (WOL)"
AUTHOR = "Claus Muus"
HOMEPAGE = "http://www.minidvblinux.de"
SECTION = "base/utils"
PR = "r0"

LICENSE = "GPL-2.0-only"

SRC_URI += " \
  file://wol.service \
  "

inherit systemd

RDEPENDS:${PN} = "ethtool"

SYSTEMD_SERVICE:${PN} = "wol.service" 

do_install:append() {
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/wol.service ${D}${systemd_unitdir}/system/
}

FILES:${PN} += "  \
	${systemd_unitdir}/system/wol.service  \ 
	"
