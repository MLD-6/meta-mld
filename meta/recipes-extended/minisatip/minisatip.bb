SUMMARY = "A DVB to SAT>IP server"
MAINTAINER = "catalinii"
HOMEPAGE = "https://minisatip.org/"
SRCREV = "${AUTOREV}"
UPSTREAMVERSION = "1.0d"
PV = "${UPSTREAMVERSION}+git${SRCPV}"
PR = "r1"

LICENSE = "CLOSED"

SRC_URI = " \
	git://github.com/catalinii/minisatip.git;protocol=https;branch=master\
	file://minisatip.service \
	"

S = "${WORKDIR}/git"
BUILD = "${WORKDIR}/git"

inherit autotools-brokensep systemd

SYSTEMD_SERVICE:${PN} = "minisatip.service" 

DEPENDS = "libdvbcsa openssl"
RDEPENDS:${PN} = "libdvbcsa openssl"

EXTRA_OECONF += " \
	NETCVCLIENT=1 \
	DVBAPI=1 \
	"

do_configure:prepend () {
}

do_install () {
	install -d -m 0755 ${D}/${bindir}
	install -m 0755 ${S}/minisatip ${D}/${bindir}/
	install -d -m 0755 ${D}/${datadir}/${PN}/html
	install -m 0644 -D ${S}/html/* ${D}/${datadir}/${PN}/html

	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/minisatip.service ${D}${systemd_unitdir}/system
}