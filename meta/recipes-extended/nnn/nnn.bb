SUMMARY = "nnn - The missing terminal file manager for X."
DESCRIPTION = "nnn is a full-featured terminal file manager. It's tiny and nearly 0-config with an incredible performance."
HOMEPAGE = "https://github.com/jarun/nnn"
LICENSE = "BSD-2-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=593eb99b50ecdfeb2fc51a1a0e60c87a"

SRC_URI = "git://github.com/jarun/nnn.git;protocol=https;branch=master"
SRCREV = "14fef0323dffefac8d6358f5991fdb63a4355915"

S = "${WORKDIR}/git"

inherit pkgconfig 

DEPENDS += " \
    readline \
    ncurses \
    "
RDEPENDS_${PN} = "bash coreutils"

do_install() {
    #oe_runmake DESTDIR=${D} install

    install -d ${D}${bindir}
    install -m 0755 nnn ${D}${bindir}/
}

FILES:${PN} = " \
    ${bindir}/* \
    ${libdir}/* \
    "

