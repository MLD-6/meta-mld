pkg_postinst:${PN}:append() {
  if [ -z "$D" ]; then
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

    setting set "apps.commands.Chromium" "su - user -c 'dbus-run-session flatpak run --file-forwarding org.chromium.Chromium'"
    setting set "apps.commands.Google_Chrome" "su - user -c 'dbus-run-session flatpak run --file-forwarding com.google.Chrome'"
    setting set "apps.commands.Ungoogled_Chromium" "su - user -c 'dbus-run-session flatpak run --file-forwarding io.github.ungoogled_software.ungoogled_chromium'"
  fi
}
