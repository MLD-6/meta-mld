# clean up list of manual installed packages

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
PR = "r1"

SRC_URI += " \
	file://apt-mark-manual-cleanup.service \
	file://apt-mark-manual-cleanup.sh \
	file://apt-config-backup.sh \
	file://invoke \
	file://downgrade \
	"

do_install:append () {
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/apt-mark-manual-cleanup.service ${D}${systemd_unitdir}/system
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/apt-mark-manual-cleanup.sh ${D}${bindir}
	install -m 0755 ${WORKDIR}/apt-config-backup.sh ${D}${bindir}
	install -d ${D}${sysconfdir}/apt/apt.conf.d
	install -m 0644 ${WORKDIR}/invoke ${D}${sysconfdir}/apt/apt.conf.d
	install -m 0644 ${WORKDIR}/downgrade ${D}${sysconfdir}/apt/apt.conf.d
}

FILES:${PN} += " \
	${systemd_unitdir}/system/* \
	${bindir}/* \
	${sysconfdir}/apt/apt.conf.d/* \
	"

pkg_postinst:${PN}:append() {
#!/bin/sh
if [ -n "$D" ]; then
	systemctl --root="$D" enable apt-mark-manual-cleanup.service
fi
}
