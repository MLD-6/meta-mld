#!/bin/sh
# clean up list of manual installed packages

depends="$(dpkg-query -W -f='${Depends}, ${Recommends}\n' | sed "s/ ([^)]*)//g; s/, /\n/g" | sort -u)"
provids="$(dpkg-query -W -f='${Package} ${Provides}\n' | while read package provides; do test "$provides" && echo "$provides" | sed 's/, /\n/g' | while read provide; do echo "$depends" | grep -q $provide && echo $package; done; done | sort -u)"

apt-mark auto $depends $provids >/dev/null