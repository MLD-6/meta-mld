#!/bin/sh

tmp="/tmp/apt-config-backup"
action="$1"
file="$2"

case "$action" in
  create)
    rm -rf $tmp

    cat /var/lib/dpkg/info/*files | awk '{if($1!="") print $1;}' | while read line; do ls $line 2>/dev/null | while read config; do
      mkdir -p $tmp${config%/*}
      ln -s $config $tmp$config
    done; done

    # die verlinkte conf auf die /etc/vdr/channels.conf zeigt mitsichern
    if [ -e "$tmp/etc/vdr/channels.conf" ]; then
      mkdir -p $tmp/etc/vdr/channels
      ln -s /etc/vdr/$(readlink /etc/vdr/channels.conf) $tmp/etc/vdr/channels/
    fi

    mkdir -p $tmp/tmp
    ln -s /etc/os-release $tmp/tmp/
    apt list --installed 2>/dev/null | grep -v ^Listing... | grep -v ^lib | grep -v automatic | sed "s|/.*||" | sort -u > $tmp/tmp/packages.list

    tar -czhf $file -C $tmp .

    rm -rf $tmp
    ;;
  restore)
    tar -xzf $file -C / || exit 1
    apt-get install -q -y -o=Dpkg::Options::=--force-confdef -o=Dpkg::Options::=--force-confold $(cat /tmp/packages.list) || exit 1
    rm /tmp/os-release /tmp/packages.list
    ;;
  *)
    echo "Usage: ${0##*/} COMMAND FILE_NAME"
    echo "  create         create backup"
    echo "  restore        restore backup"
    ;;
esac
