# clean up list of manual installed packages

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
	file://0008-optional-move-for-backup.patch \
	file://invoke \
	file://invoke.cfg \
	"

FILES:${PN} += " \
	${sysconfdir}/dpkg/* \
	"

do_install:append () {
	install -d ${D}${sysconfdir}/dpkg/dpkg.cfg.d
	install -m 0644 ${WORKDIR}/invoke.cfg ${D}${sysconfdir}/dpkg/dpkg.cfg.d
	install -d ${D}${sysconfdir}/dpkg/invoke.d
	install -m 0755 ${WORKDIR}/invoke ${D}${sysconfdir}/dpkg/
}
