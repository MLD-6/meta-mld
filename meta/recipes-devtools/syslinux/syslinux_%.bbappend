FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
	file://splash-640x480.png \
	file://splash-720x576.png \
	file://splash-800x600.png \
	file://splash-1280x720.png \
	file://splash-1920x1080.png \
	file://splash-3840x2160.png \
	"

do_configure() {
	oe_runmake clean
}

do_compile() {
	oe_runmake installer
}

do_install:class-target:append() {
	oe_runmake install INSTALLROOT="${D}"

	install -d ${D}${datadir}/syslinux/mld/
	install -m 0755 -D ${WORKDIR}/splash-*.png ${D}${datadir}/syslinux/mld/
}

FILES:${PN}-misc += " \
	${datadir}/syslinux/mld/* \
	"
