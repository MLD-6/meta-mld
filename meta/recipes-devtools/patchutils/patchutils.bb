SUMMARY = "A collection of tools for manipulating patch files"
DESCRIPTION = "Patchutils is a small collection of programs that operate on patch files."
PV = "0.4.2"

LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=YOUR_MD5_CHECKSUM_HERE"

SRC_URI = "http://cyberelk.net/tim/data/patchutils/stable/patchutils-${PV}.tar.xz"
SRC_URI[sha256sum] = "8875b0965fe33de62b890f6cd793be7fafe41a4e552edbf641f1fed5ebbf45ed"

S = "${WORKDIR}/patchutils-${PV}"

inherit autotools native

do_install:append() {
    # Optionally, if you want to install additional documentation
    install -d ${D}${datadir}/doc/${BPN}-${PV}
    install -m 0644 ${S}/doc/* ${D}${datadir}/doc/${BPN}-${PV}
}

PROVIDES:append:class-native = " patchutils-native"
BBCLASSEXTEND="native"
