SUMMARY = "The Python X Library."
PR = "r0"

LICENSE = "LGPL-2.1-only"
LIC_FILES_CHKSUM = "file://LICENSE;md5=fc178bcd425090939a8b634d1d6a9594"

SRCREV = "4e8bbf8fc4941e5da301a8b3db8d27e98de68666"
SRC_URI = "git://github.com/python-xlib/python-xlib.git;protocol=https;branch=master"

DEPENDS = " \
	python3-wheel-native \
	python3-pip-native \
"

RDEPENDS:${PN} = " \
	python3 \	
	python3-six \
"

inherit setuptools3

S = "${WORKDIR}/git"

python __anonymous() {
  d.appendVarFlag('do_compile', 'network', '1') 
}
