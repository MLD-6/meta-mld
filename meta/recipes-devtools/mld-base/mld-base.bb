SUMMARY = "MLD base funktions"
DESCRIPTION = "Look for updates and anounce system to mld server"
HOMEPAGE = "https://www.minidvblinux.de"
BUGTRACKER = "https://www.minidvblinux.de/bug"

LICENSE = "GPL-2.0-only"

SRC_URI = " \
	file://announce.service \
	file://announce.timer \
	"

inherit systemd

SYSTEMD_SERVICE:${PN} = "announce.service announce.timer"

FILES:${PN} += " \
	${systemd_unitdir}/system/* \
	"

do_install() {
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/announce.service ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/announce.timer ${D}${systemd_unitdir}/system
}
