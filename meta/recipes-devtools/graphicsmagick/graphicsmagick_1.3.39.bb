SUMMARY = "Swiss army knife of image processing"
HOMEPAGE = "http://www.graphicsmagick.org/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://Copyright.txt;md5=93c4d59582eec44dba74c2c65be2de25"

SRC_URI = "${SOURCEFORGE_MIRROR}/${BPN}/GraphicsMagick-${PV}.tar.bz2"
SRC_URI[md5sum] = "a550b573c4880df5e1ff287837f0fe84"
SRC_URI[sha256sum] = "2bd2a908dfbea6d7586224c8a471d916331fa697e45023198119603ffc9496b8"

S = "${WORKDIR}/GraphicsMagick-${PV}"

inherit autotools

DEPENDS = "libpng"

FILES:${PN} += "${datadir}/GraphicsMagick-1.3.39/config ${libdir}/GraphicsMagick-1.3.39/config"

EXTRA_OECONF += "--enable-static=no --enable-shared=yes --enable-magick-compat"