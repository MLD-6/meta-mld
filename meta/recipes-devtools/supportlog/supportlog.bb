SUMMARY = "Support log tool"
DESCRIPTION = "Create and uplodad a support log"
HOMEPAGE = "https://www.minidvblinux.de"
BUGTRACKER = "https://www.minidvblinux.de/bug"

LICENSE = "GPL-2.0-only"

SRC_URI = " \
	file://supportlog.sh \
	"

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/supportlog.sh ${D}${bindir}
}

FILES:${PN} += " \
	${bindir}/* \
	"
