SUMMARY = "Remote connection tools"
DESCRIPTION = "Tools to open a support remote connection"
HOMEPAGE = "https://www.minidvblinux.de"
BUGTRACKER = "https://www.minidvblinux.de/bug"

LICENSE = "GPL-2.0-only"

SRC_URI = " \
	file://remoteconnection.service \
	file://remoteconnection.sh \
	file://remotessh.sh \
	file://remoteconnection_rsa \
	file://remoteconnection_rsa.pub \
	"

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/remoteconnection.sh ${D}${bindir}
	install -m 0755 ${WORKDIR}/remotessh.sh ${D}${bindir}
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/remoteconnection.service ${D}${systemd_unitdir}/system
	install -d ${D}${sysconfdir}
	install -m 0600 ${WORKDIR}/remoteconnection_rsa ${D}${sysconfdir}
	install -m 0644 ${WORKDIR}/remoteconnection_rsa.pub ${D}${sysconfdir}
}

FILES:${PN} += " \
	${bindir}/* \
	${sysconfdir}/* \
	${systemd_unitdir}/system/* \
	"
