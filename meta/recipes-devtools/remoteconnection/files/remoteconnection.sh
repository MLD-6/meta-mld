#!/bin/sh

if [ ! "$1" ]; then
  echo "usage: $0 PORT" >&2
  echo "Open a remote ssh connection tunnel" >&2
  exit
fi

ssh -f -o TCPKeepAlive=yes -o StrictHostKeyChecking=no -i /etc/remoteconnection_rsa -N -R $1:localhost:22 tunnel@minidvblinux.de
