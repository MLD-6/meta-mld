#!/bin/sh

if [ ! "$1" ]; then
	echo "usage: $0 PORT [OPTIONS]" >&2
	echo "Open a remote ssh connection to a MLD user" >&2
	echo "To get access to the remote webif use the option '-L 8000:localhost:80' and call http://localhost:8000" >&2
	exit
fi

ssh -o StrictHostKeyChecking=no -o ProxyCommand="ssh -o StrictHostKeyChecking=no -i /etc/remoteconnection_rsa -W %h:%p tunnel@minidvblinux.de" root@localhost -p $@
