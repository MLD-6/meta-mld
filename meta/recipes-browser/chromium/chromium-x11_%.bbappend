FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
	file://chromium.menu \
	file://chromium \
	"

PACKAGECONFIG = "use-egl use-vaapi"

do_install:append() {
	if [ -e libGLESv2.so ]; then
    ln -s libGLESv2.so ${D}${libdir}/chromium/libGLESv2.so.2
	fi
  rm -f ${D}${bindir}/chromium
	install -m 0755 ${WORKDIR}/chromium ${D}${bindir}
	install -d ${D}${datadir}/menu
	install -m 0644 ${WORKDIR}/chromium.menu ${D}${datadir}/menu
}

FILES:${PN} += " \
	${datadir}/menu/* \
	"

# pkg_postinst:${PN}() {
# #!/bin/sh
# useradd -N -G audio user 2>/dev/null; true
# }
