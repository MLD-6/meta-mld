SUMMARY = "A web based file manager"
DESCRIPTION = "A file manager for to use ba a web browser."
PV = "2.31.1"
PR = "r2"
ARCH:aarch64 = "arm64"
ARCH:arm = "armv7"
ARCH:x86 = "386"
ARCH:x86-64 = "amd64"

LICENSE = "Apache-2.0"

SRC_URI = "https://github.com/filebrowser/filebrowser/releases/download/v${PV}/linux-${ARCH}-filebrowser.tar.gz;name=${ARCH}"
SRC_URI[amd64.sha256sum] = "dee16329431450ae33138a5c28e841a235833992560611e16b9f583b2da1a6c6"
SRC_URI[armv7.sha256sum] = "2daa0b516ae4cae0be3e24a0829117d13a0c96b8f09e643baa6708704820df95"
SRC_URI[arm64.sha256sum] = "5917529f03f88ab3128c89c330bd9eabfadc05cf4179887ff3ba04a111888e49"


SRC_URI += "\
	file://filebrowser.service \
	file://filebrowser.sh \
"

inherit systemd

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/filebrowser ${D}${bindir}
	install -m 0755 ${WORKDIR}/filebrowser.sh ${D}${bindir}
	install -d ${D}${systemd_unitdir}/system/
	install -m 0644 ${WORKDIR}/filebrowser.service ${D}${systemd_unitdir}/system/
}

INSANE_SKIP:${PN} += "already-stripped"

SYSTEMD_SERVICE:${PN} = "filebrowser.service"
