#!/bin/sh

if [ "$1" = "login" ]; then
  test "$USERNAME" = "$(setting get auth.username)" -a  "$(setting call auth.login $PASSWORD)" == "true" && echo "hook.action=auth" || echo "hook.action=block"
  exit
fi

db=/var/lib/filebrowser/database.db
filebrowser="filebrowser -c /dev/null -d $db"

if [ ! -f $db ]; then
  $filebrowser -p 8009 -a '' -r / --username root >/dev/null &
  sleep 1; kill $!
  $filebrowser config set --branding.theme dark >/dev/null
  $filebrowser config set --cache-dir /var/cache/filebrowser/ >/dev/null
  $filebrowser users update root --viewMode list >/dev/null
fi

$filebrowser users update root --locale $(setting get system.locale | cut -b -2) >/dev/null

if [ "$(setting get auth.required)" = 'true' ]; then
  $filebrowser config set --auth.method hook --auth.command "$0 login" >/dev/null
else
  $filebrowser config set --auth.method noauth >/dev/null
fi

$filebrowser
