SUMMARY = "Tntnet is a web application server for web applications written in C++."
PV = "git"
PR = "r3"

LICENSE = "LGPL-2.1-only"

SRC_URI = "git://github.com/maekitalo/tntnet.git;protocol=https;branch=master"
SRCREV = "ffebd17441d56c43749b88b3b51353feaf2e2a6d"

S = "${WORKDIR}/git"

inherit autotools pkgconfig

EXTRA_OECONF += " \
    --disable-unittest \
	"

DEPENDS += " \
    zlib \
    cxxtools \
    "

do_compile() {
    oe_runmake
}

do_install() {
    oe_runmake install DESTDIR=${D}
}

# Skip .so symlink check for tntnet
INSANE_SKIP:${PN} += "dev-so"

BBCLASSEXTEND = "native nativesdk"
