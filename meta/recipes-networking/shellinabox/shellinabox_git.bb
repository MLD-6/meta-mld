SUMMARY = "Shell In A Box"
DESCRIPTION = "Shell In A Box implements a web server that can export arbitrary command line tools to a web based terminal emulator."
HOMEPAGE = "https://github.com/shellinabox/shellinabox"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/patches:${THISDIR}/${PN}/files:"
PV = "2.21+git${SRCPV}"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=a193d25fdef283ddce530f6d67852fa5"

inherit autotools-brokensep pkgconfig systemd

DEPENDS += "zlib"

GIT_PROTOCOL = "https"
SRC_URI = "git://github.com/shellinabox/shellinabox.git;protocol=https;branch=master"
SRCREV = "4f0ecc31ac6f985e0dd3f5a52cbfc0e9251f6361"

# Patches
SRC_URI += "\
	file://0001-Add-cert-file-option.patch \
	file://shellinabox.service \
	file://white-on-black.css \
"
S = "${WORKDIR}/git"

PACKAGECONFIG ??= "ssl"
PACKAGECONFIG[ssl] = "--enable-ssl,--disable-ssl,openssl"

RDEPENDS:${PN} += "\
	${@bb.utils.contains('PACKAGECONFIG', 'ssl', 'openssl-bin', '', d)} \
"

EXTRA_OECONF += " --disable-runtime-loading"

do_install:append() {
	install -d -m 0755 ${D}${sysconfdir}/shellinabox
	install -m 0644 ${WORKDIR}/white-on-black.css ${D}${sysconfdir}/shellinabox/

	install -d ${D}${systemd_unitdir}/system/
	install -m 0644 ${WORKDIR}/shellinabox.service ${D}${systemd_unitdir}/system/
}

FILES:${PN} += "${sysconfdir} \
                ${systemd_unitdir}/system/shellinabox.service \
"

SYSTEMD_SERVICE:${PN} = "shellinabox.service"