#!/bin/sh

load_firmware()
(
  FIRMWARE=$1
  echo "Loading $FIRMWARE:"
  i=0
  while [ $i -lt 3 ]; do
    i=$(($i + 1))
    ping -c1 "mld6.minidvblinux.de" > /dev/null && break
    sleep 5
  done
  mkdir -p "/lib/firmware/$(dirname $FIRMWARE)"
  curl "https://mld6.minidvblinux.de/firmware/$FIRMWARE" -f 2>/dev/null -o "/lib/firmware/$FIRMWARE"
)

inc()
(
  echo $((0$(cat /tmp/firmware 2>/dev/null) + $1)) > /tmp/firmware
  test $(cat /tmp/firmware) != 0
)

if [ "$1" ]; then

  DEVPATH="$1"
  FIRMWARE=${DEVPATH##*/}
  FIRMWARE=${FIRMWARE//\!//}

  if [ ! -e "/lib/firmware/$FIRMWARE.not_found" ]; then
    inc 1 && plymouth display-message --text="Loading firmware $FIRMWARE"
    load_firmware $FIRMWARE
    inc -1 || plymouth display-message --text=""

    if [ -e "/lib/firmware/$FIRMWARE" ]; then
      rm -f "/lib/firmware/$FIRMWARE.not_found"
      echo 1  2>/dev/null >"/$DEVPATH/loading"
      cat "/lib/firmware/$FIRMWARE" 2>/dev/null >"/$DEVPATH/data"
    else
      ls -l "/${DEVPATH%firmware/*}driver" 2>/dev/null | sed "s/.*\///" > "/lib/firmware/$FIRMWARE.not_found"
      echo "Failed to load $FIRMWARE"
    fi
  fi
  echo 0 2>/dev/null >"/$DEVPATH/loading"

elif [ -d /lib/firmware ]; then

  # Attempts to load firmware that could not be loaded at boot time, e.g. because there was no network connection yet
  find /lib/firmware/ -name *.not_found | while read firmware; do
    FIRMWARE=${firmware%.not_found}
    FIRMWARE=${FIRMWARE#/lib/firmware/}

    load_firmware $FIRMWARE

    if [ -e "/lib/firmware/$FIRMWARE" ]; then
      module=$(cat "/lib/firmware/$FIRMWARE.not_found")
      if [ "$module" ]; then
        modprobe -r $module && modprobe $module
      fi
      rm "/lib/firmware/$FIRMWARE.not_found"
    else
      echo "Failed to load $FIRMWARE"
    fi
  done

fi
true
