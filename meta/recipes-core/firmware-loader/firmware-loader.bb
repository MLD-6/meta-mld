SUMMARY = "Firmware loader"
DESCRIPTION = "Load needed firmware from web"
HOMEPAGE = "https://www.minidvblinux.de"
BUGTRACKER = "https://www.minidvblinux.de/bug"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = " \
	file://firmware-loader.sh \
	file://firmware-loader.service \
	file://firmware-loader@.service \
	file://60-firmware-loader.rules \
	"

inherit systemd
SYSTEMD_SERVICE:${PN} = "firmware-loader.service" 

RDEPENDS:${PN} += "curl"

UDEV_DIR="${nonarch_base_libdir}/udev"

FILES:${PN} += " \
	${UDEV_DIR}/* \
	${systemd_unitdir}/system/* \
	"

do_install() {
	install -d ${D}${UDEV_DIR}
	install -m 0755 ${WORKDIR}/firmware-loader.sh ${D}${UDEV_DIR}
	install -d ${D}${UDEV_DIR}/rules.d
	install -m 0644 ${WORKDIR}/60-firmware-loader.rules ${D}${UDEV_DIR}/rules.d
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/firmware-loader.service ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/firmware-loader@.service ${D}${systemd_unitdir}/system
}
