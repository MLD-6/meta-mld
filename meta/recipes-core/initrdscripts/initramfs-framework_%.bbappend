FILESEXTRAPATHS:prepend := "${THISDIR}/initramfs-framework:"
SRC_URI += "file://rootfs"

do_install:append() {
	install -m 0755 ${WORKDIR}/rootfs ${D}/init.d/90-rootfs
}
