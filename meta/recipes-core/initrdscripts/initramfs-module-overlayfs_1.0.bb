SUMMARY = "initramfs-framework module for overlayfs"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS:${PN} = "initramfs-framework-base udev-extraconf"

inherit allarch

FILESEXTRAPATHS:prepend := "${THISDIR}/initramfs-framework:"
SRC_URI = "file://overlayfs"

S = "${WORKDIR}"

do_install() {
	install -d ${D}/init.d
	install -m 0755 ${WORKDIR}/overlayfs ${D}/init.d/95-overlayfs
}

FILES:${PN} = "/init.d/95-overlayfs"
