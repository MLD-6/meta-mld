SUMMARY = "initramfs-framework module for snapshots manager"

LICENSE = "GPL-2.0-only"

RDEPENDS:${PN} = "initramfs-framework-base parted dialog btrfs-tools"

inherit allarch

FILESEXTRAPATHS:prepend := "${THISDIR}/initramfs-framework:"
SRC_URI = "file://snapshots"

S = "${WORKDIR}"

do_install() {
	install -d ${D}/init.d
	install -m 0755 ${WORKDIR}/snapshots ${D}/init.d/70-snapshots
}

FILES:${PN} = "/init.d/70-snapshots"
