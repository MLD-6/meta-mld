FILESEXTRAPATHS:prepend := "${THISDIR}/initramfs-framework:"
SRC_URI += "file://setup-live"

do_install:append() {
	install -m 0755 ${WORKDIR}/setup-live ${D}/init.d/80-setup-live
}
