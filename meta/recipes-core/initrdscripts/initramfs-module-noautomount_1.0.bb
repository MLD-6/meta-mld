SUMMARY = "initramfs-framework module to disable automount"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS:${PN} = "initramfs-framework-base"

inherit allarch

FILESEXTRAPATHS:prepend := "${THISDIR}/initramfs-framework:"
SRC_URI = "file://noautomount"

S = "${WORKDIR}"

do_install() {
	install -d ${D}/init.d
	install -m 0755 ${WORKDIR}/noautomount ${D}/init.d/00-noautomount
}

FILES:${PN} = "/init.d/00-noautomount"
