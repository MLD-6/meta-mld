SUMMARY = "keyd - key mapping tool"
DESCRIPTION = "A deamon for application based key mapping"
PV = "git"
PR = "r3"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://LICENSE;md5=12f884d2ae1ff87c09e5b7ccc2c4ca7e"

SRCREV = "a11e6c70d4e73c9194a87dbf3d2cdb76007d8271"
SRC_URI = "git://github.com/rvaiya/keyd.git;protocol=https;branch=master"
SRC_URI[sha256sum] = "73a796ee73fc6b737a2780963af2f4d3c4ecb6bee522020f80c3e7425abfaf38"

SRC_URI += " \
	file://time.patch \
	file://app-confg-dir.patch \
	file://default.conf \
	file://browser.conf \
	file://keyd.service \
	file://keyd-application-mapper.sh \
	"

inherit systemd

SYSTEMD_SERVICE:${PN} = "keyd.service"

RDEPENDS:${PN} = " \
	python3 \
	python3-xlib \
	python3-fcntl \
	"

S = "${WORKDIR}/git"

do_install() {
	oe_runmake DESTDIR=${D} install

	install -d ${D}${sysconfdir}/keyd/apps.d
	install -m 0644 ${WORKDIR}/default.conf ${D}${sysconfdir}/keyd
	install -m 0644 ${WORKDIR}/browser.conf ${D}${sysconfdir}/keyd/apps.d
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/keyd.service ${D}${systemd_unitdir}/system
	install -d ${D}/${sysconfdir}/X11/Xsession.d/
	install -m 0755 ${WORKDIR}/keyd-application-mapper.sh ${D}/${sysconfdir}/X11/Xsession.d/30keyd-application-mapper.sh
}

FILES:${PN} += " \
	${bindir}/* \
	${sysconfdir}/keyd/* \
	${systemd_unitdir}/system/* \
	${sysconfdir}/X11/Xsession.d/ \
	"
