DESCRIPTION = "MLD installer."
SUMMARY = "MLD installer"
AUTHOR = "Claus Muus"
HOMEPAGE = "http://www.mld.de"
SECTION = "base/utils"
LICENSE = "GPL-2.0-only"
PR = "r1"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI = " \
	file://install.menu \
	file://install.sh \
	file://install.service \
	"

RDEPENDS:${PN} = " parted dosfstools btrfs-tools xfsprogs"
RDEPENDS:${PN}:append:arm = " gptfdisk"
RDEPENDS:${PN}:append:x86-64 = " syslinux syslinux-misc efibootmgr"

FILES:${PN} += " \
	${bindir}/* \
	${datadir}/menu/* \
	${systemd_unitdir}/system/* \
	"

do_install () {
	install -d ${D}${datadir}/menu
	install -m 0644 ${WORKDIR}/install.menu ${D}${datadir}/menu
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/install.sh ${D}${bindir}
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/install.service ${D}${systemd_unitdir}/system
}
