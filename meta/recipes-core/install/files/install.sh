#!/bin/sh
# install the system on hdd

set -e

while [ $# != 0 ]; do
  case $1 in
    -s) system_dev=$2
        shift 2
        ;;
    -d) data_dev=$2
        shift 2
        ;;
    --df) data_format=true
        shift 1
        ;;
    --dp) data_path=$2
        shift 2
        ;;
    --dc) data_copy=true
        shift 1
        ;;
    -i) install_packages=$2
        shift 2
        ;;
    -p) post_install=1
        shift 1
        ;;
    -h) show_help=1
        break
        ;;
     *) show_help=1
        break
        ;;
  esac
done

if [ -z "${post_install}" -a -z "${system_dev}" -o "${show_help}" ]; then
  echo "Usage $0 {-p | -s DEVICE [-d DEVICE] [-i PACKAGES]}" >&2
  echo "  -s DEVICE     System device to install on" >&2
  echo "  -d DEVICE     Data device" >&2
  echo "  --df          Format data device" >&2
  echo "  --dp PATH     Path on data device" >&2
  echo "  --dc          Copy data of current system device to new system device" >&2
  echo "  -i PACKAGES   Packages to install" >&2
  echo "  -p            Do postinstall action" >&2
  exit 1
fi

export LC_ALL=C

if [ "${post_install}" ]; then
  packages=$(setting get install.packages)
  if [ "${packages}" ]; then
    export DEBIAN_FRONTEND=noninteractive
    setting set install.failed false
    apt-get --error-on=any update || setting set install.failed true
    apt-get install -q -y -o=Dpkg::Options::=--force-confdef -o=Dpkg::Options::=--force-confold $( (echo ${packages} | tr " " $'\n'; apt-cache --generate pkgnames) | sort | uniq -d) || setting set install.failed true
  fi | while read line; do
    echo -e "$line\r"
    [ "$line" ] || continue
    [ "${line##Get*}" ] || ([ "${line##*Packages*}" -a "${line##*InRelease*}" ] && plymouth display-message --text="Downloading $(echo "$line" | cut -d " " -f4)" || plymouth display-message --text="Downloading updates")
    [ "${line##Unpacking*}" ] || plymouth display-message --text="$(echo "$line" | cut -d " " -f1-2)"
    [ "${line##Setting up*}" ] || plymouth display-message --text="$(echo "$line" | cut -d " " -f1-3)"
    [ "${line##Removing*}" ] || plymouth display-message --text="$(echo "$line" | cut -d " " -f1-2)"
    [ "${line##Err*}" ] || plymouth display-message --text="Failed to fetch $(echo "$line" | cut -d " " -f4)"
  done

  if [ "$(setting get install.failed)" != "true" ]; then
    systemctl disable install
    if [ -e /boot/syslinux/syslinux.cfg ] && ! grep -q 'needs="vc"' /usr/share/menu/*; then
      sed "s/vga=0x314/vga=current/" -i /boot/syslinux/syslinux.cfg
    fi
  fi

  if [ -d /sys/firmware/efi/efivars ] && efibootmgr &>/dev/null && [ "$(setting get install.boot_device)" ]; then
    # add efi boot entry in case it could not be set during installation
    efibootmgr | grep " MLD"$'\t' | cut -b5-8 | while read n; do echo $n; efibootmgr -b $n -B; done >/dev/null
    efibootmgr -c -d $(setting get install.boot_device) -L "MLD" -l "/efi/boot/bootx64.efi" > /dev/null
  fi
  exit
fi


if [ ! -e "${system_dev}" ]; then
  echo "Unknown device '${system_dev}'" >&2
  exit 2
fi
if [ "${data_dev}" -a ! -e "${data_dev}" ]; then
  echo "Unknown device '${data_dev}'" >&2
  exit 2
fi


step=0
steps=7
progress()
{
  [ "$2" ] && steps=$((steps + $2))
  step=$((step + 1))
  echo "$((step * 100 / steps))% $1"
}


progress "Start installing on ${system_dev}"

# Unmount the target device
umount ${system_dev}* 2>/dev/null || true

boot_size=300

part_prefix=""
if [ ! "${system_dev#/dev/mmcblk}" = "${system_dev}" ] || \
   [ ! "${system_dev#/dev/nvme}" = "${system_dev}" ]; then
    part_prefix="p"
fi
boot_part=${system_dev}${part_prefix}1
root_part=${system_dev}${part_prefix}2
data_part=${data_dev}

if [ "${install_packages}" ]; then
  progress "Store Package list..." 1
  setting set install {failed:false}
  setting set install.packages "${install_packages}"
  systemctl enable install 2>/dev/null
fi


progress "Creating partition table..."
parted()
(
  set -o pipefail
  /usr/sbin/parted $* 2>&1 | (grep -v "^Information:\|^\s*$" || true) >&2
)
dd if=/dev/zero of=${system_dev} bs=512 count=35 2>/dev/null
parted ${system_dev} mktable gpt
parted ${system_dev} mkpart boot fat32 0% $boot_size
parted ${system_dev} set 1 legacy_boot on
parted ${system_dev} mkpart root btrfs $boot_size 100%
# create bios boot partition entry for rpi
[ -x /usr/sbin/gdisk ] && printf %s\\n r h 1 n c y n w y | gdisk ${system_dev} >/dev/null
sleep 1


progress "Formatting boot partition to fat..."
mkfs.vfat -F 32 ${boot_part} >/dev/null
progress "Formatting root partition to btrfs..."
mkfs.btrfs -q -f ${root_part} >/dev/null
if [ "${data_part}" -a "${data_format}" ]; then
  progress "Formatting data partition to xfs..." 1
  mkfs.xfs -q -f ${data_part}
fi


progress "Creating boot filesystem..."
boot_tgt=/tmp/boot_tgt
boot_src=/tmp/boot_src
mkdir -p ${boot_tgt}
mkdir -p ${boot_src}

umount ${boot_tgt} 2>/dev/null || true
umount ${boot_src} 2>/dev/null || true
mount ${boot_part} ${boot_tgt}
mount --bind /boot ${boot_src}

if [ -d ${boot_src}/isolinux ]; then
  cp ${boot_src}/{initrd,bzImage} ${boot_tgt}

  mkdir -p ${boot_tgt}/syslinux
  cp /usr/share/syslinux/{ldlinux.c32,libcom32.c32,libutil.c32,vesamenu.c32} ${boot_tgt}/syslinux/
  cp ${boot_src}/isolinux/splash.lss ${boot_tgt}/syslinux/
  sed "s/vga=current/vga=0x314/g" ${boot_src}/isolinux/isolinux.cfg > ${boot_tgt}/syslinux/syslinux.cfg

  mkdir -p ${boot_tgt}/efi/boot
  cp /usr/share/syslinux/mld/* ${boot_tgt}/efi/boot/
  cp /usr/share/syslinux/efi64/{ldlinux.e64,libcom32.c32,libutil.c32,syslinux.efi,vesamenu.c32} ${boot_tgt}/efi/boot/
  cp ${boot_tgt}/efi/boot/syslinux.efi ${boot_tgt}/efi/boot/bootx64.efi
  sed "s/ vga=current//g" ${boot_src}/isolinux/isolinux.cfg > ${boot_tgt}/efi/boot/syslinux.cfg

  mount --bind ${boot_tgt} /boot
  setting call display.updateSplash
  umount /boot
else
  cp -rL ${boot_src}/* ${boot_tgt}
fi

root_uuid=$(blkid ${root_part} | sed 's/.*UUID="\(\S*\)".*/\1/')
if [ -d ${boot_tgt}/syslinux ]; then
  sed "s/root=\S*/root=UUID=${root_uuid}/g" -i ${boot_tgt}/syslinux/syslinux.cfg
  sed "s/root=\S*/root=UUID=${root_uuid}/g" -i ${boot_tgt}/efi/boot/syslinux.cfg
  sync
  syslinux --install --directory syslinux ${boot_part} 2>/dev/null
  cat /usr/share/syslinux/gptmbr.bin > ${system_dev}
elif [ -e ${boot_tgt}/cmdline.txt ]; then
  sed "s/root=.* rootwait /root=UUID=${root_uuid} /g" -i ${boot_tgt}/cmdline.txt
fi

umount ${boot_tgt}
umount ${boot_src}


if [ -d /sys/firmware/efi/efivars ] && efibootmgr &>/dev/null; then
  progress "Add uefi boot entry..." 1
  efibootmgr | grep "\sMLD\s" | cut -b5-8 | while read n; do echo $n; efibootmgr -b $n -B; done >/dev/null
  efibootmgr -c -d ${system_dev} -L "MLD" -l "/efi/boot/syslinux.efi" > /dev/null
else
  setting set install.boot_device ${system_dev}
fi


progress "Creating root filesystem..."
root_tgt=/tmp/root_tgt
root_src=/tmp/root_src
mkdir -p ${root_tgt}
mkdir -p ${root_src}
mkdir -p /data

umount ${root_tgt} 2>/dev/null || true
umount ${root_src} 2>/dev/null || true
mount ${root_part} ${root_tgt} -o subvol=/
if [ -d /rootfs ]; then
  mount -t overlay overlay -o "ro,lowerdir=/rootfs/lowerdir,upperdir=/rootfs/upperdir,workdir=/rootfs/workdir,noatime,nodiratime" ${root_src}
else
  mount --bind / ${root_src}
fi

#btrfs subvolume create ${root_tgt}/@cache
btrfs subvolume create ${root_tgt}/@root >/dev/null
btrfs subvolume set-default $(btrfs subvolume list ${root_tgt} | grep " @root" | cut -d " " -f2) ${root_tgt}/@root

btrfs subvolume create ${root_tgt}/@data >/dev/null
root_size=$(/usr/sbin/parted -sm ${root_part} unit gib print | grep ^${root_part} | cut -d: -f2 | sed "s/\(\..*\)\?GiB//")
if [ ${root_size:-0} -ge 20 ]; then
  btrfs quota enable ${root_tgt}
  btrfs qgroup limit $((${root_size} - 10))G ${root_tgt}/@data
fi

cp -a ${root_src}/* ${root_tgt}/@root
rm -rf ${root_tgt}/@root/rootfs ${root_tgt}/@root/boot/*

sed "s/\( \/ .*\)defaults/\1noatime/" -i ${root_tgt}/@root/etc/fstab

if [ -d /rootfs ]; then
  boot_uuid=$(blkid ${boot_part} | sed 's/.*UUID="\(\S*\)".*/\1/')
  sed '/ \/boot /d' -i ${root_tgt}/@root/etc/fstab
  echo "UUID=${boot_uuid} /boot auto noatime 1 2" >> ${root_tgt}/@root/etc/fstab
  if [ "${data_part}" ]; then
    data_uuid=$(blkid ${data_part} | sed 's/.*UUID="\(\S*\)".*/\1/')
    mkdir -p ${root_tgt}/@root/mnt/${data_uuid}
    echo "UUID=${data_uuid} /mnt/${data_uuid} auto noatime 1 2" >> ${root_tgt}/@root/etc/fstab
    echo "/mnt/${data_uuid}${data_path:+/$data_path} /data none bind 0 0" >> ${root_tgt}/@root/etc/fstab
  else
    root_uuid=$(blkid ${root_part} | sed 's/.*UUID="\(\S*\)".*/\1/')
    mkdir -p ${root_tgt}/@root/mnt/${root_uuid}
    echo "UUID=${root_uuid} /mnt/${root_uuid} btrfs subvol=@data 0 0" >> ${root_tgt}/@root/etc/fstab
    echo "/mnt/${root_uuid} /data none bind 0 0" >> ${root_tgt}/@root/etc/fstab
  fi
else
  old_boot_uuid=$(blkid $(mount | sed -n 's|\(.*\) on /boot .*|\1|p') | sed 's/.*UUID="\(\S*\)".*/\1/')
  new_boot_uuid=$(blkid ${boot_part} | sed 's/.*UUID="\(\S*\)".*/\1/')
  sed "s/$old_boot_uuid/$new_boot_uuid/g" -i ${root_tgt}/@root/etc/fstab
  old_root_uuid=$(blkid $(mount | sed -n 's|\(.*\) on / .*|\1|p') | sed 's/.*UUID="\(\S*\)".*/\1/')
  new_root_uuid=$(blkid ${root_part} | sed 's/.*UUID="\(\S*\)".*/\1/')
  sed "s/$old_root_uuid/$new_root_uuid/g" -i ${root_tgt}/@root/etc/fstab
fi

# We dont want udev to mount our root and data device while we're booting...
echo "${system_dev}" >> ${root_tgt}/@root/etc/udev/mount.blacklist
[ "${data_dev}" ] && echo "${data_dev}" >> ${root_tgt}/@root/etc/udev/mount.blacklist

umount ${root_tgt}
umount ${root_src}

if [ "${data_copy}" ]; then
  progress "Copying data..." 1
  data_tgt=/tmp/data_tgt
  data_src=/tmp/data_src
  mkdir -p ${data_tgt}
  mkdir -p ${data_src}

  umount ${data_tgt} 2>/dev/null || true
  umount ${data_src} 2>/dev/null || true
  mount ${root_part} ${data_tgt} -o subvol=/@data
  mount $(mount | sed -n 's|\(.*\) on / .*|\1|p') ${data_src} -o subvol=/@data

  cp -a ${data_src}/* ${data_tgt}

  umount ${data_tgt}
  umount ${data_src}
fi

progress "Finish installing"
