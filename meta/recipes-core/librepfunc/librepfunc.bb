# librepfunc_1.0.bb
DESCRIPTION = "This is librepfunc - a collection of functions, classes and so on, which i use often."
HOMEPAGE = "https://github.com/wirbel-at-vdr-portal/librepfunc"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=892f569a555ba9c07a568a7c0c4fa63a"

SRC_URI = "git://github.com/wirbel-at-vdr-portal/librepfunc.git;protocol=https;branch=main"
SRCREV = "041a31472a33d7bc51658d2e156011aa8f9f6122"

PV = "git"
PR = "r1"

S = "${WORKDIR}/git"

inherit pkgconfig

do_install() {
    oe_runmake DESTDIR=${D} install
}
