FILESEXTRAPATHS:prepend := "${THISDIR}/systemd-conf:"

SRC_URI += "file://logind-mld.conf"

do_install:append() {
	install -D -m0644 ${WORKDIR}/logind-mld.conf ${D}${systemd_unitdir}/logind.conf.d/01-${PN}.conf
}

pkg_postinst:${PN} () {
	echo "Disabling getty on console 1-2"

	if [ -n "$D" ]; then
		OPTS="--root=$D"
	else
		OPTS=""
	fi

	for i in 1 2 ; do
		systemctl $OPTS mask getty@tty$i.service
	done
}
