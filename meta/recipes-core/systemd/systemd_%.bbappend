FILESEXTRAPATHS:prepend := "${THISDIR}/systemd:"

SRC_URI += " \
  file://0005-add-hdx-to-disk-by-uuid.patch \
  file://locale.sh \
  "

do_install:append() {
  sed "s/\(ExecStart\=.*\/systemd-networkd-wait-online\)$/\\1 --timeout 30 -i eth0 -i wlan0 --any/" -i ${D}${rootlibexecdir}/systemd/system/systemd-networkd-wait-online.service

	install -d ${D}${sysconfdir}/profile.d
	install -m 0644 ${WORKDIR}/locale.sh ${D}${sysconfdir}/profile.d
}
