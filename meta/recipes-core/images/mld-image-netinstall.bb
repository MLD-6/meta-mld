SUMMARY = "A netinstall MLD image."

IMAGE_INSTALL = "packagegroup-core-boot ${CORE_IMAGE_EXTRA_INSTALL}"

IMAGE_INSTALL += "packagegroup-wifi"
IMAGE_INSTALL += "packagegroup-x11-matchbox"
IMAGE_INSTALL += "firmware-loader snapshot alsa-init libasound-module-pcm-upmix surf webif install appstarter ir-keytable irmp keyd mld-base nano htop shellinabox mergerfs"
LICENSE = "GPLv2"

inherit core-image
inherit extrausers

SYSTEMD_DEFAULT_TARGET = "graphical.target"

EXTRA_USERS_PARAMS = "\
    useradd -N -G audio user; \
    "
