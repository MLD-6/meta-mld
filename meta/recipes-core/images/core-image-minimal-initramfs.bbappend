INITRAMFS_SCRIPTS = "\
	initramfs-framework-base \
	initramfs-module-setup-live \
	initramfs-module-udev \
	"
INITRAMFS_SCRIPTS += "initramfs-module-noautomount"
INITRAMFS_SCRIPTS += "initramfs-module-overlayfs"
#INITRAMFS_SCRIPTS += "initramfs-module-quiet"
#INITRAMFS_SCRIPTS += "initramfs-module-snapshots"

#INITRAMFS_SCRIPTS += "initramfs-module-debug"

#PACKAGE_INSTALL += "plymouth"
PACKAGE_INSTALL += "udev"
