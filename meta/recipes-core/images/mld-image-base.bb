SUMMARY = "A base MLD image."

IMAGE_INSTALL = "packagegroup-core-boot ${CORE_IMAGE_EXTRA_INSTALL}"

IMAGE_INSTALL += "packagegroup-x11-matchbox"

LICENSE = "GPLv2"

inherit core-image

SYSTEMD_DEFAULT_TARGET = "graphical.target"
