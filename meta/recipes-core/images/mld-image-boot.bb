SUMMARY = "A netinstall MLD image."

IMAGE_INSTALL = "packagegroup-core-boot ${CORE_IMAGE_EXTRA_INSTALL}"

IMAGE_INSTALL += "firmware-loader snapshot alsa-init libasound-module-pcm-upmix webif install ir-keytable"
LICENSE = "GPLv2"

inherit core-image
