SUMMARY = "App starter"
DESCRIPTION = "Start a selected app"
HOMEPAGE = "https://www.minidvblinux.de"
BUGTRACKER = "https://www.minidvblinux.de/bug"
PR = "r2"

LICENSE = "GPL-2.0-only"

SRC_URI = " \
	file://appstarter.sh \
	file://appstarter.service \
	file://appstarter_reset.service \
	file://appstarter_xserver.service \
	"

inherit systemd

SYSTEMD_SERVICE:${PN} = "appstarter.service appstarter_reset.service appstarter_xserver.service"

RDEPENDS:${PN} += "coreutils"

FILES:${PN} += " \
	${bindir}/* \
	${systemd_unitdir}/system/* \
	"

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/appstarter.sh ${D}${bindir}
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/appstarter.service ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/appstarter_reset.service ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/appstarter_xserver.service ${D}${systemd_unitdir}/system
}
