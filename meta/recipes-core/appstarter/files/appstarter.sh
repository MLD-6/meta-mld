#!/bin/sh

eval $(setting get --sh apps.app)

export DISPLAY=:0
export HOME=/root

if [ "$needs" = "vc" ]; then
  systemctl stop xserver-nodm
  chvt ${vc:-1}
fi
if [ "$needs" = "x11" ]; then
  if [ ! -e /tmp/.X11-unix/X0 ]; then
    systemctl start xserver-nodm
  else
    chvt 2
  fi
fi

setting set apps.active "$title"

sh -c "$(setting get -d "$command" "apps.commands.${title//[^a-zA-Z0-9]/_}")"
status=$?

if [ $status = 0 -a  "$title" != "$(setting get apps.autostart)" ]; then
  setting unset apps.active
  exit 1 # force a appstarter service restart
fi

exit $status