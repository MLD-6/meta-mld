FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
	file://resolution.patch \
	file://theme.patch \
	file://timeout.patch \
	file://watermark-640x480.png \
	file://watermark-720x576.png \
	file://watermark-800x600.png \
	file://watermark-1280x720.png \
	file://watermark-1920x1080.png \
	file://watermark-3840x2160.png \
	"

do_install:append() {
	install -m 0644 -D ${WORKDIR}/watermark-*.png ${D}${datadir}/plymouth/themes/spinner/
	ln -fs watermark-800x600.png ${D}${datadir}/plymouth/themes/spinner/watermark.png
}
