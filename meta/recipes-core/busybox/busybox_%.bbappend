FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
	file://disable_syslog.cfg \
	file://volumeid.cfg \
	"

SYSTEMD_SERVICE:${PN}-syslog = ""
ALTERNATIVE:${PN}-syslog = ""
ALTERNATIVE_LINK_NAME[syslog-conf] = ""
FILES:${PN}-syslog = ""

do_install:append() {
	rm -f ${D}${systemd_unitdir}/system/busybox-klogd.service
	rm -rd ${D}/usr/lib
}
