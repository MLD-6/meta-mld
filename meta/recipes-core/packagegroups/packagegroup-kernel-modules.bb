SUMMARY = "(MLD) kernel-modules group"
PR = "r1"

inherit packagegroup

RDEPENDS:${PN} = "\
	kmod-8168 \
	"

RDEPENDS:${PN}:append:genericx86-64 = "\
	nvidia \
	nvidia-legacy \
	tbs \
	"

RDEPENDS:${PN}:append:qemux86-64 = "\
	"
