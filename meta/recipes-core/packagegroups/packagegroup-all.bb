SUMMARY = "(MLD) all packages"
PR = "r1"

inherit packagegroup

RDEPENDS:${PN} = "\
	packagegroup-alsa \
	packagegroup-debug \
	packagegroup-gstreamer \
	packagegroup-kernel-modules \
	packagegroup-tools \
	packagegroup-vdr \
	packagegroup-wifi \
	"

#RDEPENDS:${PN}:append:genericx86-64 = "\
#	packagegroup-webbrowser \
#	"
