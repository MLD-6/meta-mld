SUMMARY = "(MLD) develop packages"
PR = "r0"

inherit packagegroup

RDEPENDS:${PN} = "\
	packagegroup-core-buildessential \
	chrpath \
	diffstat \
	git \
	make \
	openssh-sftp-server \
	python3-setuptools \
	python3-pip \
	python3-dev \
	socat \
	texinfo \
	unzip \
	wget \
	"
