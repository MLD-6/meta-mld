SUMMARY = "(MLD) alsa group"
PR = "r1"

inherit packagegroup

RDEPENDS:${PN} = "\
	alsa-equal \
	alsa-lib \
	alsa-plugins \
	alsa-tools \
	alsa-utils \
	alsa-ucm-conf \
	"
