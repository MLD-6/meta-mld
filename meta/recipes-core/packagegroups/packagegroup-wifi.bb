SUMMARY = "(MLD) wifi group"
PR = "r1"

inherit packagegroup

RDEPENDS:${PN} = "\
	networkmanager-nmcli \
	networkmanager-wifi \
	wpa-supplicant \
	"
