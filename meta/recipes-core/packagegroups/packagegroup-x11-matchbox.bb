SUMMARY = "Matchbox X11 session"
DESCRIPTION = "Packages required to set up a matchbox X11 session"
PR = "r1"

inherit packagegroup features_check

REQUIRED_DISTRO_FEATURES = "x11"

RDEPENDS:${PN} = "\
	packagegroup-core-x11 \
	liberation-fonts \
	matchbox-wm \
	x-session \
	"
