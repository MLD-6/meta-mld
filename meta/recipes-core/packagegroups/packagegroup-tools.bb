SUMMARY = "(MLD) tools group"
PR = "r2"

inherit packagegroup

RDEPENDS:${PN} = "\
	at \
	avahi \
	bluealsa \
	cefbrowser \
	channellogos \
	cronie \
	docker-moby \
	evtest \
	filebrowser \
	flatpak \
	hdparm \
	htop \
	hub-ctrl \
	hyperion \
	i2c-tools \
	igt-gpu-tools \
	iperf3 \
	iw \
	keyd \
	libva-utils \
	lirc-drv-yausbir \
	lms \
	lmsensors \
	lshw \
	mc \
	mediathek-hbbtv \
	mergerfs \
	minisatip \
	nano \
	netdata \
	networkmanager \
	openvpn \
	oscam \
	packagegroup-core-nfs-server \
	packagegroup-core-nfs-client \
	pciutils \
	podman \
	podman-compose \
	podman-tui \
	projectx \
	remotetranscode \
	samba \
	setserial \
	shellinabox \
	squeezeplay \
	sundtek \
	supportlog \
	tree \
	usbutils  \
	util-linux \
	vdr-checkts \
	x11vnc \
	wol \
	wpa-supplicant \
	w-scan \
	xev \
	yausbir \
	yausbir-lirc \
	yt-dlp \
	"
RDEPENDS:${PN}:append:raspberrypi4 = "\
	rpi-eeprom \
	"
