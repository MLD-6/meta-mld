SUMMARY = "(MLD) vdr group"
PR = "r2"

inherit packagegroup

RDEPENDS:${PN} = "\
	vdr \
	vdr-epg-daemon \
	vdr-plugin-cecremote \
	vdr-plugin-chanman \
	vdr-plugin-channellists \
	vdr-plugin-control \
	vdr-plugin-ddci2 \
	vdr-plugin-dummydevice \
	vdr-plugin-dvbapi \
	vdr-plugin-epg2vdr \
	vdr-plugin-epgsearch \
	vdr-plugin-epgsync \
	vdr-plugin-extrecmenu \
	vdr-plugin-extrecmenung \
	vdr-plugin-favorites \
	vdr-plugin-femon \
	vdr-plugin-fritzbox \
	vdr-plugin-gstreamerdevice \
	vdr-plugin-iptv \
	vdr-plugin-imonlcd \
	vdr-plugin-lcdproc \
	vdr-plugin-live \
	vdr-plugin-markad \
	vdr-plugin-menuorg \
	vdr-plugin-mlist \
	vdr-plugin-mpv \
	vdr-plugin-neutrinoepg \
	vdr-plugin-osd2web \
	vdr-plugin-osdteletext \
	vdr-plugin-plex \
	vdr-plugin-pvrinput \
	vdr-plugin-radio \
	vdr-plugin-rcu \
	vdr-plugin-recsearch \
	vdr-plugin-restfulapi \
	vdr-plugin-robotv \
	vdr-plugin-satip \
	vdr-plugin-seduatmo \
	vdr-plugin-skincurses \
	vdr-plugin-skindesigner \
	vdr-plugin-skinenigmang \
	vdr-plugin-skinenigmang-logos-xpm-hi \
	vdr-plugin-skinflatplus \
	vdr-plugin-skinlcarsng \
	vdr-plugin-skinnopacity \
	vdr-plugin-skinsimple \
	vdr-plugin-skinsoppalusikka \
	vdr-plugin-streamdev-client \
	vdr-plugin-streamdev-server \
	vdr-plugin-suspendoutput \
	vdr-plugin-svdrpservice \
	vdr-plugin-systeminfo \
	vdr-plugin-targavfd \
	vdr-plugin-tvguide \
	vdr-plugin-tvguideng \
	vdr-plugin-tvscraper \
	vdr-plugin-undelete \
	vdr-plugin-vnsiserver \
	vdr-plugin-vompserver \
	vdr-plugin-webbridge \
	vdr-plugin-web \
	vdr-plugin-wirbelscan \
	svdrpsend \
	vdr-addon-avahi-linker \
	"

RDEPENDS:${PN}:append:genericx86-64 = "\
	vdr-plugin-mcli \
	vdr-plugin-softhddevice-cuvid \
	vdr-plugin-softhddevice-intel \
	vdr-plugin-softhddevice-intel-ihd \
	vdr-plugin-softhddevice-nvidia \
	vdr-plugin-xineliboutput \
	"
RDEPENDS:${PN}:append:qemux86-64 = "\
	vdr-plugin-xineliboutput \
	"
RDEPENDS:${PN}:append:raspberrypi = "\
	vdr-plugin-rpihddevice \
	"
RDEPENDS:${PN}:append:raspberrypi2 = "\
	vdr-plugin-rpihddevice \
	"
RDEPENDS:${PN}:append:raspberrypi3 = "\
	vdr-plugin-rpihddevice \
	"
RDEPENDS:${PN}:append:raspberrypi4 = "\
	vdr-plugin-softhddevice-drm-gles \
	vdr-plugin-xineliboutput \
	"
RDEPENDS:${PN}:append:raspberrypi5 = "\
	vdr-plugin-softhddevice-drm-gles \
	"
RDEPENDS:${PN}:append:rock-pi-4b = "\
	vdr-plugin-softhddevice-drm-gles \
	"
RDEPENDS:${PN}:append:tinker-board = "\
	vdr-plugin-softhddevice-drm \
	"
RDEPENDS:${PN}:append:tinker-board-s = "\
	vdr-plugin-softhddevice-drm \
	"
RDEPENDS:${PN}:append:hardkernel-odroidn2plus = "\
	vdr-plugin-softhddevice-drm-gles \
	"
