SUMMARY = "(MLD) debug packages"
PR = "r0"

inherit packagegroup

RDEPENDS:${PN} = "\
	packagegroup-core-buildessential \
	gcc \
	gdb \
	gdbserver \
	gnu-config \
	m4 \
	make \
	python3-dev \
	python3-pip \
	python3-setuptools \
	openssh-sftp-server \
	strace \
	"
