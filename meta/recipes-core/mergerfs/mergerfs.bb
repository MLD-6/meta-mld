SUMMARY = "mergerfs - a union filesystem"
DESCRIPTION = "A filesystem to merge several source directories into one directory"

LICENSE = "GPL-2.0-only"

SRC_URI = "git://github.com/trapexit/mergerfs.git;protocol=https;branch=master"
SRCREV = "42d0b5734c9ef959a0cab28c76cbb57f7af89ca0"

SRC_URI += "file://disable-stripping.patch"
SRC_URI += "${@bb.utils.contains('TUNE_FEATURES', 'arm', 'file://32bit.patch', '', d)}"

S = "${WORKDIR}/git"

INSANE_SKIP:${PN} += "already-stripped ldflags"

EXTRA_OEMAKE = " \
	STRIP=/bin/true \
	PREFIX=/usr \
	"

do_install() {
	oe_runmake DESTDIR=${D} install
	mv ${D}/sbin ${D}${sbindir}
}
