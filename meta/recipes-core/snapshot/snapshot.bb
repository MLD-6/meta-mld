SUMMARY = "Snapshot manager"
DESCRIPTION = "Manage snapshots of the root file system"
HOMEPAGE = "https://www.minidvblinux.de"
BUGTRACKER = "https://www.minidvblinux.de/bug"

LICENSE = "GPL-2.0-only"

SRC_URI = " \
	file://snapshot_manager \
	file://snapshot \
	file://snapshot.invoke \
	"

RRECOMMENDS:${PN} += "dialog"

FILES:${PN} += " \
	${bindir}/* \
	${sysconfdir}/dpkg/invoce.d/* \
	"

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/snapshot_manager ${D}${bindir}
	install -m 0755 ${WORKDIR}/snapshot ${D}${bindir}
	install -d ${D}${sysconfdir}/dpkg/invoke.d
	install -m 0755 ${WORKDIR}/snapshot.invoke ${D}${sysconfdir}/dpkg/invoke.d/snapshot
}
