#!/bin/sh

if [ -z "$1" -o "$1" = "-h" ]; then
	echo "Usage: $0 [-d DEVICE] {list | create NAME | restore NAME | delete NAME}" >&2
	echo "       Manage snapshots of the root filesystem."
	exit
fi

if [ "$1" = "-d" ]; then
	device=$2
	shift 2
fi
device=${device:-$(mount | grep " / " | cut -d" " -f1)}
action="$1"
name="$2"

if ! blkid | grep -q "^$device:.*btrfs"; then
	echo "Wrong root filesystem type" >&2
	exit 1
fi

case $action in
	list)
		mkdir -p /mnt/root
		if mount $device /mnt/root -o subvol=/ 2>/dev/null; then
			usleep 100000
			#btrfs subvolume list /mnt/root | grep -v "@" | cut -d " " -f9- | sort
			ls /mnt/root | grep -v "@" | sort | while read snapshot; do
				echo "$snapshot - $(cat "/mnt/root/$snapshot/snapshotinfo" 2>/dev/null | head -n1)"
			done
			umount /mnt/root
		fi
		rmdir /mnt/root
		;;
	create)
		status=1
		if [ ! "$name" ]; then
			echo "no snapshot name given"
		else
			mkdir -p /mnt/root
			mount $device /mnt/root -o subvol=/
			usleep 100000
			snapshot="$(date +"%Y-%m-%d %H:%M")"
			if grep -q " /boot " /proc/mounts; then
				cp -prf /boot/* /mnt/root/@root/boot/
				find /mnt/root/@root/boot -name ldlinux.sys -exec rm {} \;
			fi
			if btrfs subvolume snapshot /mnt/root/@root "/mnt/root/$snapshot"; then
			 	echo "$name" > "/mnt/root/$snapshot/snapshotinfo"
				status=0
			fi
			umount /mnt/root
			rmdir /mnt/root
		fi
		exit $status
		;;
	restore)
		status=1
		if [ ! "$name" ]; then
			echo "no snapshot name given"
		else
			mkdir -p /mnt/root
			if mount $device /mnt/root -o subvol=/; then
				usleep 100000
				backup="$(date +"%Y-%m-%d %H:%M")"
				if [ -e "/mnt/root/$backup" ]; then
					echo "can not create snapshot '$backup' before restore '$name'. snapshot already exists"
				elif [ ! -e "/mnt/root/${name% - *}" ]; then
					echo "snapshot '$name' does not exists"
				else
					root="$(ls -d /mnt/root/@root)"
					mv "$root" "/mnt/root/$backup"
					if btrfs subvolume snapshot "/mnt/root/${name% - *}" "$root"; then
						echo "auto snapshot before restore of snapshot ${name% - *}" > "/mnt/root/$backup/snapshotinfo"
						if btrfs subvolume set-default $(btrfs subvolume list /mnt/root | grep " @root" | cut -d " " -f2) /mnt/root; then
							mkdir -p /mnt/boot
							bootdevice=$(sed -n "s/\(.*\) \+\/boot .*/\1/p" $root/etc/fstab)
							if [ "$bootdevice" ] && mount "$bootdevice" /mnt/boot; then
								cp -prf $root/boot/* /mnt/boot/
								umount /mnt/boot
							fi
							rmdir /mnt/boot
							echo "you must reboot to activate the restore!"
							status=0
						else
							echo "activation of restore failed"
						fi
					else
						echo "restore of snapshot '$name' failed"
					fi
				fi
				umount /mnt/root
			else
				echo "can not mount device '$device'"
			fi
			rmdir /mnt/root
		fi
		exit $status
		;;
	delete)
		status=1
		if [ ! "$name" ]; then
			echo "no snapshot name given"
		else
			mkdir -p /mnt/root
			mount $device /mnt/root -o subvol=/
			usleep 100000
			if btrfs subvolume delete "/mnt/root/${name% - *}"; then
				status=0
			else
				echo "delete of snapshot '$name' failed"
			fi
			umount /mnt/root
			rmdir /mnt/root
		fi
		exit $status
		;;
esac
