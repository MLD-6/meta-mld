#!/bin/sh

createSnapshot()
{
	eval dialog --clear --backtitle \"$SYSTEM_NAME\" --title \"$(gt 'Create snapshot')\" --inputbox \"$(gt 'Name of new snapshot')\" 8 60 \"\" 2>/tmp/$$
	if [ $? = 0 ]; then
		snapshot="$(date +'%Y-%m-%d %H:%M')"
		if grep -q " /boot " /proc/mounts; then
			cp -prf /boot/* /mnt/root/@root*/boot/
			find /mnt/root/@root*/boot -name ldlinux.sys -exec rm {} \;
		fi
		if [ ! -e "/mnt/root/$snapshot" ] && btrfs subvolume snapshot /mnt/root/@root* "/mnt/root/$snapshot"; then
			cat /tmp/$$ > "/mnt/root/$snapshot/snapshotinfo"
		else
			dialog --clear --backtitle "$SYSTEM_NAME" --title "$(gt 'Create snapshot')" --msgbox  "$(gt 'Creation of the snapshot failed')" 6 60
		fi
		rm -f /tmp/$$
	fi
}

restoreSnapshot()
{
	#btrfs subvolume list /mnt/root | grep -v "@" | cut -d " " -f9- | sort > /tmp/$$
	ls /mnt/root | grep -v "@" | sort > /tmp/$$
	options=
	while read snapshot; do
		options="$options '$snapshot' '$(cat "/mnt/root/$snapshot/snapshotinfo" 2>/dev/null | head -n1)'"
	done < /tmp/$$; rm /tmp/$$
	eval dialog --clear --backtitle \"$SYSTEM_NAME\" --title \"$(gt 'Restore snapshot')\" --menu \"$(gt 'Please select the snapshot that should be restore')\" 20 80 13 $options 2>/tmp/$$
	if [ $? = 0 ]; then
		snapshot="$(cat /tmp/$$)"
		backup="$(date +"%Y-%m-%d %H:%M")"
		root="$(ls -d /mnt/root/@root*)"
		if [ ! -e "/mnt/root/$backup" ] && mv "$root" "/mnt/root/$backup" && btrfs subvolume snapshot "/mnt/root/$snapshot" "$root"; then
			echo "auto snapshot before restore of snapshot $snapshot" > "/mnt/root/$backup/snapshotinfo"
			if btrfs subvolume set-default $(btrfs subvolume list /mnt/root | grep " @root" | cut -d " " -f2) /mnt/root; then
				boot=$(grep " /boot " $root/etc/fstab | cut -d" " -f1)
				blkid -t "$boot" &>/dev/null && boot=$(blkid -o device -l -t "$boot")
				if [ -e "$boot" ]; then
					mkdir -p /mnt/boot
					mount $boot /mnt/boot
					cp -prf $root/boot/* /mnt/boot/
					umount /mnt/boot
					rmdir /mnt/boot
				fi
				dialog --clear --backtitle "$SYSTEM_NAME" --title "$(gt 'Restore snapshot')" --yesno  "$(gt 'Snapshot restored. Reboot now?')" 6 60
				if [ $? = 0 ]; then
					reboot
					exit
				fi
			else
				dialog --clear --backtitle "$SYSTEM_NAME" --title "$(gt 'Restore snapshot')" --msgbox  "$(gt 'Switch to snapshot failed')" 6 60
			fi
		else
			if [ ! -e "$root" ]; then
				mv "/mnt/root/$backup" "$root"
			fi
			dialog --clear --backtitle "$SYSTEM_NAME" --title "$(gt 'Restore snapshot')" --msgbox  "$(gt 'Restore of the snapshot failed')" 6 60
		fi
		rm -f /tmp/$$
	fi
}

deleteSnapshot()
{
	#btrfs subvolume list /mnt/root | grep -v "@" | cut -d " " -f9- | sort > /tmp/$$
	ls /mnt/root | grep -v "@" | sort > /tmp/$$
	options=
	while read snapshot; do
		options="$options '$snapshot' '$(cat "/mnt/root/$snapshot/snapshotinfo" 2>/dev/null | head -n1)'"
	done < /tmp/$$; rm /tmp/$$
	eval dialog --clear --backtitle \"$SYSTEM_NAME\" --title \"$(gt 'Delete snapshot')\" --menu \"$(gt 'Please select the snapshot that should be delete')\" 20 80 13 $options 2>/tmp/$$
	if [ $? = 0 ]; then
		snapshot="$(cat /tmp/$$)"
		if ! btrfs subvolume delete "/mnt/root/$snapshot"; then
			dialog --clear --backtitle "$SYSTEM_NAME" --title "$(gt 'Delete snapshot')" --msgbox  "$(gt 'Delete of the snapshot failed')" 6 60
		fi
		rm -f /tmp/$$
	fi
}

gt()
{
	echo "$*"
}



SYSTEM_NAME=$(setting get system.hostname)
#export NCURSES_NO_UTF8_ACS=1

if ! mount | grep -q "/ type btrfs"; then
	options=$(
		parted -lsm 2>/dev/null | while IFS=: read dev _ _ size type _ name _; do echo $dev | grep -q ^/ && device="$name:$dev"; test "$type" = btrfs && echo "$device$dev:$size"; done | while IFS=: read name dev size; do
			echo "'$dev' '$name - $size'"
		done
	)

	eval dialog --clear --backtitle \"$SYSTEM_NAME\" --title \"$(gt 'Snapshot device')\" --menu \"$(gt 'Please select your system device')\" 20 60 13 $options 2>/tmp/$$
	if [ $? = 0 -a -s /tmp/$$ ]; then
		dev="$(cat /tmp/$$)"
		ROOT_DISK=$dev
		rm -f /tmp/$$
	else
		exit
	fi
else
	ROOT_DISK=$(mount | grep " / " | cut -d" " -f1)
fi

while true; do
	options=
	options="$options '$(gt 'Create snapshot')' ''"
	options="$options '$(gt 'Restore snapshot')' ''"
	options="$options '$(gt 'Delete snapshot')' ''"
	eval dialog --clear --backtitle \"$SYSTEM_NAME\" --title \"$(gt 'Manage snapshots')\" --menu \"$(gt 'Please select what you want to do')\" 20 60 13 $options 2>/tmp/$$
	if [ $? = 0 ]; then
		mkdir -p /mnt/root
		if mount $ROOT_DISK /mnt/root -o subvol=/; then
			case "$(cat /tmp/$$)" in
				"$(gt 'Create snapshot')")
					createSnapshot
				;;
				"$(gt 'Restore snapshot')")
					restoreSnapshot
				;;
				"$(gt 'Delete snapshot')")
					deleteSnapshot
				;;
			esac
			umount /mnt/root
		else
			dialog --clear --backtitle "$SYSTEM_NAME" --title "$(gt 'Manage snapshots')" --msgbox  "$(gt 'Mount of snapshot device failed')" 6 60
		fi
		rmdir /mnt/root
		rm -f /tmp/$$
	else
		if [ -n "$init" ]; then
			dialog --clear --backtitle "$SYSTEM_NAME" --title "$(gt 'Manage snapshots')" --yesno  "$(gt 'Reboot now?')" 6 60
			if [ $? = 0 ]; then
				reboot
				exit
			fi
		fi
		exit
	fi
done
