FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
PR = "r7"

SRC_URI += "file://compress-kernel.cfg"
SRC_URI += "file://forcedeth.cfg"
SRC_URI += "file://fw-loader.cfg"
SRC_URI += "file://hid.cfg"
SRC_URI += "file://i2c.cfg"
SRC_URI += "file://ir.cfg"
SRC_URI += "file://nfsd.cfg"
SRC_URI += "file://nouveau.cfg"
SRC_URI += "file://ntfs.cfg"
SRC_URI += "file://r8169.cfg"
SRC_URI += "file://smipcie.cfg"
SRC_URI += "file://snd.cfg"
SRC_URI += "file://squashfs.cfg"
#SRC_URI += "file://usb-serial.cfg"
SRC_URI += "file://wlan.cfg"
SRC_URI += "file://xfs.cfg"
SRC_URI += "file://video.cfg"
SRC_URI += "file://meson.cfg"
SRC_URI += "file://bt.cfg"

KERNEL_FEATURES += "cfg/vesafb.scc"
KERNEL_FEATURES += "features/media/media-all.scc"
KERNEL_FEATURES += "features/overlayfs/overlayfs.scc"
KERNEL_FEATURES += "features/pci/pci.scc"
KERNEL_FEATURES += "features/wifi/wifi-all.scc"

# pkg_postrm:${KERNEL_PACKAGE_NAME}-image-bzimage:prepend() {
# #!/bin/sh
# [ ! -e ${KERNEL_IMAGEDEST}/bzImage-* ] || exit 0
# }
