FILESEXTRAPATHS:prepend := "${THISDIR}/setserial:"

SRC_URI += " \
  file://setserial.service \
  "

inherit systemd
SYSTEMD_SERVICE:${PN} = "setserial.service" 

do_install:append() {
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/setserial.service ${D}${systemd_unitdir}/system/
}

FILES:${PN} += "  \
	${systemd_unitdir}/system/setserial.service  \ 
	"
