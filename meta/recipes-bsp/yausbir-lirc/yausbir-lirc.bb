SUMMARY = "yausbir_lirc deamon"
DESCRIPTION = "lirc for yausbir remote control receiver"
PV = "git"
PR = "r0"

LICENSE = "GPL-2.0-only"

SRCREV = "4fbc03d5d8b6a28abc3f3f054739cd22cd9f753b"
SRC_URI = "git://github.com/M-Reimer/yausbir_lirc.git;protocol=https;branch=master"

inherit systemd

SYSTEMD_SERVICE:${PN} = "yausbir_lirc.service"

DEPENDS = "libusb1"
RDEPENDS:${PN} = "lirc"

S = "${WORKDIR}/git"

inherit pkgconfig

INSANE_SKIP:${PN} += "ldflags"

do_install() {
	install -d ${D}${sbindir}/
	install -m 0755 -D ${S}/yausbir_lirc ${D}${sbindir}

	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${S}/yausbir_lirc.service ${D}${systemd_unitdir}/system
}

FILES:${PN} += " \
	${systemd_unitdir}/system/* \
	${sbindir}/* \
	"

pkg_postinst:${PN}:append() {
	sed "s/^\(driver \+=\) default/\1 udp/" -i $D/etc/lirc/lirc_options.conf
	sed "s/^\(device \)/#\1/" -i $D/etc/lirc/lirc_options.conf
}

pkg_prerm:${PN}:append() {
	sed "s/^\(driver \+=\) udp/\1 default/" -i $D/etc/lirc/lirc_options.conf
	sed "s/^#\(device \)/\1/" -i $D/etc/lirc/lirc_options.conf
}
