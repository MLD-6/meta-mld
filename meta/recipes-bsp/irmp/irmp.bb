SUMMARY = "IRMP STM32 Linux Configuration"
DESCRIPTION = "A tool to configure STM32 IRMP remotecontrol devices"
HOMEPAGE = "https://www.minidvblinux.de"
BUGTRACKER = "https://www.minidvblinux.de/bug"
PR = "r7"

LICENSE = "GPL-2.0-only"

SRC_URI = " \
  https://github.com/j1rie/IRMP_STM32_KBD/raw/master/RP2xxx/build/2025-01-29_23-41_waveshare_rp2040_one_hid_irmp_kbd.uf2;downloadfilename=irmp-rp2040-firmware.uf2;name=rp2040 \
  https://github.com/j1rie/IRMP_STM32_KBD/raw/master/RP2xxx/build/2025-01-29_23-42_seeed_xiao_rp2350_hid_irmp_kbd.uf2;downloadfilename=irmp-rp2050-firmware.uf2;name=rp2050 \
  "
SRC_URI[rp2040.sha256sum] = "5f2a86569836d21599fb183fb9bcd9ceda1557b744568be7d64adefe86464f6e"
SRC_URI[rp2050.sha256sum] = "f46f06676c39d6689f49d62c37faa1c7301c64bc4e9698cc625273e9156c4748"

SRC_URI += " \
	file://stm32kbdIRconfig_cmd \
  "

S = "${WORKDIR}/stm32kbdIRconfig_cmd"

TARGET_CC_ARCH += "${LDFLAGS}"

do_compile() {
  oe_runmake -C ${S}
}

do_install() {
  install -d ${D}/${bindir}
  install -m 0755 ${S}/stm32kbdIRconfig_cmd ${D}/${bindir}
  install -d ${D}/${base_libdir}/firmware
  install -m 0755 ${WORKDIR}/irmp-rp2040-firmware.uf2 ${D}/${base_libdir}/firmware
  install -m 0755 ${WORKDIR}/irmp-rp2050-firmware.uf2 ${D}/${base_libdir}/firmware
}

FILES:${PN} += " \
	${base_libdir}/firmware/* \
	"
