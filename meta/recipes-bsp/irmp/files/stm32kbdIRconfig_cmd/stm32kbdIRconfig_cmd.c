/**********************************************************************************************************
	stm32kbdIRconfig_cmd: configure and monitor IRMP_STM32_KBD

	Copyright (C) 2014-2023 Joerg Riechardt

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

***********************************************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sysexits.h>
#include <sys/stat.h>
#include <errno.h>
#include <inttypes.h>
#include <termios.h>
#include <fcntl.h>
#include "usb_hid_keys.h"
#include <sys/ioctl.h>
#include <linux/hidraw.h>
#include <signal.h>

enum access {
	ACC_GET,
	ACC_SET,
	ACC_RESET
};

enum command {
	CMD_CAPS,
	CMD_ALARM,
	CMD_IRDATA,
	CMD_KEY,
	CMD_WAKE,
	CMD_REBOOT,
	CMD_IRDATA_REMOTE,
	CMD_WAKE_REMOTE,
	CMD_REPEAT,
	CMD_EEPROM_RESET,
	CMD_EEPROM_COMMIT,
	CMD_EEPROM_GET_RAW,
	CMD_HID_TEST,
	CMD_STATUSLED,
	CMD_EMIT,
	CMD_NEOPIXEL,
	CMD_MACRO,
	CMD_MACRO_REMOTE,
};
enum status {
	STAT_CMD,
	STAT_SUCCESS,
	STAT_FAILURE
};

enum report_id {
	REPORT_ID_KBD = 1,
	REPORT_ID_CONFIG_IN = 2,
	REPORT_ID_CONFIG_OUT = 3
};

static int stm32fd = -1;
uint8_t inBuf[64];
uint8_t outBuf[64];
unsigned int in_size, out_size;

volatile sig_atomic_t done = 0;
void term(int signum) {
	done = 1;
}

static bool open_stm32(const char *devicename) {
	stm32fd = open(devicename, O_RDWR );
	if (stm32fd == -1) {
		fprintf(stderr, "error opening stm32 device: %s\n", strerror(errno));
		return false;
	}
	return true;
}

static void read_stm32(int in_size, int show_len = 0) {
	int retVal;
	retVal = read(stm32fd, inBuf, in_size);
	if (retVal < 0) {
		fprintf(stderr, "read error\n");
	} else if (show_len) {
		for (int i = 4; i < 4+show_len; i++)
			printf("%02hhx", inBuf[i]);
	}
} 

static void write_stm32(int in_len) {
	int retVal;
	retVal = write(stm32fd, outBuf, in_len);
	if (retVal < 0) {
		fprintf(stderr, "write error\n");
	}
}

bool write_and_check(int in_len, int show_len = 0) {
	write_stm32(in_len);
	usleep(3000);
	read_stm32(4 + show_len, show_len); // blocking per default, waits until data arrive
	while (inBuf[0] == REPORT_ID_KBD)
		read_stm32(4 + show_len, show_len);
	if (show_len)
		printf("\n");
	return (inBuf[0] == REPORT_ID_CONFIG_IN) && (inBuf[1] == STAT_SUCCESS) && (inBuf[2] == outBuf[2]) && (inBuf[3] == outBuf[3]);
}

int main(int argc, const char **argv) {
	uint64_t i;
	uint8_t l, m, eeprom_lines;
	char s[21], c;
	int res, desc_size = 0;
	unsigned int n;

	if (argc<4) {
		help:
		fprintf(stderr, "usage: %s HIDRAW_DEVICE [get [alarm|keymap|macros|repeat [delay|period|timeout]|wakeup] | set [alarm TIME|keymap|macros|repeat [delay|period|timeout] TIME|wakeup CODE|led STATE|neopixel LED RRGGBB] | emit CODE | querry ir]\n", argv[0]);
		return 1;
	}

	if (!open_stm32(argv[1])) {
		return 2;
	}

	struct hidraw_report_descriptor rpt_desc;
	// Get Report Descriptor Size
	res = ioctl(stm32fd, HIDIOCGRDESCSIZE, &desc_size);
	if (res < 0)
		perror("HIDIOCGRDESCSIZE");
	// Get Report Descriptor
	rpt_desc.size = desc_size;
	res = ioctl(stm32fd, HIDIOCGRDESC, &rpt_desc);
	if (res < 0)
		perror("HIDIOCGRDESC");
	// Get Report count
	for(n = 0; n < rpt_desc.size - 2; n++) {
		if(rpt_desc.value[n] == 0x95 && rpt_desc.value[n+2] == 0x81){ // REPORT_COUNT, INPUT
			in_size = rpt_desc.value[n+1] + 1;
		}
		if(rpt_desc.value[n] == 0x95 && rpt_desc.value[n+2] == 0x91){ // REPORT_COUNT, OUTPUT
			out_size = rpt_desc.value[n+1] + 1;
			break;
		}
	}

	if (!in_size) {
		fprintf(stderr, "hid in report count missing\n");
		return 3;
	}
	if (!out_size) {
		fprintf(stderr, "hid out report count missing\n");
		return 3;
	}

	outBuf[0] = REPORT_ID_CONFIG_OUT;
	outBuf[1] = STAT_CMD;

	outBuf[2] = ACC_GET;
	outBuf[3] = CMD_CAPS;
	outBuf[4] = 0;
	write_stm32(5);
	usleep(3000);
	read_stm32(in_size);
	while (inBuf[0] == 0x01)
		read_stm32(in_size);
	eeprom_lines = inBuf[4];
	if(in_size != (inBuf[7] ? inBuf[7] : 17))
		fprintf(stderr, "warning: hid in report count mismatch: %u %u\n", in_size, inBuf[7] ? inBuf[7] : 17);
	if(out_size != (inBuf[8] ? inBuf[8] : 17))
		fprintf(stderr, "warning: hid out report count mismatch: %u %u\n", out_size,  inBuf[8] ? inBuf[8] : 17);
	if(!inBuf[7] || !inBuf[8])
		fprintf(stderr, "warning: old firmware!\n");


	switch (argv[2][0]) {
	case 'g': // get
		outBuf[2] = ACC_GET;
		switch (argv[3][0]) {
		case 'a':
			outBuf[3] = CMD_ALARM;
			write_stm32(4);
			read_stm32(8);
			printf("%u\n", *(uint32_t *)(&inBuf[4]));
			break;
		case 'k': // get key mapping table
			for(l = 0; l < eeprom_lines; l++) {
				outBuf[3] = CMD_IRDATA;
				outBuf[4] = l;
				write_stm32(5);
				usleep(3000);
				read_stm32(10, 6);
				printf(" ");
				outBuf[3] = CMD_KEY;
				outBuf[4] = l;
				usleep(3000);
				write_stm32(5);
				usleep(3000);
				read_stm32(6);
				for(int n=0; n < lines-1; n++) {
					if(mapusb[n].usb_hid_key == inBuf[4]) {
						printf("%s", mapusb[n].key);
						break;
					}
				}
				printf("\n");
				usleep(3000);
			}
			break;
		case 'm': // get macros
			outBuf[3] = CMD_MACRO;
			for(m = 0; m < 8; m++) {
				outBuf[4] = m;
				for(l = 0; l < 8; l++) {
					outBuf[5] = l;
					write_stm32(6);
					usleep(3000);
					read_stm32(10, 6);
					printf(" ");
					usleep(3000);
				}
				printf("\n");
			}
			break;
		case 'r': // get key repeat
			if (argc<5) {
				close(stm32fd);
				goto help;
			}
			outBuf[3] = CMD_REPEAT;
			switch (argv[4][0]) {
			case 'd': // delay
				outBuf[4] = 0;
				break;
			case 'p': // period
				outBuf[4] = 1;
				break;
			case 't': // timeout
				outBuf[4] = 2;
				break;
			default:
				close(stm32fd);
				goto help;
			}
			write_stm32(5);
			read_stm32(6);
			printf("%u\n", *(uint16_t *)(&inBuf[4]));
			break;
		case 'w': // get wakeup key
			outBuf[3] = CMD_WAKE;
			outBuf[4] = 0;
			write_and_check(5, 6);
			break;
		default:
			fprintf(stderr, "unknown command\n");
			close(stm32fd);
			return 1;
		}
		break;
	case 's': // set
		outBuf[2] = ACC_SET;
		switch(argv[3][0]) {
		case 'a': // set alarm time
			if (argc<5) {
				close(stm32fd);
				goto help;
			}
			outBuf[3] = CMD_ALARM;
			sscanf(argv[4], "%" SCNu32 "", (uint32_t *)(&outBuf[4]));
			write_and_check(8);
			outBuf[3] = CMD_EEPROM_COMMIT;
			write_and_check(4);
			break;
		case 'k': // set key mapping table
			for(l = 0; l < eeprom_lines && scanf("%" SCNx64 " %s", &i, s) != EOF; l++) {
				outBuf[3] = CMD_IRDATA;
				outBuf[4] = l;
				outBuf[5] = (i>>40) & 0xFF;
				outBuf[6] = (i>>32) & 0xFF;
				outBuf[7] = (i>>24) & 0xFF;
				outBuf[8] = (i>>16) & 0xFF;
				outBuf[9] = (i>>8) & 0xFF;
				outBuf[10] = i & 0xFF;
				if(!write_and_check(11)) {
					close(stm32fd);
					return 5;
				}
				usleep(3000);
				outBuf[3] = CMD_KEY;
				outBuf[4] = l;
				outBuf[5] = 0xff;
				for(m=0; m < lines; m++) {
					if(!strcmp(mapusb[m].key, s)) {
						outBuf[5] = mapusb[m].usb_hid_key;
						break;
					}
				}
				if(outBuf[5] == 0xff)
					fprintf(stderr, "warning: unknown KEY code: %s\n", s);
				outBuf[6] = 0xff;
				if(!write_and_check(7)) {
					close(stm32fd);
					return 5;
				}
				usleep(3000);
			}

			for(; l < eeprom_lines; l++) {
				outBuf[3] = CMD_IRDATA;
				outBuf[4] = l;
				outBuf[5] = 0xff;
				outBuf[6] = 0xff;
				outBuf[7] = 0xff;
				outBuf[8] = 0xff;
				outBuf[9] = 0xff;
				outBuf[10] = 0xff;
				if(!write_and_check(11)) {
					close(stm32fd);
					return 5;
				}
				usleep(3000);
				outBuf[3] = CMD_KEY;
				outBuf[4] = l;
				outBuf[5] = 0xff;
				outBuf[6] = 0xff;
				if(!write_and_check(7)) {
					close(stm32fd);
					return 5;
				}
				usleep(3000);
			}
			outBuf[3] = CMD_EEPROM_COMMIT;
			write_and_check(4);
			break;
		case 'm': // set macros
			outBuf[3] = CMD_MACRO;
			for(l = 0, m = 0; l < 8 && m < 8 && scanf("%" SCNx64 "%c", &i, &c) != EOF;) {
				outBuf[4] = m;
				outBuf[5] = l;
				outBuf[6] = (i>>40) & 0xFF;
				outBuf[7] = (i>>32) & 0xFF;
				outBuf[8] = (i>>24) & 0xFF;
				outBuf[9] = (i>>16) & 0xFF;
				outBuf[10] = (i>>8) & 0xFF;
				outBuf[11] = i & 0xFF;
				if(!write_and_check(12)) {
					close(stm32fd);
					return 5;
				}
				usleep(3000);
				if (c == '\n') {
					l++;
					if (l < 8) {
						outBuf[2] = ACC_RESET;
						outBuf[5] = l;
						if(!write_and_check(6)) {
							close(stm32fd);
							return 5;
						}
						outBuf[2] = ACC_SET;
					}
					l = 0;
					m++;
				} else {
					l++;
				}
			}
			if (l) {
				l = 0;
				m++;
			}
			if (m < 8) {
				outBuf[2] = ACC_RESET;
				outBuf[4] = m;
				outBuf[5] = l;
				if(!write_and_check(6)) {
					close(stm32fd);
					return 5;
				}
				outBuf[2] = ACC_SET;
			}
			outBuf[3] = CMD_EEPROM_COMMIT;
			write_and_check(4);
			break;
		case 'r': // set key repeat
			if (argc<6) {
				close(stm32fd);
				goto help;
			}
			outBuf[3] = CMD_REPEAT;
			switch (argv[4][0]) {
			case 'd': // delay
				outBuf[4] = 0;
				break;
			case 'p': // period
				outBuf[4] = 1;
				break;
			case 't': // timeout
				outBuf[4] = 2;
				break;
			default:
				fprintf(stderr, "unknown repeat type\n");
				close(stm32fd);
				return 1;
			}
			sscanf(argv[5], "%" SCNu16 "", (uint16_t *)(&outBuf[5]));
			write_and_check(7);
			outBuf[3] = CMD_EEPROM_COMMIT;
			write_and_check(4);
			break;
		case 'w': // set wakeup key
			sscanf(argv[4], "%" SCNx64 "", &i);
			outBuf[3] = CMD_WAKE;
			outBuf[4] = 0;
			outBuf[5] = (i>>40) & 0xFF;
			outBuf[6] = (i>>32) & 0xFF;
			outBuf[7] = (i>>24) & 0xFF;
			outBuf[8] = (i>>16) & 0xFF;
			outBuf[9] = (i>>8) & 0xFF;
			outBuf[10] = i & 0xFF;
			if(!write_and_check(11)) {
				close(stm32fd);
				return 5;
			}
			outBuf[3] = CMD_EEPROM_COMMIT;
			write_and_check(4);
			break;
		case 'l': // set led status
			if (argc<5) {
				close(stm32fd);
				goto help;
			}
			outBuf[3] = CMD_STATUSLED;
			sscanf(argv[4], "%" SCNx8 "", (uint8_t *)(&outBuf[4]));
			write_and_check(5);
			outBuf[3] = CMD_EEPROM_COMMIT;
			write_and_check(4);
			break;
		case 'n': // set neo pixel
			if (argc<6) {
				close(stm32fd);
				goto help;
			}
			outBuf[3] = CMD_NEOPIXEL;
			sscanf(argv[4], "%" SCNu8 "", &l);
			outBuf[4] = 3 * l;
			outBuf[5] = (l - 1) / 19;
			outBuf[6] = 3 * ((l - 1) % 19) + 1;
			m = 6 + 3 * ((l - 1) % 19);
			sscanf(argv[5], "%2hhx%2hhx%2hhx", &outBuf[m+2], &outBuf[m+1], &outBuf[m+3]);
			write_and_check(m+4);
			outBuf[3] = CMD_EEPROM_COMMIT;
			write_and_check(4);
			break;
		default:
			fprintf(stderr, "unknown command\n");
			close(stm32fd);
			return 1;
		}
		break;
	case 'e': // emit ir code
		sscanf(argv[3], "%" SCNx64 "", &i);
		outBuf[2] = ACC_SET;
		outBuf[3] = CMD_EMIT;
		outBuf[4] = (i>>40) & 0xFF;
		outBuf[5] = (i>>32) & 0xFF;
		outBuf[6] = (i>>24) & 0xFF;
		outBuf[7] = (i>>16) & 0xFF;
		outBuf[8] = (i>>8) & 0xFF;
		outBuf[9] = i & 0xFF;
		if(!write_and_check(10)) {
			close(stm32fd);
			return 5;
		}
		break;
	case 'q': // querry
		switch (argv[3][0]) {
		case 'i': // querry ir codes
			signal(SIGTERM, term);
			while(!done) {
				outBuf[2] = ACC_SET;
				outBuf[3] = CMD_IRDATA_REMOTE;
				outBuf[4] = eeprom_lines-1;
				while(!done && !write_and_check(5)) {}
				outBuf[2] = ACC_GET;
				outBuf[3] = CMD_IRDATA;
				outBuf[4] = eeprom_lines-1;
				if(!done && !write_and_check(5, 6)) {
					close(stm32fd);
					return 5;
				}
			}
			outBuf[2] = ACC_SET;
			outBuf[3] = CMD_IRDATA;
			outBuf[4] = eeprom_lines-1;
			outBuf[5] = 0xff;
			outBuf[6] = 0xff;
			outBuf[7] = 0xff;
			outBuf[8] = 0xff;
			outBuf[9] = 0xff;
			outBuf[10] = 0xff;
			write_and_check(11);
			break;
		default:
			fprintf(stderr, "unknown command\n");
			close(stm32fd);
			return 1;
		}
		break;
	default:
		fprintf(stderr, "unknown action\n");
		close(stm32fd);
		return 1;
	}
	close(stm32fd);
	return 0;
}
