SUMMARY = "yausbir lirc deamon"
DESCRIPTION = "lirc for yausbir remote control receiver"
PV = "git"
PR = "r1"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://LICENSE;md5=12f884d2ae1ff87c09e5b7ccc2c4ca7e"

SRCREV = "1f6f1628a171aabf6a5392d3feaaea6f5deb0e54"
SRC_URI = "git://github.com/seahawk1986/pyYaUsbIR.git;protocol=https;branch=receiver"

SRC_URI += " \
	file://00_python3.8.patch \
	file://yausbir.service \
	"

inherit systemd

SYSTEMD_SERVICE:${PN} = "yausbir.service"

RDEPENDS:${PN} = " \
	python3 \
	python3-bitarray \
	python3-dbus \
	python3-pygobject \
	python3-uinput \
	python3-pyusb \
	"

S = "${WORKDIR}/git"

do_install() {
	install -d ${D}${sysconfdir}/
	install -m 0644 ${S}/keymap.txt ${D}${sysconfdir}/yausbir.conf

	install -d ${D}${libdir}/yausbir
	install -m 0755 -D ${S}/*.py ${D}${libdir}/yausbir

	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/yausbir.service ${D}${systemd_unitdir}/system
}

CONFFILES:${PN} += " \
  ${sysconfdir}/* \
  "
FILES:${PN} += " \
	${sysconfdir}/* \
	${systemd_unitdir}/system/* \
	${libdir}/yausbir/* \
	"
