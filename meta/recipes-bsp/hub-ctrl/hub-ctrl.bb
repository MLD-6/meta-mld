SUMMARY = "Control power of USB-hub ports"
DESCRIPTION = "Switch power of USB-hub ports on or off"
PR = "r1"

LICENSE = "GPL-2.0-only"

SRC_URI = " \
	file://src \
  "

S = "${WORKDIR}/src"

TARGET_CC_ARCH += "${LDFLAGS}"

DEPENDS = "libusb"

do_compile() {
  oe_runmake -C ${S}
}

do_install() {
  install -d ${D}/${bindir}
  install -m 0755 ${S}/hub-ctrl ${D}/${bindir}
}
