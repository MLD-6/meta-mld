SUMMARY = "Open Sans is a humanist sans serif typeface"
LICENSE = "Apache-2.0"
AUTHOR = "Steve Matteson"


SRC_URI = "git://github.com/googlefonts/opensans.git;protocol=https;branch=main"
SRCREV = "bd7e37632246368c60fdcbd374dbf9bad11969b6"

PR="r1"


S = "${WORKDIR}/git"


do_install() {
	install -d ${D}/usr/share/fonts
	install -m 0755 ${S}/fonts/ttf/OpenSans-BoldItalic.ttf      ${D}/usr/share/fonts
	install -m 0755 ${S}/fonts/ttf/OpenSans-Bold.ttf            ${D}/usr/share/fonts
	install -m 0755 ${S}/fonts/ttf/OpenSans-ExtraBoldItalic.ttf ${D}/usr/share/fonts
	install -m 0755 ${S}/fonts/ttf/OpenSans-ExtraBold.ttf       ${D}/usr/share/fonts
	install -m 0755 ${S}/fonts/ttf/OpenSans-Italic.ttf          ${D}/usr/share/fonts
	install -m 0755 ${S}/fonts/ttf/OpenSans-LightItalic.ttf     ${D}/usr/share/fonts
	install -m 0755 ${S}/fonts/ttf/OpenSans-Light.ttf           ${D}/usr/share/fonts
	install -m 0755 ${S}/fonts/ttf/OpenSans-Regular.ttf         ${D}/usr/share/fonts
	install -m 0755 ${S}/fonts/ttf/OpenSans-SemiBoldItalic.ttf  ${D}/usr/share/fonts
	install -m 0755 ${S}/fonts/ttf/OpenSans-SemiBold.ttf        ${D}/usr/share/fonts
}

FILES:${PN} += "${datadir}/fonts/*.ttf"