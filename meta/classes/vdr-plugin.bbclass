inherit vdr

PLUGIN_NAME = "${@d.getVar('PN')[11:]}"

DEPENDS += "vdr (=${VDR_VERSION})"
RDEPENDS:${PN} += "vdr (>=${VDR_VERSION}-${VDR_REVISION}) vdr (<${VDR_VERSION}-${VDR_REVISION}..)"

FILES:${PN} += "${libdir}/vdr/* ${sysconfdir}/vdr/conf.d/"
FILES:${PN}-dbg += "${libdir}/vdr/.debug"
FILES:${PN}-locale += "${datadir}/locale"

CONFFILES:${PN} += "${sysconfdir}/vdr/conf.d/50_${PLUGIN_NAME}.conf"

#CXXFLAGS += "-g"

do_install:append() {
	if [ ! -d ${D}${sysconfdir}/vdr/conf.d ]; then
		install -d ${D}${sysconfdir}/vdr/conf.d
		echo "[${PLUGIN_NAME}]" > ${D}${sysconfdir}/vdr/conf.d/50_${PLUGIN_NAME}.conf
	fi
}
