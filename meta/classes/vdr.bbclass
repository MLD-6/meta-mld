VDR_VERSION = "2.7.3"
VDR_REVISION = "r2"
SECTION = "VDR"

inherit pkgconfig gettext 

PLUGINDIR = "${libdir}/vdr"
sharedir = "${prefix}/share"
